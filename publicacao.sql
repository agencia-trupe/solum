-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: previa_solum.mysql.dbaas.com.br
-- Generation Time: 13-Abr-2017 às 11:48
-- Versão do servidor: 5.6.35-80.0-log
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `previa_solum`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `avisos`
--

CREATE TABLE `avisos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aviso` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `avisos`
--

INSERT INTO `avisos` (`id`, `ordem`, `data`, `categoria`, `aviso`, `created_at`, `updated_at`) VALUES
(1, 0, '2016-10-19 12:45', 'aviso', '<strong>Tr&acirc;nsito Lento<br />\r\nDica para quem vem da Av. Paulista</strong><br />\r\nPara voc&ecirc; que estiver pr&oacute;ximo da regi&atilde;o do Para&iacute;so ou demais localiza&ccedil;&otilde;es, sugerimos que ao inv&eacute;s de pegar a Av. Paulista, venha atrav&eacute;s da rua Treze de Maio (rua do Shopping Paulista). A seq&uuml;&ecirc;ncia desta rua &eacute; a Rua Rui Barbosa que vai at&eacute; a rua Augusta, o tr&acirc;nsito &eacute; bem mais tranq&uuml;ilo devido ao contra-fluxo na Consola&ccedil;&atilde;o.', '2016-10-19 16:45:44', '2016-10-19 21:24:23'),
(4, 0, '2016-10-19 17:24', 'diversao', '<strong>BAILE DA SOLUM - 29 de ABRIL</strong><br />\r\n22h30 -&nbsp;Ritmos Variados<br />\r\n(Sertanejo, Vanera, Zouk, Salsa, Bachata, Samba Rock, Gafieira e Forr&oacute;)<br />\r\nLocal: Solum Escola de Dan&ccedil;a -&nbsp;Rua da Consola&ccedil;&atilde;o 1741', '2016-10-19 21:24:48', '2017-04-01 06:57:14'),
(5, 0, '2016-10-19 17:25', 'aviso', '<strong>Domingo Av. Paulista Fechada -&nbsp;DICA</strong><br />\r\nAv.Paulista aos domingos fechada para lazer. Fique ligado e evite este caminho, sugerimos vir pelo centro e subir a consola&ccedil;&atilde;o.', '2016-10-19 21:25:17', '2017-04-01 06:58:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `frase`, `link`, `created_at`, `updated_at`) VALUES
(15, 0, 'solum-10-anos-020_20170401025902.jpg', 'O 1º PASSO', 'É ensinar você', '', '2017-04-01 05:59:04', '2017-04-01 06:32:25'),
(19, 0, 'edercarol_20170401032439.jpg', '', '', '', '2017-04-01 06:24:39', '2017-04-01 06:24:39'),
(21, 0, 'samba-saltando_20170401032733.jpg', 'O 1º PASSO', 'É Ensinar Você', '', '2017-04-01 06:27:33', '2017-04-01 06:27:33'),
(22, 0, 'zouk-navio_20170401032932.jpg', 'O 1º PASSO', 'É ensinar você', '', '2017-04-01 06:29:32', '2017-04-01 06:29:32'),
(23, 0, 'zouk-rubia_20170401033031.jpg', 'O 1º PASSO', 'É ensinar você', '', '2017-04-01 06:30:31', '2017-04-01 06:30:31'),
(24, 0, 'solum-10-anos-002_20170401033455.jpg', '', '', '', '2017-04-01 06:34:58', '2017-04-01 06:35:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE `chamadas` (
  `id` int(10) UNSIGNED NOT NULL,
  `chamada_1` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_4` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_4_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `chamada_1`, `chamada_1_link`, `chamada_2`, `chamada_2_link`, `chamada_3`, `chamada_3_link`, `chamada_4`, `chamada_4_link`, `created_at`, `updated_at`) VALUES
(1, 'CONFIRA NOSSA<br />\r\n<strong>GRADE DE AULAS</strong><br />\r\nE VENHA APRENDER A DAN&Ccedil;AR COM A GENTE!', '#', 'CONFIRA NOSSO<br />\r\n<strong>CALEND&Aacute;RIO</strong><br />\r\nE PARTICIPE DOS EVENTOS DE DAN&Ccedil;A DE SAL&Atilde;O!', 'previa-solum/calendario/eventos', 'ACOMPANHE NOSSO<br />\r\n<strong>MURAL ONLINE</strong><br />\r\nE FIQUE ANTENADO COM TUDO QUE ACONTECE NA SOLUM!', '#', 'SAIBA MAIS SOBRE<br />\r\n<strong>A SOLUM</strong><br />\r\nE CONFIRA FOTOS, PERFIS, V&Iacute;DEOS E MUITO MAIS!', '#', NULL, '2017-04-13 03:45:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `como_chegar`
--

CREATE TABLE `como_chegar` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `codigo_googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `como_chegar`
--

INSERT INTO `como_chegar` (`id`, `imagem`, `endereco`, `observacoes`, `codigo_googlemaps`, `created_at`, `updated_at`) VALUES
(1, 'logo-layout-oficial-cortetransparente-chapado_20170401035221.png', 'Rua da Consola&ccedil;&atilde;o, 1741 - S&atilde;o Paulo - SP', '<p><strong>Dica para quem vem da Av. Paulista</strong></p>\r\n\r\n<p>Para voc&ecirc;, que est&aacute; cansado do intenso tr&acirc;nsito da Av. Paulista. Temos um caminho opcional... Se voc&ecirc; estiver pr&oacute;ximo da regi&atilde;o do Para&iacute;so ou demais localiza&ccedil;&otilde;es, sugerimos que ao inv&eacute;s de pegar a Av. Paulista, venha atrav&eacute;s da Rua treze de Maio (Rua do Shopping Paulista). A sequ&ecirc;ncia dessa rua &eacute; a Rua Rui Barbosa que vai at&eacute; a rua Augusta, o tr&acirc;nsito &eacute; bem mais tranquilo devido ao contra-fluxo na Consola&ccedil;&atilde;o.<br />\r\n<br />\r\n*Fique ligado pois aos domingos a Av. Paulista &eacute; fechada para lazer</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.4600536880034!2d-46.65939748461522!3d-23.551915084687476!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5833e80e4ef1%3A0x10fc336ad19262a4!2sSolum+Escola+de+Dan%C3%A7a!5e0!3m2!1spt-BR!2sbr!4v1476888580167" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', NULL, '2017-04-01 06:52:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `horarios_de_atendimento` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefones`, `horarios_de_atendimento`, `facebook`, `youtube`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'contato@solum.com.br', '11 3255-5162 | 11 3151-2185 | 11 98138-0273 ', '<p>Segunda a Quinta: 10h30 &agrave;s 21h30<br />\r\nSextas: 10h30 &agrave;s 18h30<br />\r\nS&aacute;bados: Fechado<br />\r\nDomingos: 12h00 &agrave;s 21h00<br />\r\n<br />\r\nAtendimento Online Whatsapp<br />\r\n(11) 98138-0273 (somente p/ atendimentos r&aacute;pidos e objetivos)</p>\r\n', 'https://www.facebook.com/SolumEscoladeDanca', '#', 'icone-telefonesite_20170412180529.png', NULL, '2017-04-12 21:05:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_fixo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_celular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ritmo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curso` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `telefone_fixo`, `telefone_celular`, `assunto`, `ritmo`, `curso`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(2, 'Teste', 'teste@teste.com', '11 1234 5678', '', 'Informações', 'Tango', '', 'Mensagem de teste.', 0, '2016-10-19 17:00:32', '2016-10-19 17:00:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos_intensivos`
--

CREATE TABLE `cursos_intensivos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `investimento` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `cursos_intensivos`
--

INSERT INTO `cursos_intensivos` (`id`, `ordem`, `titulo`, `imagem`, `descricao`, `investimento`, `chamada`, `link`, `created_at`, `updated_at`) VALUES
(1, 4, ' INTENSIVÃO DE SERTANEJO UNIVERSITÁRIO ', 'sertanejo-2011_20161019185501.jpg', '<p>3 horas de aula por domingo (iniciantes)</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00&nbsp;<br />\r\nM&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2: &nbsp;21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 18:00 &agrave;s 21:00&nbsp;<br />\r\nM&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2: &nbsp;21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio<br />\r\n-----------------------------------------------<br />\r\n<br />\r\n<strong>M&Oacute;DULOS 4,5,6</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00&nbsp;<br />\r\nM&oacute;dulo 4 : 04/Junho<br />\r\nM&oacute;dulo 5: &nbsp;11/Junho<br />\r\nM&oacute;dulo 6 : 25/Junho<br />\r\n<br />\r\n<strong>M&Oacute;DULOS 7,8,9</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00&nbsp;<br />\r\nM&oacute;dulo 7 : 02/Julho<br />\r\nM&oacute;dulo 8 : 16/Julho<br />\r\nM&oacute;dulo 9 : 23/Julho</p>\r\n\r\n<p>-----------------------------------------------</p>\r\n\r\n<p><strong>M&Oacute;DULOS 4,5,6 (p/ quem est&aacute; fez 1,2,3):</strong></p>\r\n\r\n<p>Hor&aacute;rio do Curso 18:00 &agrave;s 21:00<br />\r\nM&oacute;dulo 4 : 07/Maio<br />\r\nM&oacute;dulo 5 : 21/Maio<br />\r\nM&oacute;dulo 6 : 28/Maio</p>\r\n\r\n<p>---------------------------------------</p>\r\n\r\n<p><strong>M&Oacute;DULOS 10,11,12</strong>:<br />\r\n(p/ quem fez 7,8,9 = 110,00)</p>\r\n\r\n<p>S&aacute;bados 18:00 &agrave;s 21:00<br />\r\n*Turma sugerida pelos alunos (&ntilde; haver&aacute; bolsistas)<br />\r\nM&oacute;dulo 10 : 06/Maio<br />\r\nM&oacute;dulo 11 : 13/Maio<br />\r\nM&oacute;dulo 12 : 20/Maio</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110<br />\r\nPacote 6 M&oacute;ds = R$210<br />\r\nPacote 9 M&oacute;ds = R$310</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120<br />\r\nPacote 6 M&oacute;ds = R$230<br />\r\nPacote 9 M&oacute;ds = R$330</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pagtos at&eacute; 20/Abril,<br />\r\nouenquanto restar vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135<br />\r\nPacote 6 M&oacute;ds = R$245<br />\r\nPacote 9 M&oacute;ds = R$345</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 16:38:42', '2017-04-12 22:47:39'),
(2, 9, 'INTENSIVÃO DE SAMBA ROCK', 'srock-2011_20170412202657.jpg', '<p>3 horas de aula por domingo (iniciantes)</p>\r\n\r\n<p><strong>M&oacute;dulos 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00&nbsp;</p>\r\n\r\n<p>M&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2: &nbsp;21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 20/Abril,&nbsp;<br />\r\nou enquanto houver vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 20:56:48', '2017-04-12 23:26:57'),
(3, 10, ' INTENSIVÃO DE FORRÓ', 'forro-2011_20161019185749.gif', '<p>(para iniciantes)<br />\r\n3 horas de aula por domingo</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 18:00 &agrave;s 21:00</p>\r\n\r\n<p>M&oacute;dulo 1 : 04/Junho<br />\r\nM&oacute;dulo 2: &nbsp;11/Junho<br />\r\nM&oacute;dulo 3 : 25/Junho</p>\r\n\r\n<p>----------------------------------------------</p>\r\n\r\n<p><strong>M&Oacute;DULOS 4,5,6</strong><br />\r\n(Para quem j&aacute; fez o iniciante)</p>\r\n\r\n<p>Hor&aacute;rio do Curso 12:00 &agrave;s 15:00</p>\r\n\r\n<p>M&oacute;dulo 4 : 07/Maio<br />\r\nM&oacute;dulo 5: &nbsp;21/Maio<br />\r\nM&oacute;dulo 6 : 28/Maio</p>\r\n', '<p><strong>Pagamentos at&eacute; 27/Abril:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pagamentos at&eacute; 04/Maio:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pagtos at&eacute; 18/Maio,&nbsp;<br />\r\nou enquanto restar vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 20:57:49', '2017-04-12 22:46:11'),
(4, 6, 'INTENSIVÃO DE ZOUK', 'zouk-2011_20161019185914.jpg', '<p>(para iniciantes)<br />\r\n3 horas de aula por domingo</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 18:00 &agrave;s 21:00:</p>\r\n\r\n<p>M&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2: &nbsp;21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio</p>\r\n\r\n<p><br />\r\n<strong>M&Oacute;DULOS 4,5,6</strong><br />\r\nHor&aacute;rio do Curso 18:00 &agrave;s 21:00:</p>\r\n\r\n<p>M&oacute;dulo 4 : 04/Junho<br />\r\nM&oacute;dulo 5: &nbsp;11/Junho<br />\r\nM&oacute;dulo 6 : 25/Junho</p>\r\n\r\n<p><br />\r\n<strong>M&Oacute;DULOS 7,8,9</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00:</p>\r\n\r\n<p>M&oacute;dulo 7 : 30/Julho<br />\r\nM&oacute;dulo 8 : 06/Agosto<br />\r\nM&oacute;dulo 9 : 20/Agosto</p>\r\n\r\n<p>--------------------------------------------</p>\r\n\r\n<p><em><strong>Para quem j&aacute; est&aacute; fez m&oacute;dulos 4,5,6</strong></em>:</p>\r\n\r\n<p><strong>M&Oacute;DULOS 7,8,9<br />\r\nEquil&iacute;brio, T&eacute;cnicas de Leveza, Mov.Cabe&ccedil;a</strong><br />\r\n(110,00 p/ pagtos at&eacute; 13/abr)<br />\r\n15:00 &agrave;s 18:00:</p>\r\n\r\n<p>M&oacute;dulo 7 : 07/Maio<br />\r\nM&oacute;dulo 8 : 21/Maio<br />\r\nM&oacute;dulo 9 : 28/Maio</p>\r\n\r\n<p><br />\r\n<strong>M&Oacute;DULOS 10,11,12</strong> (pacote 110,00)<br />\r\n<strong>Zouk Black, Dissocia&ccedil;&otilde;es, Giros, Mov.Cabe&ccedil;a</strong><br />\r\n15:00 &agrave;s 18:00:</p>\r\n\r\n<p>M&oacute;dulo 10 : 04/Junho<br />\r\nM&oacute;dulo 11 : 11/Junho<br />\r\nM&oacute;dulo 12 : 25/Junho</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110<br />\r\nPacote 6 M&oacute;ds = R$210<br />\r\nPacote 9 M&oacute;ds = R$310</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120<br />\r\nPacote 6 M&oacute;ds = R$230<br />\r\nPacote 9 M&oacute;ds = R$330</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 20/Abril,<br />\r\nou enquanto restar vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135<br />\r\nPacote 6 M&oacute;ds = R$245<br />\r\nPacote 9 M&oacute;ds = R$345</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 20:59:14', '2017-04-12 22:54:30'),
(5, 11, 'INTENSIVÃO SALSA', 'salsa2011_20161019190005.jpg', '<p>3 horas de aula por domingo<br />\r\n(para iniciantes)</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 18:00 &agrave;s 21:00&nbsp;</p>\r\n\r\n<p>M&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2 : 21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio<br />\r\n&nbsp;</p>\r\n', '<p><strong>Pagamentos at&eacute; 26/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 13/Abril,&nbsp;<br />\r\nou enquanto houver vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 21:00:05', '2017-04-12 22:59:10'),
(6, 8, ' INTENSIVÃO DE BACHATA', 'banner-bachata2012_20161019190113.jpg', '<p>3 horas de aula por domingo<br />\r\n(para iniciantes)&nbsp;&nbsp;</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00<br />\r\nM&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2: &nbsp;21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio</p>\r\n\r\n<p><br />\r\n<strong>M&Oacute;DULOS 4,5,6</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00<br />\r\nM&oacute;dulo 4 : 04/Junho<br />\r\nM&oacute;dulo 5: &nbsp;11/Junho<br />\r\nM&oacute;dulo 6 : 25/Junho</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110<br />\r\nPacote 6 M&oacute;ds = R$210</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120<br />\r\nPacote 6 M&oacute;ds = R$230</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 20/Abril,&nbsp;<br />\r\nou enquanto houver vagas:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135<br />\r\nPacote 6 M&oacute;ds = R$245</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 21:01:13', '2017-04-12 23:01:52'),
(7, 5, 'VANERA PAULISTA', 'vanera-2016_20170413003737.png', '<p>3 horas de aula por domingo&nbsp;<br />\r\n(para iniciantes)</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00&nbsp;<br />\r\nM&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2 : 21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio</p>\r\n\r\n<p><br />\r\n<strong>M&Oacute;DULOS 4,5,6</strong><br />\r\nHor&aacute;rio do Curso 18:00 &agrave;s 21:00&nbsp;<br />\r\nM&oacute;dulo 4 : 02/Julho<br />\r\nM&oacute;dulo 5 : 16/Julho<br />\r\nM&oacute;dulo 6 : 23/Julho<br />\r\n----------------------------------------------</p>\r\n\r\n<p><strong>M&Oacute;DULOS 7,8,9</strong><br />\r\n<em>(Para quem j&aacute; fez m&oacute;dulos anteriores)</em></p>\r\n\r\n<p>Hor&aacute;rio do Curso 15:00 &agrave;s 18:00&nbsp;<br />\r\nM&oacute;dulo 7 : 07/Maio<br />\r\nM&oacute;dulo 8 : 21/Maio<br />\r\nM&oacute;dulo 9 : 28/Maio</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110<br />\r\nPacote 6 M&oacute;ds = R$210</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120<br />\r\nPacote 6 M&oacute;ds = R$230</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 13/Abril,&nbsp;<br />\r\nou enquanto houver vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135<br />\r\nPacote 6 M&oacute;ds = R$245</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 21:02:07', '2017-04-13 03:37:37'),
(8, 12, ' INTENSIVÃO DE TANGO', 'tango-2011_20161019190319.jpg', '<p>3 horas de aula por domingo&nbsp;<br />\r\n(para iniciantes)</p>\r\n\r\n<p><br />\r\n<strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 12:00 &agrave;s 15:00<br />\r\nM&oacute;dulo 1 : 04/Junho<br />\r\nM&oacute;dulo 2: &nbsp;11/Junho<br />\r\nM&oacute;dulo 3 : 25/Junho</p>\r\n', '<p><strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 04/Maio:</strong><br />\r\n1 M&oacute;dulo = R$60,00<br />\r\nPacote 3 M&oacute;ds = R$130</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 18/Maio,&nbsp;<br />\r\nou enquanto houver vaga:</strong><br />\r\n1 M&oacute;dulo = R$65<br />\r\nPacote 3 M&oacute;ds = R$145</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 21:03:19', '2017-04-12 23:05:31'),
(9, 7, 'EXPRESSÃO CORPORAL FEMININA', 'banner-mulheres-2013_20161019190417.jpg', '<p>3 domingos<br />\r\nAula para Mulheres<br />\r\nN&iacute;vel Neutro</p>\r\n\r\n<p>----------------------------------<br />\r\n&nbsp;Aula para soltar mais o corpo, n&atilde;o exige pr&eacute;-requisitos de&nbsp;ritmos espec&iacute;ficos. Possui como objetivo exercitar, e desenvolver mais o charme e a suavidade na dan&ccedil;a.<br />\r\n*Melhor se vier de salto, mas traga cal&ccedil;ados alternativos</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio 15:30 &agrave;s 17:30:</p>\r\n\r\n<p>M&oacute;dulo 1 : 04/Junho<br />\r\nM&oacute;dulo 2 : 11/Junho<br />\r\nM&oacute;dulo 3 : 25/Junho</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n', '<p><strong>Pagamentos at&eacute; 27/Abril:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 04/Maio:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 18/Maio,&nbsp;<br />\r\nou enquanto restar vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 21:04:17', '2017-04-12 23:07:39'),
(10, 13, 'INTENSIVÃO DE STILETTO', 'banner-stiletto2014_20161019190513.jpg', '<p>3 domingos (n&iacute;vel neutro)&nbsp;&nbsp;</p>\r\n\r\n<p>Hor&aacute;rio do Curso 15:30 &agrave;s 17:30&nbsp;<br />\r\n<strong>M&oacute;dulo 1</strong> : 07/Maio<br />\r\n<strong>M&oacute;dulo 2 </strong>: 21/Maio&nbsp;<br />\r\n<strong>M&oacute;dulo 3</strong> : 28/Maio&nbsp;</p>\r\n\r\n<p><br />\r\nStiletto &eacute; uma aula em cima do Salto para mulheres, que desenvolve a atitude com movimentos de v&aacute;rias influ&ecirc;ncias de dan&ccedil;as femininas.<br />\r\nRefer&ecirc;ncias e influ&ecirc;ncias nas core&oacute;grafas de Beyonce.</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00<br />\r\nPacote 3 M&oacute;ds = R$110</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\nM&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 13/Abr,&nbsp;<br />\r\nou enquanto houver vaga:</strong><br />\r\nM&oacute;dulo = R$60,00<br />\r\nPacote 3 M&oacute;ds = R$135</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 21:05:13', '2017-04-12 23:10:19'),
(11, 14, 'INTENSIVÃO DE KIZOMBA', 'banner-kizomba2016_20161019190555.jpg', '<p>3 horas de aula por domingo&nbsp;<br />\r\n(para iniciantes)</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio do Curso 15:00 &agrave;s 18:00</p>\r\n\r\n<p>M&oacute;dulo 1 : 04/Junho<br />\r\nM&oacute;dulo 2: &nbsp;11/Junho<br />\r\nM&oacute;dulo 3 : 25/Junho</p>\r\n', '<p><br />\r\n<strong>Pagamentos at&eacute; 27/Abril:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 04/Maio:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 18/Maio,&nbsp;<br />\r\nou enquanto restar vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135<br />\r\n&nbsp;</p>\r\n', 'INSCREVA-SE!', '#', '2016-10-19 21:05:55', '2017-04-12 23:11:49'),
(12, 3, 'INTENSIVÃO DE GAFIEIRA', 'gafieira-2012_20170412202648.gif', '<p>3 horas de aula por domingo<br />\r\n(para iniciantes)</p>\r\n\r\n<p><strong>M&Oacute;DULOS 1,2,3</strong><br />\r\nHor&aacute;rio dos Cursos 15:00 &agrave;s 18:00</p>\r\n\r\n<p>M&oacute;dulo 1 : 07/Maio<br />\r\nM&oacute;dulo 2: &nbsp;21/Maio<br />\r\nM&oacute;dulo 3 : 28/Maio<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>M&Oacute;DULOS 4,5,6</strong><br />\r\nHor&aacute;rio dos Cursos 15:00 &agrave;s 18:00</p>\r\n\r\n<p>M&oacute;dulo 4 : 02/Julho<br />\r\nM&oacute;dulo 5 : 16/Julho<br />\r\nM&oacute;dulo 6 : 23/Julho</p>\r\n\r\n<p>--------------------------------------------------</p>\r\n\r\n<p><strong>M&Oacute;DULOS 4,5,6&nbsp;</strong><br />\r\n(p/ quem fez 1,2,3 em abril ou antes)<br />\r\nHor&aacute;rio dos Cursos 15:00 &agrave;s 18:00</p>\r\n\r\n<p>M&oacute;dulo 4 : 07/Maio<br />\r\nM&oacute;dulo 5 : 21/Maio<br />\r\nM&oacute;dulo 6 : 28/Maio</p>\r\n\r\n<p><strong>M&Oacute;DULOS 7,8,9</strong><br />\r\nHor&aacute;rio dos Cursos 15:00 &agrave;s 18:00</p>\r\n\r\n<p>M&oacute;dulo 7 : 04/Junho<br />\r\nM&oacute;dulo 8 : 11/Junho<br />\r\nM&oacute;dulo 9 : 25/Junho</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00&nbsp;<br />\r\nPacote 3 M&oacute;ds = R$110<br />\r\nPacote 6 M&oacute;ds = R$210</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$120<br />\r\nPacote 6 M&oacute;ds = R$230</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pagtos at&eacute; 20/Abril,<br />\r\nou enquanto restar vaga:</strong><br />\r\n1 M&oacute;dulo = R$60<br />\r\nPacote 3 M&oacute;ds = R$135<br />\r\nPacote 6 M&oacute;ds = R$245<br />\r\n&nbsp;</p>\r\n', 'INSCREVA-SE!', '#', '2017-04-12 22:36:04', '2017-04-12 23:26:48'),
(13, 2, 'TÉCNICAS DE CONDUÇÃO', 'homens-2013_20170412202637.jpg', '<p><strong>INTENSIV&Atilde;O PARA HOMENS</strong></p>\r\n\r\n<p>N&iacute;vel Neutro<br />\r\n3 Domingos</p>\r\n\r\n<p>Uma s&eacute;rie de exerc&iacute;cio e dicas para que possa compreender quais s&atilde;o os pontos chaves de uma boa condu&ccedil;&atilde;o.<br />\r\nPontos do abra&ccedil;o, contato e sinergia.</p>\r\n\r\n<p><strong>M&oacute;dulo x </strong>= 07 de Maio (c/&nbsp;Edgar Fernandes)<br />\r\n<strong>M&oacute;dulo y </strong>= 21 de Maio (c/&nbsp;Marcelo Eidy)<br />\r\n<strong>M&oacute;dulo z</strong> = 28 de Maio &nbsp;(c/&nbsp;Everson Santos)</p>\r\n\r\n<p>Hor&aacute;rio: 18:00 &agrave;s 21:00</p>\r\n\r\n<p>---------------------------------------<br />\r\nMulheres que se interessarem por cortesia, encaminhem e-mail para contato@solum.com.br e aguardem a confirma&ccedil;&atilde;o da inscri&ccedil;&atilde;o.</p>\r\n', '<p><strong>Pagamentos at&eacute; 30/Mar&ccedil;o:</strong><br />\r\n1 M&oacute;dulo = R$50,00<br />\r\nPacote 3 M&oacute;ds = R99,00</p>\r\n\r\n<p><br />\r\n<strong>Pagamentos at&eacute; 06/Abril:</strong><br />\r\n1 M&oacute;dulo = R$55,00<br />\r\nPacote 3 M&oacute;ds = R$110,00</p>\r\n\r\n<p><br />\r\n<strong>Pagtos at&eacute; 13/Abr,&nbsp;<br />\r\nou enquanto houver vaga:</strong><br />\r\n1 M&oacute;dulo = R$60,00<br />\r\nPacote 3 M&oacute;ds = R$120,00<br />\r\n&nbsp;</p>\r\n', 'INSCREVA-SE!', '#', '2017-04-12 23:15:36', '2017-04-12 23:26:37'),
(14, 1, 'SAMBA ROCK DAY', 'banner-quadr-sitesolum_20170412202628.jpg', '<p><strong>PARTICIPE DESTE EVENTO</strong><br />\r\n<br />\r\n<strong>15 de Abril </strong>= Baile de Samba&nbsp;<br />\r\nShows de dan&ccedil;a e m&uacute;sica ao vivo</p>\r\n\r\n<p><strong>16 de Abril </strong>= AULAS o dia Inteiro para diversos n&iacute;veis</p>\r\n', '<p><strong>Samba Rock Day<br />\r\nProfessores renomados</strong> ir&atilde;o trazer&nbsp;<br />\r\no Melhor do Samba Rock.<br />\r\nConhe&ccedil;a a Grade das Aulas e feche seu pacote:</p>\r\n\r\n<p>www.sambarockday.com.br</p>\r\n', 'INFORMAÇÕES', '#', '2017-04-12 23:19:18', '2017-04-12 23:36:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos_intensivos_chamada`
--

CREATE TABLE `cursos_intensivos_chamada` (
  `id` int(10) UNSIGNED NOT NULL,
  `chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `cursos_intensivos_chamada`
--

INSERT INTO `cursos_intensivos_chamada` (`id`, `chamada`, `created_at`, `updated_at`) VALUES
(1, 'Intensivos são bem populares e procurados.\r\nEm caso de depósito informe imediatamente p/ não perder sua vaga', NULL, '2017-04-13 03:36:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos_regulares`
--

CREATE TABLE `cursos_regulares` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `cursos_regulares`
--

INSERT INTO `cursos_regulares` (`id`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Duração de 20 Aulas (2ª à 5ª Feira)', '<p>S&atilde;o turmas&nbsp;com aulas de 1 hora.<br />\r\nOs cursos possuem 20 aulas (aproximadamente 5 meses)</p>\r\n\r\n<p>A cada 20 aulas o curso muda de hor&aacute;rio. Entenda:<br />\r\nAp&oacute;s os 5 meses&nbsp;o curso encerra, e caso queira participar do pr&oacute;ximo n&iacute;vel ent&atilde;o dever&aacute; escolher outro hor&aacute;rio e contratar um novo plano.<br />\r\n<br />\r\n<strong><em>N&Iacute;VEIS:</em></strong></p>\r\n\r\n<p><strong>Iniciante:</strong>&nbsp;B&aacute;sico da estaca zero (observe a data de in&iacute;cio, pois a turma pode estar no meio ou final do n&iacute;vel iniciante).</p>\r\n\r\n<p><strong>Intermedi&aacute;rio:</strong>&nbsp;Recomendado para aqueles que possuem conhecimentos m&iacute;nimos equivalentes a 6 meses de iniciante.</p>\r\n\r\n<p><strong>Est&aacute;gio A:</strong>&nbsp;Para o aluno entrar no n&iacute;vel Est&aacute;gio A, dever&aacute; obter o conhecimento m&iacute;nimo de 6 meses do n&iacute;vel intermedi&aacute;rio.</p>\r\n\r\n<p><strong>Est&aacute;gio A2:</strong>&nbsp;Para o aluno entrar no n&iacute;vel Est&aacute;gio A2, dever&aacute; obter o conhecimento m&iacute;nimo de 6 meses do n&iacute;vel Est&aacute;gio A. Esta &eacute; uma etapa cont&iacute;nua, aonde o aluno manter&aacute; por tempo indeterminado, e s&oacute; poder&aacute; migrar para o n&iacute;vel Avan&ccedil;ado mediante a aprova&ccedil;&atilde;o do Professor.</p>\r\n\r\n<p><strong>Avan&ccedil;ado:</strong>&nbsp;Para alunos com mais pr&aacute;tica e experi&ecirc;ncia. Para entrar no Avan&ccedil;ado, dever&aacute; passar por uma avalia&ccedil;&atilde;o do professor.<br />\r\n<br />\r\nACESSE A GRADE DE AULAS, e veja a programa&ccedil;&atilde;o ATUAL</p>\r\n', NULL, '2017-04-13 03:52:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos_regulares_imagens_laterais`
--

CREATE TABLE `cursos_regulares_imagens_laterais` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `cursos_regulares_imagens_laterais`
--

INSERT INTO `cursos_regulares_imagens_laterais` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(3, 0, 'logo-layout-oficial-cortetransparente-chapado_20170401042301.png', '2017-04-01 07:23:01', '2017-04-01 07:23:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estrutura`
--

CREATE TABLE `estrutura` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `estrutura`
--

INSERT INTO `estrutura` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>&bull;&nbsp;Recep&ccedil;&atilde;o com TVs LED, mesas e cadeiras para consumo de, caf&eacute;, bebidas, salgados, lanches e doces;<br />\r\n&bull;&nbsp;Salas com at&eacute; 140m2;<br />\r\n&bull;&nbsp;Pisos pr&oacute;prios para dan&ccedil;a de sal&atilde;o;<br />\r\n&bull;&nbsp;Boa ilumina&ccedil;&atilde;o;<br />\r\n&bull;&nbsp;Aparelhos de ar condicionado;<br />\r\n&bull;&nbsp;Ventiladores;<br />\r\n&bull;&nbsp;Purificador de &aacute;gua;<br />\r\n&bull;&nbsp;Som com mesa com alta potencia;<br />\r\n&bull;&nbsp;Raio de luz para ambientes climatizados;</p>\r\n', NULL, '2017-01-17 01:04:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estrutura_imagens`
--

CREATE TABLE `estrutura_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `estrutura_imagens`
--

INSERT INTO `estrutura_imagens` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '3_20161019182750.jpg', '2016-10-19 20:27:50', '2016-10-19 20:27:50'),
(2, 0, '4_20161019182750.jpg', '2016-10-19 20:27:50', '2016-10-19 20:27:50'),
(3, 0, '5_20161019182751.jpg', '2016-10-19 20:27:51', '2016-10-19 20:27:51'),
(4, 0, '6_20161019182751.jpg', '2016-10-19 20:27:52', '2016-10-19 20:27:52'),
(5, 0, '2_20161019182751.jpg', '2016-10-19 20:27:52', '2016-10-19 20:27:52'),
(6, 0, '9_20161019182752.jpg', '2016-10-19 20:27:52', '2016-10-19 20:27:52'),
(7, 0, '8_20161019182752.jpg', '2016-10-19 20:27:53', '2016-10-19 20:27:53'),
(8, 0, '7_20161019182752.jpg', '2016-10-19 20:27:53', '2016-10-19 20:27:53'),
(9, 0, '10_20161019182753.jpg', '2016-10-19 20:27:53', '2016-10-19 20:27:53'),
(10, 0, '11_20161019182753.jpg', '2016-10-19 20:27:54', '2016-10-19 20:27:54'),
(11, 0, 'solumnight1_20161019182754.jpg', '2016-10-19 20:27:54', '2016-10-19 20:27:54'),
(12, 0, '12_20161019182753.jpg', '2016-10-19 20:27:54', '2016-10-19 20:27:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estrutura_imagens_laterais`
--

CREATE TABLE `estrutura_imagens_laterais` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos`
--

CREATE TABLE `eventos` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `eventos`
--

INSERT INTO `eventos` (`id`, `data`, `titulo`, `descricao`, `created_at`, `updated_at`) VALUES
(1, '2017-01-28', 'Baile ritmos variados', '<p>A partir das 22h30.</p>\r\n\r\n<p>Rua da Consola&ccedil;&atilde;o 1.741.</p>\r\n\r\n<p>Muito Zouk, Salsa, Bachata, Samba, Sertanejo e Forr&oacute;</p>\r\n', '2016-10-19 16:43:04', '2017-01-17 01:47:09'),
(4, '2017-04-16', 'SAMBA ROCK DAY', '<p><strong>15 e 16 de ABRIL</strong></p>\r\n\r\n<p>Em Abril&nbsp;acontecer&aacute; um grande evento de Samba Rock, com aulas, shows&nbsp;e baile.&nbsp;<br />\r\nS&atilde;o professores&nbsp;renomados, trazendo seus&nbsp;conhecimento para compartilhar com Iniciantes, Intermedi&aacute;rios e Avan&ccedil;ados.<br />\r\n<strong>Baile 15 de Abril </strong>na Solum (s&aacute;bado 22:00)<br />\r\n<strong>AULAS 16&nbsp;de Abril </strong>na Solum (dia inteiro)<br />\r\nFeche seu pacote, conhe&ccedil;a a os professores, grade de aulas, e os hor&aacute;rios do bailes: em http://www.sambarockday.com.br</p>\r\n', '2016-10-19 20:52:07', '2017-04-01 07:08:35'),
(5, '2017-09-07', 'ZOUK DAY CONGRESS', '<p>6 a 10 de Setembro (bailes e aulas)</p>\r\n\r\n<p>Congresso Internacional que traz grandes professores de zouk da atualidade mundial.<br />\r\nAcompanhe tudo que acontece! Curta no face: &nbsp;https://www.facebook.com/pg/zoukdaycongress/</p>\r\n\r\n<p>Aulas para Iniciantes a avan&ccedil;ados<br />\r\nInscri&ccedil;&otilde;es<br />\r\nwww.zoukday.com.br</p>\r\n', '2017-01-17 01:56:16', '2017-01-17 01:56:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `grade_de_aulas`
--

CREATE TABLE `grade_de_aulas` (
  `id` int(10) UNSIGNED NOT NULL,
  `dia_da_semana` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ritmo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nivel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inicio` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `horario` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `professores` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `observacoes` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Disponível para Homens e Mulheres',
  `observacoes_extra` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `grade_de_aulas`
--

INSERT INTO `grade_de_aulas` (`id`, `dia_da_semana`, `ritmo`, `nivel`, `inicio`, `horario`, `professores`, `observacoes`, `observacoes_extra`, `created_at`, `updated_at`) VALUES
(1, 'SEGUNDAS', 'Sertanejo Universitário', 'Iniciante', '03/abr', '19h15 - 20h15', 'Emerson/Jessica', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(2, 'SEGUNDAS', 'Kizomba', 'Intermediário', '20/mar', '19h15 - 20h15', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(3, 'SEGUNDAS', 'Dança de Salão Sequencial', 'Iniciante', '03/abr', '19h15 - 20h15', 'Everson/Luisa', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(4, 'SEGUNDAS', 'Gafieira', 'Avançado', '20/mar', '19h15 - 20h15', 'Carol/Danilo', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(5, 'SEGUNDAS', 'Bachata', 'Iniciante', '03/abr', '20h30 - 21h30', 'Rodrigo/Laura', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(6, 'SEGUNDAS', 'Gafieira', 'Intermediário', '03/abr', '20h30 - 21h30', 'Everson/Luisa', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(7, 'SEGUNDAS', 'Zouk', 'Estágio A2', '20/mar', '20h30 - 21h30', 'Carol/Vitor', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(8, 'SEGUNDAS', 'Zouk', 'Avançado', '20/mar', '20h30 - 21h30', 'Ale/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(9, 'SEGUNDAS', 'Bachata', 'Intermediário', '03/abr', '21h30 - 22h30', 'Rodrigo/Laura', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(10, 'SEGUNDAS', 'Salsa', 'Iniciante', '03/abr', '21h30 - 22h30', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(11, 'SEGUNDAS', 'Gafieira', 'Estágio A', '20/mar', '21h30 - 22h30', 'Carol/Danilo', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(12, 'SEGUNDAS', 'Gafieira', 'Estágio A2', '20/mar', '21h30 - 22h30', 'Everson/Luisa', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(13, 'TERÇAS', 'Jazz', 'Intermediário', '21/mar', '19h15 - 20h15', 'Rúbia', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(14, 'TERÇAS', 'Gafieira', 'Avançado', '21/mar', '19h15 - 20h15', 'Everson/Luisa', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(15, 'TERÇAS', 'Zouk', 'Iniciante', '04/abr', '19h15 - 20h15', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(16, 'TERÇAS', 'Samba-Rock', 'Estágio A', '21/mar', '19h15 - 20h15', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(17, 'TERÇAS', 'Gafieira', 'Avançado', '21/mar', '20h30 - 21h30', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(18, 'TERÇAS', 'Sertanejo Universitário', 'Intermediário', '04/abr', '20h30 - 21h30', 'Eder/Maria', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(19, 'TERÇAS', 'Dança de Salão Sequencial', 'Iniciante', '04/abr', '20h30 - 21h30', 'Everson/Luisa', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(20, 'TERÇAS', 'Zouk', 'Estágio A', '21/mar', '20h30 - 21h30', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(21, 'TERÇAS', 'Bachata', 'Iniciante', '04/abr', '21h30 - 22h30', 'Rodrigo/Laura', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(22, 'TERÇAS', 'Zouk', 'Intermediário', '04/abr', '21h30 - 22h30', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(23, 'TERÇAS', 'Gafieira', 'Intermediário', '04/abr', '21h30 - 22h30', 'Everson/Luisa', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(24, 'TERÇAS', 'Sertanejo Universitário', 'Estágio A2', '21/mar', '21h30 - 22h30', 'Eder/Maria', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(25, 'QUARTAS', 'Sertanejo Universitário', 'Avançado', '29/mar', '19h15 - 20h15', 'Jr Arruda/Susy', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(26, 'QUARTAS', 'Samba-Rock', 'Iniciante', '05/abr', '19h15 - 20h15', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(27, 'QUARTAS', 'Dança de Salão Sequencial', 'Iniciante', '05/abr', '19h15 - 20h15', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(28, 'QUARTAS', 'Gafieira', 'Iniciante', '05/abr', '19h15 - 20h15', 'Everson/Carol', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(29, 'QUARTAS', 'Jazz', 'Iniciante', '05/abr', '20h30 - 21h30', 'Rúbia', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(30, 'QUARTAS', 'Gafieira', 'Intermediário', '05/abr', '20h30 - 21h30', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(31, 'QUARTAS', 'Zouk', 'Iniciante', '05/abr', '20h30 - 21h30', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(32, 'QUARTAS', 'Vanera Paulista', 'Estágio A', '29/mar', '20h30 - 21h30', 'Jr Arruda/Susy', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(33, 'QUARTAS', 'Zouk', 'Avançado', '29/mar', '21h30 - 22h30', 'Rúbia/Edgar', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(34, 'QUARTAS', 'Gafieira', 'Iniciante', '05/abr', '21h30 - 22h30', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(35, 'QUARTAS', 'Bolero', 'Intermediário', '05/abr', '21h30 - 22h30', 'Everson/Luisa', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(36, 'QUARTAS', 'Sertanejo Universitário', 'Estágio A', '29/mar', '21h30 - 22h30', 'Jr Arruda/Susy', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(37, 'QUINTAS', 'Salsa', 'Estágio A', '23/mar', '19h15 - 20h15', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(38, 'QUINTAS', 'Zouk', 'Iniciante', '30/mar', '19h15 - 20h15', 'Rúbia/Edgar', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(39, 'QUINTAS', 'Gafieira', 'Estágio A', '23/mar', '19h15 - 20h15', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(40, 'QUINTAS', 'Sertanejo Universitário', 'Intermediário', '30/mar', '19h15 - 20h15', 'Jr Arruda/Susy', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(41, 'QUINTAS', 'Samba-Rock', 'Intermediário', '30/mar', '20h30 - 21h30', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(42, 'QUINTAS', 'Zouk', 'Intermediário', '30/mar', '20h30 - 21h30', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(43, 'QUINTAS', 'Zouk', 'Estágio A', '23/mar', '20h30 - 21h30', 'Rúbia/Edgar', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(44, 'QUINTAS', 'Sertanejo Universitário', 'Estágio A', '23/mar', '20h30 - 21h30', 'Jr Arruda/Susy', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(45, 'QUINTAS', 'Gafieira Aprimoramento', 'Intermediário', '30/mar', '21h30 - 22h30', 'Will/Mary', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(46, 'QUINTAS', 'Salsa', 'Intermediário', '30/mar', '21h30 - 22h30', 'Alê/Aretha', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(47, 'QUINTAS', 'Vanera Paulista', 'Iniciante', '30/mar', '21h30 - 22h30', 'Jr Arruda/Susy', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13'),
(48, 'QUINTAS', 'Zouk', 'Estágio A', '23/mar', '21h30 - 22h30', 'Rúbia/Edgar', 'Disponível para Homens e Mulheres', '', '2017-03-29 16:05:13', '2017-03-29 16:05:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico`
--

CREATE TABLE `historico` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `historico`
--

INSERT INTO `historico` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p><b>Solum</b>&nbsp;&eacute; uma escola especializada em dan&ccedil;as de sal&atilde;o, que tem como miss&atilde;o capacitar o p&uacute;blico jovem em pouco tempo de curso. Transmitimos nossa metodologia de forma simplificada para que voc&ecirc; possa&nbsp;integrar ao grupo e sentir-se confort&aacute;vel em nosso ambiente.<br />\r\nEm geral as turmas s&atilde;o bem animadas, pois&nbsp;s&atilde;o compostas&nbsp;por grande volume de alunos,&nbsp;torna-se um ambiente ideal para quem deseja aumentar o ciclo de amizade.</p>\r\n', NULL, '2016-10-19 20:22:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico_imagens`
--

CREATE TABLE `historico_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `historico_imagens`
--

INSERT INTO `historico_imagens` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(6, 0, '4_20161019182714.jpg', '2016-10-19 20:27:14', '2016-10-19 20:27:14'),
(7, 0, '3_20161019182714.jpg', '2016-10-19 20:27:14', '2016-10-19 20:27:14'),
(8, 0, '5_20161019182714.jpg', '2016-10-19 20:27:14', '2016-10-19 20:27:14'),
(9, 0, '6_20161019182714.jpg', '2016-10-19 20:27:15', '2016-10-19 20:27:15'),
(10, 0, '10_20161019182722.jpg', '2016-10-19 20:27:22', '2016-10-19 20:27:22'),
(11, 0, '9_20161019182722.jpg', '2016-10-19 20:27:22', '2016-10-19 20:27:22'),
(12, 0, '8_20161019182722.jpg', '2016-10-19 20:27:22', '2016-10-19 20:27:22'),
(13, 0, '7_20161019182722.jpg', '2016-10-19 20:27:23', '2016-10-19 20:27:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico_imagens_laterais`
--

CREATE TABLE `historico_imagens_laterais` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `historico_imagens_laterais`
--

INSERT INTO `historico_imagens_laterais` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(3, 0, '11_20161019182604.jpg', '2016-10-19 20:26:04', '2016-10-19 20:26:04'),
(4, 0, 'solumnight1_20161019182617.jpg', '2016-10-19 20:26:17', '2016-10-19 20:26:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `hospedagem`
--

CREATE TABLE `hospedagem` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `hospedagem`
--

INSERT INTO `hospedagem` (`id`, `ordem`, `nome`, `imagem`, `endereco`, `telefone`, `link`, `observacoes`, `created_at`, `updated_at`) VALUES
(1, 1, ' Hotel 4 estrelas - Quality Suites', 'qualitysuites1_20170116233300.jpg', 'Localizado atr&aacute;s da Solum<br />\r\nRua Bela Cintra, 521 - S&atilde;o Paulo - SP&nbsp;<br />\r\nDesconto especial c/ Moacyr 11 3728-0800', '11 3728 0800', 'http://www.atlanticahotels.com.br/hotel/sao-paulo/quality-suites-long-stay-bela-cintra', 'Hotel 4 estrelas, localizado atr&aacute;s da Solum.&nbsp;<br />\r\nTarifa com desconto procurar por Moacyr (informar Solum Escola de Dan&ccedil;a)<br />\r\nContato direto Moacyr:&nbsp;11 99652-5888', '2016-10-19 16:53:09', '2017-01-17 01:44:18'),
(4, 0, 'Hostel - Bee.W', 'hostel-bee_20170116234134.jpg', 'Rua Haddock Lobo, 167<br />\r\nHostel &eacute; uma categoria de hospedagem, aonde voc&ecirc; aluga apenas a cama em um quarto coletivo com diversas pessoas.<br />\r\n&nbsp;', '11-4328-6222', 'http://www.beewtravel.com.br/site/index.php#yt_spotlight3', '', '2017-01-17 01:41:35', '2017-01-17 01:41:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mais_servicos`
--

CREATE TABLE `mais_servicos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `mais_servicos`
--

INSERT INTO `mais_servicos` (`id`, `ordem`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(2, 0, 'CONTRATE PARA EVENTOS', 'contrate-para-eventos', '<p>Somos uma escola que oferece profissionais para todos os p&uacute;blicos, desde festas simples aos mais luxuosos eventos.</p>\r\n\r\n<p>Contatos:<br />\r\n11 3151-2185 | 11 3255-5162<br />\r\nE-mail: contato3@solum.com.br<br />\r\nRua da Consola&ccedil;&atilde;o 1741 / SP</p>\r\n\r\n<p><img src="http://trupe.net/previa-solum/assets/img/editor-upload/solum-public_20161019190838.jpg" /></p>\r\n\r\n<p><strong>Shows e Performances (um dos mais contratados)</strong></p>\r\n\r\n<p>Casal ou casais que apresentam numeros de danca ou improvisam o estilo abordado, mas independente do estilo buscamos sempre excelencia na qualidade da performance. Dentre os ritmos mais solicitados: Tango, Bolero, Zouk, Anos 60, Ritmos Latinos como Salsa e Merengue, Ritmos Brasileiros como Forro, Samba de Gafieira e Samba Rock.</p>\r\n\r\n<p><a href="http://www.youtube.com/watch?v=mmL5mFhfQXM"><img src="http://www.solum.com.br/_imgs/contrate/contrate2.jpg" /></a></p>\r\n\r\n<p><strong>Na Midia, na TV</strong></p>\r\n\r\n<p>Participamos de trabalhos na midia em Programas de Tv ou Materias de Jornais, Revistas e Internet. Por isso que a Solum significa Noticia Atual, significa Moderna e estamos na frente.</p>\r\n\r\n<p><a href="http://www.youtube.com/watch?v=uVjsYHFTSao"><img src="http://www.solum.com.br/_imgs/contrate/contrate3.jpg" /></a></p>\r\n\r\n<p><strong>Animadores</strong></p>\r\n\r\n<p>Um ou mais profissionais que mediante o microfone, levantam a festa atraves de coreografias bem animadas a fim de abrir uma pista ou simplesmente fazer o pessoal levantar da cadeira. Este esta entre os mais economicos e maior indice de satisfacao, pois atraves do talento e capacidade de improviso conseguimos tornar a festa inesquecivel e marcante para os convidados. A longa experiencia neste setor foram imprescindiveis para atingirmos este sucesso de trabalho. Estilos musicais mais bem sucedidos: Latinos, Disco Music e Country.</p>\r\n\r\n<p><a href="http://www.youtube.com/watch?v=pkRPvMg7Ko4" target="_blank"><img src="http://www.solum.com.br/_imgs/contrate/contrate4.jpg" /></a></p>\r\n\r\n<p><strong>Coreografias</strong></p>\r\n\r\n<p>Desenvolvemos coreografias ou aulas para noivos, debutantes ou ocasioes adversas. Em muitos casos o casal deseja surpreender os convidados com algo inusitado ou simplesmente mais glamuroso, e para isso possuimos professores aptos a dar aulas, ou ensinar uma coreografia conforme o tema da festa.&nbsp;<br />\r\n<br />\r\nOs valores variam de acordo com o numero de aulas/ensaios.<br />\r\n<br />\r\nCaso necessite tambem oferecemos contrato do(a) professor(a) para ser o(a) parceiro(a) na data do evento. Durante as aulas o professor(a) sugere todas as dicas como estilos musicais, preparativos da dance, postura, sapatos, expressao, improvisos, dicas de como tornar mais moderno ou tradicional, etc. Tambem relatam experiencias bem sucedidas e outras muito inusitadas.<br />\r\n<a href="http://www.youtube.com/watch?v=pkRPvMg7Ko4" target="_blank">Clique Aqui</a>&nbsp;para assistir um video da Montagem Coreografica do Filme Dirty Dancing</p>\r\n\r\n<p><strong>Personal Dancer</strong></p>\r\n\r\n<p>Servico contratado exclusivamente para atender a necessidade de falta de pares para homens ou mulheres de uma festa. Somos formados por uma equipe jovem e bem treinada com o objetivo de tratar aos convidados de forma respeitosa e proporcionar uma dance muito agradavel.</p>\r\n', '2016-10-19 16:45:20', '2016-10-19 21:08:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mais_servicos_imagens`
--

CREATE TABLE `mais_servicos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `servico_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mais_servicos_imagens_laterais`
--

CREATE TABLE `mais_servicos_imagens_laterais` (
  `id` int(10) UNSIGNED NOT NULL,
  `servico_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_09_23_140911_create_banners_table', 1),
('2016_09_23_142748_create_chamadas_table', 1),
('2016_09_23_162552_create_videos_home_table', 1),
('2016_09_26_145105_create_mais_servicos_table', 1),
('2016_09_26_152801_create_mais_servicos_imagens_laterais_table', 1),
('2016_09_26_173439_create_mais_servicos_imagens_table', 1),
('2016_09_26_205333_create_como_chegar_table', 1),
('2016_09_26_210456_create_hospedagem_table', 1),
('2016_09_26_212045_create_eventos_table', 1),
('2016_09_26_212200_create_turmas_e_aulas_table', 1),
('2016_09_27_142824_create_respostas_automaticas_table', 1),
('2016_09_29_182919_create_historico_table', 1),
('2016_09_29_191134_create_estrutura_table', 1),
('2016_09_29_191138_create_vantagens_table', 1),
('2016_09_30_143339_create_onde_dancar_table', 1),
('2016_09_30_143402_create_onde_comprar_table', 1),
('2016_09_30_190710_create_cursos_regulares_table', 1),
('2016_09_30_193009_create_cursos_intensivos_table', 1),
('2016_09_30_193034_create_cursos_intensivos_chamada_table', 1),
('2016_09_30_200856_create_precos_table', 1),
('2016_09_30_202243_create_promocoes_table', 1),
('2016_10_03_172908_create_ritmos_table', 1),
('2016_10_03_174845_create_ritmos_videos_table', 1),
('2016_10_03_181546_create_professores_table', 1),
('2016_10_03_184629_create_professores_videos_table', 1),
('2016_10_03_184635_create_professores_imagens_table', 1),
('2016_10_03_232520_create_videos_e_fotos_table', 1),
('2016_10_03_234051_create_table_videos_e_fotos_videos_table', 1),
('2016_10_03_235839_create_table_videos_e_fotos_imagens_table', 1),
('2016_10_04_202312_create_niveis_table', 1),
('2016_10_04_223517_create_grade_de_aulas_table', 1),
('2016_10_19_002343_create_avisos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `niveis`
--

CREATE TABLE `niveis` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estrutura_do_curso` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `niveis`
--

INSERT INTO `niveis` (`id`, `ordem`, `titulo`, `estrutura_do_curso`, `created_at`, `updated_at`) VALUES
(2, 5, 'Avançado', 'Para alunos com mais prática e experiência. Para entrar no Avançado, deverá passar por uma avaliação do professor.', '2016-10-19 16:33:56', '2017-03-31 22:13:08'),
(3, 2, 'Intermediário', 'Recomendado para aqueles que possuem conhecimentos mínimos equivalentes a 6 meses de iniciante.', '2017-03-29 16:03:38', '2017-03-31 22:12:44'),
(4, 1, 'Iniciante', 'Básico da estaca zero (observe a data de início, pois a turma pode estar no meio ou final do nível iniciante).', '2017-03-29 16:03:46', '2017-03-31 22:12:32'),
(5, 3, 'Estágio A', 'Para o aluno entrar no nível Estágio A, deverá obter o conhecimento mínimo de 6 meses do nível intermediário.', '2017-03-29 16:03:54', '2017-03-31 22:12:51'),
(6, 4, 'Estágio A2', 'Para o aluno entrar no nível Estágio A2, deverá obter o conhecimento mínimo de 6 meses do nível Estágio A. Esta é uma etapa contínua, aonde o aluno manterá por tempo indeterminado, e só poderá migrar para o nível Avançado mediante a aprovação do Professor.', '2017-03-29 16:03:58', '2017-03-31 22:13:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `onde_comprar`
--

CREATE TABLE `onde_comprar` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `onde_comprar`
--

INSERT INTO `onde_comprar` (`id`, `ordem`, `nome`, `imagem`, `endereco`, `telefone`, `link`, `observacoes`, `created_at`, `updated_at`) VALUES
(2, 0, 'Em construção...', 'icone-logosolumquadradosite3_20170413013309.png', '<p>...aguarde</p>\r\n', '', '', '', '2017-04-13 04:28:26', '2017-04-13 04:33:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `onde_dancar`
--

CREATE TABLE `onde_dancar` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `onde_dancar`
--

INSERT INTO `onde_dancar` (`id`, `ordem`, `nome`, `imagem`, `endereco`, `telefone`, `link`, `observacoes`, `created_at`, `updated_at`) VALUES
(2, 0, 'Em construção...', 'icone-logosolumquadradosite3_20170413013214.png', '<p>..aguarde</p>\r\n', '', '', '', '2017-04-13 04:27:52', '2017-04-13 04:32:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `precos`
--

CREATE TABLE `precos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `precos`
--

INSERT INTO `precos` (`id`, `ordem`, `titulo`, `subtitulo`, `descricao`, `observacoes`, `chamada`, `link`, `created_at`, `updated_at`) VALUES
(1, 1, 'CURSO INTENSIVÃO', 'Descritivo', '<p><strong>PRE&Ccedil;O POPULAR: </strong>A partir de R$50,00 (sem taxa de matr&iacute;cula)</p>\r\n\r\n<p>Curso r&aacute;pido, realizado em um &uacute;nico dia, com m&oacute;dulo de 3 horas (geralmente aos domingos).&nbsp;</p>\r\n\r\n<p><strong>Ideal para:&nbsp;</strong></p>\r\n\r\n<p>* Aqueles que n&atilde;o possuem tempo;&nbsp;</p>\r\n\r\n<p>* Desejam aprender ao menos um pouco para se divertir;</p>\r\n\r\n<p>* Alcan&ccedil;ar uma turma de n&iacute;vel superior;</p>\r\n\r\n<p>* Praticar passos ou adicionar mais repert&oacute;rios</p>\r\n', '<p><strong>ATEN&Ccedil;&Atilde;O:</strong><br />\r\n1&ordm; - &Eacute; poss&iacute;vel optar pagamento via dep&oacute;sito banc&aacute;rio (ita&uacute;, bradesco ou santander), entretanto n&atilde;o se deve esquecer que ap&oacute;s a transa&ccedil;&atilde;o voc&ecirc; dever&aacute; entrar em contato IMEDIATAMENTE, via e-mail ou telefone, caso contr&aacute;rio sua matr&iacute;cula ainda n&atilde;o estar&aacute; efetivada.<br />\r\n2&ordm; - N&atilde;o h&aacute; devolu&ccedil;&atilde;o de valores no caso de aus&ecirc;ncia sem&nbsp;aviso pr&eacute;vio&nbsp;m&iacute;nimo de&nbsp;48 horas.</p>\r\n', 'VEJA AS OPÇÕES DE TURMAS', '#', '2016-10-19 16:40:17', '2017-04-13 03:57:32'),
(2, 2, 'CURSOS REGULARES p/ INICIANTES', 'Descritivo', '<p><strong>TAXA DE MATR&Iacute;CULA:</strong> R$20,00</p>\r\n\r\n<p><strong>INICIANTES<br />\r\nPLANO 1 RITMO: </strong>1&nbsp;vez por Semana (1 Ritmo)</p>\r\n\r\n<p>Plano Mensal: R$119,00 (por pessoa)&nbsp;</p>\r\n\r\n<p>Plano 5 Meses : 5 x R$99,00 (por pessoa),&nbsp;(cheque ou cart&atilde;o)</p>\r\n\r\n<p>A vista 5 Meses: R$455,00 (por pessoa)</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><strong>ATEN&Ccedil;&Atilde;O:</strong></p>\r\n\r\n<p>Este pre&ccedil;o &eacute; v&aacute;lido para os cinco primeiros meses. Ap&oacute;s a mudan&ccedil;a de n&iacute;vel h&aacute; um pequeno reajuste no valor das parcelas.</p>\r\n\r\n<p><strong>A cada 20 aulas o curso muda de hor&aacute;rio. Entenda</strong>:<br />\r\nAp&oacute;s os 5 meses&nbsp;o curso encerra, e caso queira participar do pr&oacute;ximo n&iacute;vel ent&atilde;o dever&aacute; escolher outro hor&aacute;rio e contratar um novo plano.</p>\r\n', 'VEJA A GRADE DE AULAS REGULAR', '#', '2016-10-19 20:41:16', '2017-04-13 04:04:32'),
(3, 3, 'PLANO GLOBAL', 'Descritivo', '<p>Curso de Dan&ccedil;a de Sal&atilde;o Seq&uuml;encial&nbsp;</p>\r\n\r\n<p>(Forr&oacute;, Gafieira, Salsa, Samba Rock e Zouk)&nbsp;(1 x semana)</p>\r\n\r\n<p>Plano Mensal: R$119,00 (por pessoa)</p>\r\n\r\n<p>Plano 5 Meses: 5 x R$99,00 (por pessoa), (cheque ou cart&atilde;o)</p>\r\n\r\n<p>A vista 5 Meses: R$455,00 (por pessoa)</p>\r\n', '<p>Este curso &eacute; exclusivamente para os iniciantes que desejam conhecer os 5 ritmos. &Eacute; ministrado&nbsp;em sequ&ecirc;ncia&nbsp;4 aulas de cada ritmo, sem misturar (em m&eacute;dia 1 ritmos por m&ecirc;s). Caso o aluno j&aacute; saiba quais os ritmos mais se identifica, n&atilde;o ser&aacute; necess&aacute;rio passar pelo curso de Dan&ccedil;a de Sal&atilde;o Sequencial.</p>\r\n\r\n<p>Ressaltamos tamb&eacute;m que a escolha pelo plano de Dan&ccedil;a de Sal&atilde;o Seq&uuml;encial n&atilde;o limita ao aluno a realizar outros cursos a qualquer momento.</p>\r\n\r\n<p>Ap&oacute;s os 5 meses o aluno dever&aacute; escolher qual o ritmo, ou ritmos, que mais se identificou para montar sua pr&oacute;pria modula&ccedil;&atilde;o, indo para um dos planos acima. (Ap&oacute;s o t&eacute;rmino dos 5 meses, tamb&eacute;m poder&aacute; optar em refazer o curso novamente)</p>\r\n', 'MAIORES INFORMAÇÕES', '#', '2016-10-19 20:42:40', '2017-04-13 03:58:10'),
(4, 4, 'AULAS PARTICULARES c/ Professor(a)', 'Descritivo', '<p>Reservadamente aula entre aluno(a) X professor(a), c/ data e hora marcada (dura&ccedil;&atilde;o da aula: 01hora).</p>\r\n\r\n<p>Utilizadas para:</p>\r\n\r\n<p>- Aprimorar dificuldades que em turma n&atilde;o v&ecirc;m desenvolvendo;</p>\r\n\r\n<p>- Acelerar o processo de conte&uacute;do;</p>\r\n\r\n<p>- Ensaio p/ coreografias de casamento, debutantes, eventos, etc.</p>\r\n\r\n<p>- Entre outras;</p>\r\n\r\n<p>1 Aula Individual: R$99,00</p>\r\n\r\n<p>1 Aula Casal: R$139,00&nbsp;</p>\r\n\r\n<p>Pacote de 5 aulas Individual: R$419,00</p>\r\n\r\n<p>Pacote de 5 aulas Casal: R$570,00</p>\r\n', '<p>Obs: A aula somente &eacute; agendada ap&oacute;s o pagamento.</p>\r\n', 'MAIORES INFORMAÇÕES', '#', '2016-10-19 20:44:02', '2017-04-13 03:58:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `professores`
--

CREATE TABLE `professores` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `professores`
--

INSERT INTO `professores` (`id`, `ordem`, `nome`, `slug`, `imagem`, `descricao`, `created_at`, `updated_at`) VALUES
(4, 0, 'Em construção', 'em-construcao', 'logo-layout-oficial-cortetransparente-chapado_20170405062459.png', '<p>Em constru&ccedil;&atilde;o...</p>\r\n', '2017-04-05 09:25:00', '2017-04-05 09:26:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `professores_imagens`
--

CREATE TABLE `professores_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `professor_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `professores_ritmos`
--

CREATE TABLE `professores_ritmos` (
  `id` int(10) UNSIGNED NOT NULL,
  `professor_id` int(10) UNSIGNED NOT NULL,
  `ritmo_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `professores_videos`
--

CREATE TABLE `professores_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `professor_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `promocoes`
--

CREATE TABLE `promocoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `promocoes`
--

INSERT INTO `promocoes` (`id`, `ordem`, `titulo`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 0, 'PROMO&Ccedil;&Atilde;O<br />\r\n<strong>PAGUE 1 E FA&Ccedil;A 2</strong><br />\r\nNOVAS TURMAS INICIANDO', '<p><strong>Aproveite esta oferta rel&acirc;mpago</strong>, matricule-se em 1 turma e escolha mais outra para frequentar gratuitamente.<br />\r\n&nbsp;</p>\r\n\r\n<p>HOMENS = Matricula em 1 turma e pode escolher mais 2 outras&nbsp;<br />\r\n(&uacute;ltimas vagas em poucas turmas)</p>\r\n\r\n<p>MULHERES = Matricule em 1 turma e pode escolher mais uma outra turma.<br />\r\n(&uacute;ltimas vagas em poucas turmas)</p>\r\n\r\n<p>Oferta v&aacute;lida por ordens de matr&iacute;culas</p>\r\n', '2016-10-19 16:40:59', '2017-04-13 04:14:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas_automaticas`
--

CREATE TABLE `respostas_automaticas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `assunto_formulario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assunto_do_e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resposta_automatica` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `respostas_automaticas`
--

INSERT INTO `respostas_automaticas` (`id`, `ordem`, `assunto_formulario`, `assunto_do_e_mail`, `resposta_automatica`, `created_at`, `updated_at`) VALUES
(1, 0, 'Informações', 'SOLUM - Informações', '<p>Exemplo de Respota Autom&aacute;tica.</p>\r\n', '2016-10-19 16:56:19', '2016-10-19 16:56:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ritmos`
--

CREATE TABLE `ritmos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `ritmos`
--

INSERT INTO `ritmos` (`id`, `ordem`, `titulo`, `slug`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 21, 'Zouk', 'zouk', '<p>Zouk est&aacute; em alta&nbsp;-&nbsp;&eacute; uma dan&ccedil;a praticada hoje em todos os continentes, sendo disseminada atrav&eacute;s de profissionais do mundo inteiro.</p>\r\n\r\n<p>A dan&ccedil;a se dilui em diversas vertentes e formas de se dan&ccedil;ar, mas a base para a dan&ccedil;a nunca deixou de ser algo em comum entre essas vertentes, com os giros, movimentos de bra&ccedil;os e cabe&ccedil;a. Se dan&ccedil;a&nbsp;lenta ou&nbsp;muito r&aacute;pida, de acordo com a ess&ecirc;ncia musical, mas sempre explorando a sensualidade<br />\r\n<br />\r\nO zouk &eacute; um estilo bastante presente no Brasil, e com tantos profissionais de nosso pa&iacute;s, apelidam nosso estilo como Brazilian Zouk.<br />\r\nN&atilde;o se deve confundir o estilo musical Zouk com o estilo de dan&ccedil;a Zouk, pois a dan&ccedil;a Zouk&nbsp;abrange a possibilidade de dan&ccedil;ar em muitos estilos musicais como Kizombas, R&amp;B,&nbsp;Reggaeton e mais recentemente mixagens&nbsp;que contenham&nbsp;Black Music, NewAge, Pop e outros.<br />\r\nPode ser considerada uma das dan&ccedil;as mais expressivas pois trabalha muita amplitude de espa&ccedil;o, e desmebra movimenta&ccedil;&otilde;es&nbsp;desde os p&eacute;s at&eacute; a cabe&ccedil;a.</p>\r\n', '2016-10-19 16:27:56', '2017-04-13 04:49:49'),
(2, 18, 'Gafieira', 'gafieira', '<p>Muito forte na parte instrumental, o samba de gafieira originou-se no Rio de Janeiro. Permite dan&ccedil;ar de casal em diversos tipos de m&uacute;sicas (pagode, mpb, choro, bossa-nova, samba-funk&nbsp;entre outros). Um estilo de dan&ccedil;a muito admirado pela beleza dos desenhos&nbsp;de perna da dama e do cavalheiro.</p>\r\n', '2016-10-19 16:28:03', '2016-10-19 20:36:53'),
(3, 17, 'Samba-Rock', 'samba-rock', '<p>Ritmo brasileiro que j&aacute; foi conhecido como sambala&ccedil;o ou samba jovem na &eacute;poca da Jovem Guarda. &Eacute; uma mistura de passos de rock anos 60 com samba. Diferente da gafieira, o samba-rock utiliza muitos entrela&ccedil;amentos de bra&ccedil;os, produzindo efeitos de belos giros entre o casal. No estilo musical Jorge Ben Jor e Seu Jorge s&atilde;o muito utilizados.</p>\r\n', '2016-10-19 16:28:10', '2016-10-19 20:36:35'),
(4, 15, 'Sertanejo Universitário', 'sertanejo-universitario', '<p>Surgiu&nbsp;em sequ&ecirc;ncia de movimentos&nbsp;do sertanejo rom&acirc;ntico, esse estilo j&aacute; n&atilde;o conta com letras t&atilde;o regionais e situa&ccedil;&otilde;es vividas por caipiras (como o Sertanejo Raiz). Geralmente as m&uacute;sicas tratam de assuntos do Sertanejo Rom&acirc;ntico da forma como os jovens veem (assuntos como poligamia e trai&ccedil;&atilde;o).<br />\r\nA Dan&ccedil;a &eacute; recente&nbsp;e adaptada aos compassos musicais&nbsp;mel&oacute;dicos de ritmo lento ou m&eacute;dio, bem diferentes do estilo Country. Proporciona evolu&ccedil;&otilde;es de deslocamento lateral e frontal permitindo uma diversidade de&nbsp;movimentos.</p>\r\n', '2016-10-19 16:28:17', '2016-10-19 20:36:15'),
(5, 14, 'Forró Universitário', 'forro-universitario', '<p>&Eacute; um dos ritmos mais dan&ccedil;ados por todas as faixas et&aacute;rias. O ritmo veio para S&atilde;o Paulo nos anos 60, trazido por Dominguinhos e Luiz Gonzaga. O modo de dan&ccedil;a no Forr&oacute; Universit&aacute;rio &eacute;&nbsp;principalmente em&nbsp;dois-dois, giro simples do cavalheiro e da dama e as marca&ccedil;&otilde;es com vai e v&eacute;ns da variados anglos.</p>\r\n', '2016-10-19 16:28:25', '2016-10-19 20:35:51'),
(6, 13, 'Salsa', 'salsa', '<p>Salsa, em castelhano, significa &quot;tempero&quot;, e a ado&ccedil;&atilde;o do nome quis transmitir a id&eacute;ia de uma m&uacute;sica com &quot;sabor&quot;. A &nbsp;m&uacute;sica hoje chamada salsa &eacute; uma mescla de ritmos afro-caribenhos, tais como o son montuno, o mambo e a rumba cubanos, com a bomba e a plena porto-riquenhas. A salsa nasceu em Cuba, e &eacute; uma esp&eacute;cie de adapta&ccedil;&atilde;o do mambo da d&eacute;cada de 1950. Recebeu ainda influ&ecirc;ncias do merengue (da Rep&uacute;blica Dominicana), do calipso de Trinidad e Tobago, da cumbia colombiana, do rock norte-americano e do reggae jamaicano. Hoje, &eacute; uma mistura de sons e absorve at&eacute; influ&ecirc;ncias de ritmos mais modernos como rap ou techno. A dan&ccedil;a pode ser caracterizada por diversos tempos musicais. Enfim &eacute; uma dan&ccedil;a muito completa, pois possibilita muita criatividade do cavalheiro e da dama, ela d&aacute; bastante&nbsp;liberdade de express&atilde;o corporal&nbsp;mas exige&nbsp;muita t&eacute;cnica.</p>\r\n', '2016-10-19 16:28:31', '2016-10-19 20:35:33'),
(7, 12, 'Bolero', 'bolero', '<p>O bolero tem a sua mais remota origem na Espanha no final do s&eacute;culo XVIII, derivado do fandango. Trata-se de um ritmo lento com uma m&uacute;sica cantada e acompanhada por castanholas e viol&otilde;es. O bolero cubano surge com Tristeza, composto em 1883 por Jos&eacute; Sanchez, sendo posteriormente adoptado pelos mexicanos, e depois por toda a Am&eacute;rica Latina. &Eacute; uma dan&ccedil;a&nbsp;que possibilita rodar muito o sal&atilde;o. Os passos&nbsp;possuem principalmente caminhadas deslocadas&nbsp;e giros com deslizes.<br />\r\nAtualmente utilizam na dan&ccedil;a&nbsp;tanto m&uacute;sicas conservadoras como m&uacute;sicas mel&oacute;dicas que possibilitem a marca&ccedil;&atilde;o do bolero.</p>\r\n', '2016-10-19 16:28:36', '2016-10-19 20:35:21'),
(8, 5, 'Tango', 'tango', '<p>O tango &eacute; um estilo musical e uma dan&ccedil;a proveniente do Cone Sul da Am&eacute;rica do Sul. Em meados do s&eacute;culo XIX, o tango nasceu de uma mistura de v&aacute;rios ritmos dos sub&uacute;rbios de Buenos Aires, mas n&atilde;o se limitou &agrave;s zonas baixas, estendeu-se tamb&eacute;m aos bairros prolet&aacute;rios e passou a ser aceito &quot;nas melhores fam&iacute;lias&quot;, principalmente depois que a dan&ccedil;a teve sucesso na Europa. &Eacute; uma dan&ccedil;a elegante e criativa que permite encenar hist&oacute;rias envolventes entre o casal. Hoje em dia o tango vive, n&atilde;o como o fen&ocirc;meno de massas que o engendrou, mas sem nenhuma d&uacute;vida como elemento identificat&oacute;rio da alma portenha e em permanentes evoca&ccedil;&otilde;es espalhadas por todo o Mundo.<br />\r\n<br />\r\nAtualmente &eacute; poss&iacute;vel dan&ccedil;ar de forma tradicional com musicas conservadoras, ou de forma moderna com as novas m&uacute;sicas eletr&ocirc;nicas reforlumadas pelos argentinos denominando-se Tango Novo. Em uma dan&ccedil;a &eacute; poss&iacute;vel variar os estilos.</p>\r\n', '2016-10-19 16:28:42', '2016-10-19 20:34:00'),
(9, 1, 'Dança de Salão Sequencial', 'danca-de-salao-sequencial', '<p>Este curso &eacute; formado por um conjunto de ritmos:&nbsp;</p>\r\n\r\n<p>Forr&oacute;, Gafieira, Salsa, Samba Rock e Zouk.</p>\r\n\r\n<p>Dan&ccedil;a de Sal&atilde;o Sequencial &eacute; exclusivamente para os iniciantes que desejam conhecer os 5 ritmos. &Eacute; ministrado&nbsp;em sequ&ecirc;ncia&nbsp;4 aulas de cada ritmo, sem misturar (em m&eacute;dia 1 ritmos por m&ecirc;s). A ordem dos ritmos ensinados dever&aacute; ser consultada em nosso atendimento, pois cada turma possui uma sequ&ecirc;ncia.<br />\r\nO aluno poder&aacute; entrar nesta turma em qualquer &eacute;poca do ano, desde que tenha vagas e&nbsp;seja na 1&ordf; aula da sequ&ecirc;ncia de ritmos. Para&nbsp;tomar conhecimento sobre vagas e datas&nbsp;deve-se consultar o cronograma em nosso atendimento, atrav&eacute;s de email ou contato telef&ocirc;nico.<br />\r\n&nbsp;</p>\r\n\r\n<p>Caso o aluno j&aacute; saiba quais os ritmos mais se identifica, n&atilde;o ser&aacute; necess&aacute;rio passar pelo curso de Dan&ccedil;a de Sal&atilde;o Sequencial.&nbsp;Ressaltamos tamb&eacute;m que a escolha pelo plano de Dan&ccedil;a de Sal&atilde;o Sequencial n&atilde;o limita ao aluno a realizar outros cursos a qualquer momento.</p>\r\n\r\n<p>Ap&oacute;s os 5 meses, ou 20 aulas,&nbsp;o aluno dever&aacute; escolher qual o ritmo, ou ritmos, que mais se identificou para montar sua pr&oacute;pria modula&ccedil;&atilde;o.&nbsp;(Ap&oacute;s o t&eacute;rmino dos 5 meses, tamb&eacute;m poder&aacute; optar em refazer o curso)<br />\r\n<br />\r\nEsta turma permite que o aluno(a) entre a qualquer momento no grupo, desde que respeitando a data de um ritmo novo que se renova a cada 4 aulas.</p>\r\n', '2016-10-19 21:16:25', '2016-10-19 21:17:11'),
(10, 16, 'Valsa', 'valsa', '<p>A Valsa &eacute; uma dan&ccedil;a com origem em dan&ccedil;as camponesas tradicionais austr&iacute;acas. Originalmente dan&ccedil;ada como uma das figuras da contradan&ccedil;a, com bra&ccedil;os entrela&ccedil;ados ao n&iacute;vel da cintura. A palavra tem origem no alem&atilde;o Walzen, que significa girar ou deslizar. &Eacute; uma dan&ccedil;a de compasso tern&aacute;rio (3/4) com acento no primeiro tempo e um padr&atilde;o b&aacute;sico de passos-passo-espera, resultando em um deslizar vivamente pelo sal&atilde;o.<br />\r\n<br />\r\nN&atilde;o possu&iacute;mos turmas deste ritmo, entretanto ministramos muitas aulas particulares para pais, noivos, debutantes etc.</p>\r\n', '2016-10-19 21:17:56', '2016-10-19 21:17:56'),
(11, 19, 'Expressão Feminina', 'expressao-feminina', '<p>Aula que desenvolve t&eacute;cnicas para que a dama tenha mais beleza e charme na dan&ccedil;a.</p>\r\n\r\n<p>N&atilde;o possui pr&eacute; requisito ou rela&ccedil;&atilde;o direta com algum estilo de dan&ccedil;a.</p>\r\n\r\n<p>Podem participar qualquer n&iacute;vel e estilo.</p>\r\n\r\n<p>A proposta &eacute; apresentar exerc&iacute;cios para que se possa adquirir maior consci&ecirc;ncia corporal e se soltar mais durante a dan&ccedil;a.</p>\r\n', '2016-10-19 21:18:36', '2016-10-19 21:18:36'),
(12, 20, 'West Coast Swing', 'west-coast-swing', '<p>De&nbsp;origem norte-americana, derivada do LindyHop, caracteriza por ser em linha e movimentos el&aacute;sticos.&nbsp;<br />\r\nComo o estilo &eacute; propor muita ginga, ent&atilde;o possibilita muita improvisa&ccedil;&atilde;o em&nbsp;diversos momentos, possibilitando bastante liberdade individual e express&atilde;o do corpo conforme a m&uacute;sica.<br />\r\nPodem ser dan&ccedil;ados com diversos estilos musicais, e sempre vem acompanhando as tend&ecirc;ncias de cada d&eacute;cada.<br />\r\nAtualmente utiliza-se musicas pop, as mesmas tocadas na r&aacute;dio e at&eacute; discotecas eletr&ocirc;nicas.</p>\r\n', '2016-10-19 21:18:59', '2016-10-19 21:18:59'),
(13, 9, 'Vanera Paulista', 'vanera-paulista', '<p>&Eacute; tamb&eacute;m sertanejo, entretanto caracterizado por movimentos de mais pr&oacute;ximidade, curtos e r&aacute;pidos.</p>\r\n', '2016-10-19 21:19:18', '2017-03-27 21:59:34'),
(14, 10, 'Ritmos', 'ritmos', '<p>&nbsp;</p>\r\n\r\n<p>Aula livre, sem par, desenvolvida para soltar mais o corpo (para homens e mulheres)</p>\r\n', '2016-10-19 21:19:32', '2016-10-19 21:19:32'),
(15, 7, 'Hip Hop', 'hip-hop', '<p>em breve.</p>\r\n', '2016-10-19 21:19:57', '2016-10-19 21:19:57'),
(16, 8, 'Técnicas de Condução Masculina', 'tecnicas-de-conducao-masculina', '<p>Exerc&iacute;cios para desenvolvimento da consist&ecirc;ncia na condu&ccedil;&atilde;o. N&atilde;o &eacute; abordado especificamente um determinado ritmo. Dicas para expressar a linguagem corporal entre ambos.&nbsp;</p>\r\n', '2016-10-19 21:20:13', '2016-10-19 21:20:13'),
(17, 11, 'Jazz', 'jazz', '<p>Curso&nbsp;distinto &agrave; Dan&ccedil;a de Sal&atilde;o.&nbsp;<br />\r\nAjuda na flexibilidade, linhas&nbsp;e elasticidade</p>\r\n', '2016-10-19 21:20:27', '2016-10-19 21:20:27'),
(19, 2, 'Bachata', 'bachata', '<p>&nbsp;</p>\r\n\r\n<p>Dan&ccedil;a-se com m&uacute;sicas de letras rom&acirc;nticas, e tem a sensualidade de influ&ecirc;ncias latinas. Dan&ccedil;a-se no abra&ccedil;o ou atrav&eacute;s das conex&otilde;es das m&atilde;os para cria&ccedil;&otilde;es de movimentos envolventes entre o casal.</p>\r\n', '2016-10-19 21:20:46', '2016-10-19 21:20:46'),
(20, 3, 'Kizomba', 'kizomba', '<p>Dan&ccedil;a conduzida e conectada basicamente pelo abra&ccedil;o.<br />\r\n&Eacute; sensual, e possui um gingado especial do pr&oacute;prio r&iacute;tmo.</p>\r\n', '2016-10-19 21:20:57', '2017-04-13 04:53:19'),
(21, 4, 'Gafieira Aprimoramento', 'gafieira-aprimoramento', '<p>em breve..</p>\r\n', '2016-10-19 21:21:16', '2016-10-19 21:21:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ritmos_videos`
--

CREATE TABLE `ritmos_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ritmo_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `ritmos_videos`
--

INSERT INTO `ritmos_videos` (`id`, `ritmo_id`, `ordem`, `titulo`, `video_tipo`, `video_codigo`, `capa`, `created_at`, `updated_at`) VALUES
(1, 8, 0, 'Exemplo', 'youtube', 'ac2T1AFtRsI', 'ac2T1AFtRsI_20161019143106.jpg', '2016-10-19 16:31:06', '2016-10-19 16:31:06'),
(2, 8, 0, 'Exemplo', 'youtube', 'ac2T1AFtRsI', 'ac2T1AFtRsI_20161019143109.jpg', '2016-10-19 16:31:09', '2016-10-19 16:31:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `turmas_e_aulas`
--

CREATE TABLE `turmas_e_aulas` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aviso` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$jSx199wfnAXYsDYsqMliLuLWYrqKI.NagUicm4dLYEZrYlQArUukW', 'Z9ZkPnqfRPswytkEGhbT5UtkUQ80bVcuWT4lZTFr8VQvnudot4urLxyRhyaT', NULL, '2016-10-19 16:19:00'),
(2, 'Solum', 'contato@solum.com.br', '$2y$10$0oyzZbNy6SyFjuqUyXPq4.pIDWo3YrY.5xcko6nQR6woGRJru2Eay', 'P8k7oYenj6VWqklvUIebtWYJMEIOHyoNvL9zEQtFNqsMpc3ow1WacfYLxNZn', '2016-10-20 21:55:37', '2017-04-13 04:59:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vantagens`
--

CREATE TABLE `vantagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vantagens`
--

INSERT INTO `vantagens` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Sendo um aluno Solum voc&ecirc; tem in&uacute;meras vantagens, veja:</p>\r\n\r\n<p>Estacionamento para alunos mensalistas R$8,00 com manobrista e seguro (exceto domingos ou em dias de bailes e cursos intensivos).</p>\r\n\r\n<p>Alunos matriculados participam de sorteios&nbsp;ou promo&ccedil;&otilde;es&nbsp;peri&oacute;dicas de cursos gratuitos dentro da escola.</p>\r\n\r\n<p>Veja as Promo&ccedil;&otilde;es de Cursos Intensivos com pre&ccedil;os bem especiais (consulte o menu Cursos Intensivos)</p>\r\n\r\n<p>Para acompanhar tudo que acontece em tempo real curta a p&aacute;gina do Facebook: https://www.facebook.com/SolumEscoladeDanca</p>\r\n\r\n<p>Siga nosso Insta: @Solumoficial<br />\r\n&nbsp;</p>\r\n', NULL, '2017-04-13 04:34:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vantagens_imagens`
--

CREATE TABLE `vantagens_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `vantagens_imagens_laterais`
--

CREATE TABLE `vantagens_imagens_laterais` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `videos_e_fotos`
--

CREATE TABLE `videos_e_fotos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `videos_e_fotos`
--

INSERT INTO `videos_e_fotos` (`id`, `ordem`, `data`, `titulo`, `slug`, `capa`, `created_at`, `updated_at`) VALUES
(1, 0, '2016-10', 'DVD - SOLUM 7 ANOS', 'dvd-solum-7-anos', '8_20161020111630.jpg', '2016-10-19 16:29:41', '2016-10-20 13:16:30'),
(2, 0, '2017-04', 'Zouk', 'zouk', 'logo-layout-oficial-cortetransparente-chapado_20170405055840.png', '2017-04-05 08:58:40', '2017-04-05 08:58:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `videos_e_fotos_imagens`
--

CREATE TABLE `videos_e_fotos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `videos_e_fotos_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `videos_e_fotos_imagens`
--

INSERT INTO `videos_e_fotos_imagens` (`id`, `videos_e_fotos_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(11, 1, 0, 'solum6anos-1_20161020112009.jpg', '2016-10-20 13:20:10', '2016-10-20 13:20:10'),
(12, 1, 0, 'solum6anos-2_20161020112009.jpg', '2016-10-20 13:20:10', '2016-10-20 13:20:10'),
(13, 1, 0, 'solum6anos-3_20161020112011.jpg', '2016-10-20 13:20:11', '2016-10-20 13:20:11'),
(14, 1, 0, 'solum6anos-4_20161020112011.jpg', '2016-10-20 13:20:12', '2016-10-20 13:20:12'),
(15, 1, 0, 'solum6anos-5_20161020112013.jpg', '2016-10-20 13:20:14', '2016-10-20 13:20:14'),
(16, 1, 0, 'solum6anos-6_20161020112014.jpg', '2016-10-20 13:20:14', '2016-10-20 13:20:14'),
(17, 1, 0, 'solum6anos-7_20161020112014.jpg', '2016-10-20 13:20:15', '2016-10-20 13:20:15'),
(18, 1, 0, 'solum6anos-8_20161020112015.jpg', '2016-10-20 13:20:16', '2016-10-20 13:20:16'),
(19, 1, 0, 'solum6anos-9_20161020112017.jpg', '2016-10-20 13:20:18', '2016-10-20 13:20:18'),
(20, 1, 0, 'solum6anos-10_20161020112017.jpg', '2016-10-20 13:20:18', '2016-10-20 13:20:18'),
(21, 1, 0, 'solum6anos-11_20161020112019.jpg', '2016-10-20 13:20:19', '2016-10-20 13:20:19'),
(22, 1, 0, 'solum6anos-12_20161020112019.jpg', '2016-10-20 13:20:20', '2016-10-20 13:20:20'),
(23, 1, 0, 'solum6anos-14_20161020112020.jpg', '2016-10-20 13:20:20', '2016-10-20 13:20:20'),
(25, 1, 0, 'solum6anos-15_20161020112021.jpg', '2016-10-20 13:20:22', '2016-10-20 13:20:22'),
(26, 1, 0, 'solum6anos-16_20161020112021.jpg', '2016-10-20 13:20:22', '2016-10-20 13:20:22'),
(27, 1, 0, 'solum6anos-17_20161020112022.jpg', '2016-10-20 13:20:23', '2016-10-20 13:20:23'),
(28, 1, 0, 'solum6anos-18_20161020112023.jpg', '2016-10-20 13:20:23', '2016-10-20 13:20:23'),
(29, 1, 0, 'solum6anos-19_20161020112023.jpg', '2016-10-20 13:20:24', '2016-10-20 13:20:24'),
(30, 1, 0, 'solum6anos-20_20161020112024.jpg', '2016-10-20 13:20:24', '2016-10-20 13:20:24'),
(31, 1, 0, 'solum6anos-21_20161020112025.jpg', '2016-10-20 13:20:25', '2016-10-20 13:20:25'),
(32, 1, 0, 'solum6anos-22_20161020112025.jpg', '2016-10-20 13:20:26', '2016-10-20 13:20:26'),
(33, 1, 0, 'solum6anos-24_20161020112028.jpg', '2016-10-20 13:20:28', '2016-10-20 13:20:28'),
(34, 1, 0, 'solum6anos-23_20161020112028.jpg', '2016-10-20 13:20:29', '2016-10-20 13:20:29'),
(35, 1, 0, 'solum6anos-25_20161020112028.jpg', '2016-10-20 13:20:29', '2016-10-20 13:20:29'),
(36, 1, 0, 'solum6anos-26_20161020112029.jpg', '2016-10-20 13:20:30', '2016-10-20 13:20:30'),
(37, 1, 0, 'solum6anos-27_20161020112029.jpg', '2016-10-20 13:20:30', '2016-10-20 13:20:30'),
(38, 1, 0, 'solum6anos-28_20161020112030.jpg', '2016-10-20 13:20:31', '2016-10-20 13:20:31'),
(39, 1, 0, 'solum6anos-29_20161020112031.jpg', '2016-10-20 13:20:31', '2016-10-20 13:20:31'),
(40, 1, 0, 'solum6anos-30_20161020112031.jpg', '2016-10-20 13:20:32', '2016-10-20 13:20:32'),
(41, 1, 0, 'solum6anos-31_20161020112033.jpg', '2016-10-20 13:20:33', '2016-10-20 13:20:33'),
(42, 1, 0, 'solum6anos-32_20161020112034.jpg', '2016-10-20 13:20:34', '2016-10-20 13:20:34'),
(43, 1, 0, 'solum6anos-33_20161020112034.jpg', '2016-10-20 13:20:34', '2016-10-20 13:20:34'),
(44, 1, 0, 'solum6anos-34_20161020112035.jpg', '2016-10-20 13:20:36', '2016-10-20 13:20:36'),
(45, 1, 0, 'solum6anos-36_20161020112036.jpg', '2016-10-20 13:20:37', '2016-10-20 13:20:37'),
(46, 1, 0, 'solum6anos-35_20161020112036.jpg', '2016-10-20 13:20:37', '2016-10-20 13:20:37'),
(47, 1, 0, 'solum6anos-37_20161020112038.jpg', '2016-10-20 13:20:39', '2016-10-20 13:20:39'),
(48, 1, 0, 'solum6anos-38_20161020112038.jpg', '2016-10-20 13:20:39', '2016-10-20 13:20:39'),
(49, 1, 0, 'solum6anos-39_20161020112039.jpg', '2016-10-20 13:20:39', '2016-10-20 13:20:39'),
(50, 1, 0, 'solum6anos-40_20161020112039.jpg', '2016-10-20 13:20:40', '2016-10-20 13:20:40'),
(51, 1, 0, 'solum6anos-41_20161020112039.jpg', '2016-10-20 13:20:40', '2016-10-20 13:20:40'),
(52, 1, 0, 'solum6anos-42_20161020112042.jpg', '2016-10-20 13:20:42', '2016-10-20 13:20:42'),
(53, 1, 0, 'solum6anos-43_20161020112042.jpg', '2016-10-20 13:20:42', '2016-10-20 13:20:42'),
(54, 1, 0, 'solum6anos-44_20161020112043.jpg', '2016-10-20 13:20:43', '2016-10-20 13:20:43'),
(55, 1, 0, 'solum6anos-45_20161020112043.jpg', '2016-10-20 13:20:44', '2016-10-20 13:20:44'),
(56, 1, 0, 'solum6anos-46_20161020112044.jpg', '2016-10-20 13:20:45', '2016-10-20 13:20:45'),
(57, 1, 0, 'solum6anos-47_20161020112045.jpg', '2016-10-20 13:20:45', '2016-10-20 13:20:45'),
(58, 1, 0, 'solum6anos-48_20161020112045.jpg', '2016-10-20 13:20:46', '2016-10-20 13:20:46'),
(59, 1, 0, 'solum6anos-49_20161020112046.jpg', '2016-10-20 13:20:47', '2016-10-20 13:20:47'),
(60, 1, 0, 'solum6anos-50_20161020112046.jpg', '2016-10-20 13:20:47', '2016-10-20 13:20:47'),
(61, 1, 0, 'solum6anos-51_20161020112047.jpg', '2016-10-20 13:20:48', '2016-10-20 13:20:48'),
(62, 1, 0, 'solum6anos-52_20161020112047.jpg', '2016-10-20 13:20:48', '2016-10-20 13:20:48'),
(63, 1, 0, 'solum6anos-53_20161020112048.jpg', '2016-10-20 13:20:49', '2016-10-20 13:20:49'),
(64, 1, 0, 'solum6anos-54_20161020112050.jpg', '2016-10-20 13:20:50', '2016-10-20 13:20:50'),
(65, 1, 0, 'solum6anos-55_20161020112051.jpg', '2016-10-20 13:20:51', '2016-10-20 13:20:51'),
(66, 1, 0, 'solum6anos-56_20161020112052.jpg', '2016-10-20 13:20:52', '2016-10-20 13:20:52'),
(67, 1, 0, 'solum6anos-57_20161020112053.jpg', '2016-10-20 13:20:54', '2016-10-20 13:20:54'),
(68, 1, 0, 'solum6anos-58_20161020112054.jpg', '2016-10-20 13:20:54', '2016-10-20 13:20:54'),
(69, 1, 0, 'solum6anos-59_20161020112054.jpg', '2016-10-20 13:20:55', '2016-10-20 13:20:55'),
(70, 1, 0, 'solum6anos-60_20161020112056.jpg', '2016-10-20 13:20:57', '2016-10-20 13:20:57'),
(71, 1, 0, 'solum6anos-61_20161020112056.jpg', '2016-10-20 13:20:57', '2016-10-20 13:20:57'),
(72, 1, 0, 'solum6anos-62_20161020112057.jpg', '2016-10-20 13:20:57', '2016-10-20 13:20:57'),
(73, 1, 0, 'solum6anos-63_20161020112058.jpg', '2016-10-20 13:20:58', '2016-10-20 13:20:58'),
(74, 1, 0, 'solum6anos-65_20161020112058.jpg', '2016-10-20 13:20:58', '2016-10-20 13:20:58'),
(75, 1, 0, 'solum6anos-64_20161020112058.jpg', '2016-10-20 13:20:59', '2016-10-20 13:20:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `videos_e_fotos_ritmos`
--

CREATE TABLE `videos_e_fotos_ritmos` (
  `id` int(10) UNSIGNED NOT NULL,
  `videos_e_fotos_id` int(10) UNSIGNED NOT NULL,
  `ritmo_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `videos_e_fotos_ritmos`
--

INSERT INTO `videos_e_fotos_ritmos` (`id`, `videos_e_fotos_id`, `ritmo_id`, `created_at`, `updated_at`) VALUES
(1, 1, 8, NULL, NULL),
(2, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `videos_e_fotos_videos`
--

CREATE TABLE `videos_e_fotos_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `videos_e_fotos_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `videos_e_fotos_videos`
--

INSERT INTO `videos_e_fotos_videos` (`id`, `videos_e_fotos_id`, `ordem`, `titulo`, `video_tipo`, `video_codigo`, `capa`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 'Marcelo Eidy e Jessica Santh', 'youtube', 'R5uIqcQ3z7U', 'R5uIqcQ3z7U_20170405055913.jpg', '2017-04-05 08:59:13', '2017-04-05 08:59:13'),
(3, 2, 2, 'Rúbia Frutuoso e Edgar Fernandes', 'youtube', 'KrIH0BOStW0', 'KrIH0BOStW0_20170405062314.jpg', '2017-04-05 09:23:14', '2017-04-05 09:23:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `videos_home`
--

CREATE TABLE `videos_home` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `videos_home`
--

INSERT INTO `videos_home` (`id`, `ordem`, `titulo`, `video_tipo`, `video_codigo`, `capa`, `created_at`, `updated_at`) VALUES
(5, 1, 'AME A VIDA', 'youtube', 'y2F2xyPCzrY', 'y2F2xyPCzrY_20170405054058.jpg', '2017-04-05 08:40:58', '2017-04-05 08:40:58'),
(6, 2, 'Momentos - A Década Solum', 'youtube', 'fp6OAv7Q324', 'fp6OAv7Q324_20170405054449.jpg', '2017-04-05 08:44:49', '2017-04-05 08:44:49'),
(7, 3, 'Sertanejo', 'youtube', 'EP428CbGsFM', 'EP428CbGsFM_20170405054706.jpg', '2017-04-05 08:47:06', '2017-04-05 08:47:06'),
(8, 4, 'Expressão Corporal', 'youtube', 'nKdtLp45INw', 'nKdtLp45INw_20170405054926.jpg', '2017-04-05 08:49:26', '2017-04-05 08:49:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avisos`
--
ALTER TABLE `avisos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chamadas`
--
ALTER TABLE `chamadas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `como_chegar`
--
ALTER TABLE `como_chegar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cursos_intensivos`
--
ALTER TABLE `cursos_intensivos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cursos_intensivos_chamada`
--
ALTER TABLE `cursos_intensivos_chamada`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cursos_regulares`
--
ALTER TABLE `cursos_regulares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cursos_regulares_imagens_laterais`
--
ALTER TABLE `cursos_regulares_imagens_laterais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estrutura`
--
ALTER TABLE `estrutura`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estrutura_imagens`
--
ALTER TABLE `estrutura_imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estrutura_imagens_laterais`
--
ALTER TABLE `estrutura_imagens_laterais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade_de_aulas`
--
ALTER TABLE `grade_de_aulas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historico`
--
ALTER TABLE `historico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historico_imagens`
--
ALTER TABLE `historico_imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historico_imagens_laterais`
--
ALTER TABLE `historico_imagens_laterais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospedagem`
--
ALTER TABLE `hospedagem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mais_servicos`
--
ALTER TABLE `mais_servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mais_servicos_imagens`
--
ALTER TABLE `mais_servicos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mais_servicos_imagens_servico_id_foreign` (`servico_id`);

--
-- Indexes for table `mais_servicos_imagens_laterais`
--
ALTER TABLE `mais_servicos_imagens_laterais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mais_servicos_imagens_laterais_servico_id_foreign` (`servico_id`);

--
-- Indexes for table `niveis`
--
ALTER TABLE `niveis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `onde_comprar`
--
ALTER TABLE `onde_comprar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `onde_dancar`
--
ALTER TABLE `onde_dancar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `precos`
--
ALTER TABLE `precos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professores`
--
ALTER TABLE `professores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professores_imagens`
--
ALTER TABLE `professores_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `professores_imagens_professor_id_foreign` (`professor_id`);

--
-- Indexes for table `professores_ritmos`
--
ALTER TABLE `professores_ritmos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `professores_ritmos_professor_id_foreign` (`professor_id`),
  ADD KEY `professores_ritmos_ritmo_id_foreign` (`ritmo_id`);

--
-- Indexes for table `professores_videos`
--
ALTER TABLE `professores_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `professores_videos_professor_id_foreign` (`professor_id`);

--
-- Indexes for table `promocoes`
--
ALTER TABLE `promocoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `respostas_automaticas`
--
ALTER TABLE `respostas_automaticas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ritmos`
--
ALTER TABLE `ritmos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ritmos_videos`
--
ALTER TABLE `ritmos_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ritmos_videos_ritmo_id_foreign` (`ritmo_id`);

--
-- Indexes for table `turmas_e_aulas`
--
ALTER TABLE `turmas_e_aulas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vantagens`
--
ALTER TABLE `vantagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vantagens_imagens`
--
ALTER TABLE `vantagens_imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vantagens_imagens_laterais`
--
ALTER TABLE `vantagens_imagens_laterais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos_e_fotos`
--
ALTER TABLE `videos_e_fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos_e_fotos_imagens`
--
ALTER TABLE `videos_e_fotos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_e_fotos_imagens_videos_e_fotos_id_foreign` (`videos_e_fotos_id`);

--
-- Indexes for table `videos_e_fotos_ritmos`
--
ALTER TABLE `videos_e_fotos_ritmos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_e_fotos_ritmos_videos_e_fotos_id_foreign` (`videos_e_fotos_id`),
  ADD KEY `videos_e_fotos_ritmos_ritmo_id_foreign` (`ritmo_id`);

--
-- Indexes for table `videos_e_fotos_videos`
--
ALTER TABLE `videos_e_fotos_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_e_fotos_videos_videos_e_fotos_id_foreign` (`videos_e_fotos_id`);

--
-- Indexes for table `videos_home`
--
ALTER TABLE `videos_home`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avisos`
--
ALTER TABLE `avisos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `chamadas`
--
ALTER TABLE `chamadas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `como_chegar`
--
ALTER TABLE `como_chegar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cursos_intensivos`
--
ALTER TABLE `cursos_intensivos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `cursos_intensivos_chamada`
--
ALTER TABLE `cursos_intensivos_chamada`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cursos_regulares`
--
ALTER TABLE `cursos_regulares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cursos_regulares_imagens_laterais`
--
ALTER TABLE `cursos_regulares_imagens_laterais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `estrutura`
--
ALTER TABLE `estrutura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `estrutura_imagens`
--
ALTER TABLE `estrutura_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `estrutura_imagens_laterais`
--
ALTER TABLE `estrutura_imagens_laterais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `grade_de_aulas`
--
ALTER TABLE `grade_de_aulas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `historico`
--
ALTER TABLE `historico`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `historico_imagens`
--
ALTER TABLE `historico_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `historico_imagens_laterais`
--
ALTER TABLE `historico_imagens_laterais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hospedagem`
--
ALTER TABLE `hospedagem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mais_servicos`
--
ALTER TABLE `mais_servicos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mais_servicos_imagens`
--
ALTER TABLE `mais_servicos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mais_servicos_imagens_laterais`
--
ALTER TABLE `mais_servicos_imagens_laterais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `niveis`
--
ALTER TABLE `niveis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `onde_comprar`
--
ALTER TABLE `onde_comprar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `onde_dancar`
--
ALTER TABLE `onde_dancar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `precos`
--
ALTER TABLE `precos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `professores`
--
ALTER TABLE `professores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `professores_imagens`
--
ALTER TABLE `professores_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `professores_ritmos`
--
ALTER TABLE `professores_ritmos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `professores_videos`
--
ALTER TABLE `professores_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `promocoes`
--
ALTER TABLE `promocoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `respostas_automaticas`
--
ALTER TABLE `respostas_automaticas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ritmos`
--
ALTER TABLE `ritmos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `ritmos_videos`
--
ALTER TABLE `ritmos_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `turmas_e_aulas`
--
ALTER TABLE `turmas_e_aulas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vantagens`
--
ALTER TABLE `vantagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vantagens_imagens`
--
ALTER TABLE `vantagens_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vantagens_imagens_laterais`
--
ALTER TABLE `vantagens_imagens_laterais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `videos_e_fotos`
--
ALTER TABLE `videos_e_fotos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `videos_e_fotos_imagens`
--
ALTER TABLE `videos_e_fotos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `videos_e_fotos_ritmos`
--
ALTER TABLE `videos_e_fotos_ritmos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `videos_e_fotos_videos`
--
ALTER TABLE `videos_e_fotos_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `videos_home`
--
ALTER TABLE `videos_home`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `mais_servicos_imagens`
--
ALTER TABLE `mais_servicos_imagens`
  ADD CONSTRAINT `mais_servicos_imagens_servico_id_foreign` FOREIGN KEY (`servico_id`) REFERENCES `mais_servicos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `mais_servicos_imagens_laterais`
--
ALTER TABLE `mais_servicos_imagens_laterais`
  ADD CONSTRAINT `mais_servicos_imagens_laterais_servico_id_foreign` FOREIGN KEY (`servico_id`) REFERENCES `mais_servicos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `professores_imagens`
--
ALTER TABLE `professores_imagens`
  ADD CONSTRAINT `professores_imagens_professor_id_foreign` FOREIGN KEY (`professor_id`) REFERENCES `professores` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `professores_ritmos`
--
ALTER TABLE `professores_ritmos`
  ADD CONSTRAINT `professores_ritmos_professor_id_foreign` FOREIGN KEY (`professor_id`) REFERENCES `professores` (`id`),
  ADD CONSTRAINT `professores_ritmos_ritmo_id_foreign` FOREIGN KEY (`ritmo_id`) REFERENCES `ritmos` (`id`);

--
-- Limitadores para a tabela `professores_videos`
--
ALTER TABLE `professores_videos`
  ADD CONSTRAINT `professores_videos_professor_id_foreign` FOREIGN KEY (`professor_id`) REFERENCES `professores` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `ritmos_videos`
--
ALTER TABLE `ritmos_videos`
  ADD CONSTRAINT `ritmos_videos_ritmo_id_foreign` FOREIGN KEY (`ritmo_id`) REFERENCES `ritmos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `videos_e_fotos_imagens`
--
ALTER TABLE `videos_e_fotos_imagens`
  ADD CONSTRAINT `videos_e_fotos_imagens_videos_e_fotos_id_foreign` FOREIGN KEY (`videos_e_fotos_id`) REFERENCES `videos_e_fotos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `videos_e_fotos_ritmos`
--
ALTER TABLE `videos_e_fotos_ritmos`
  ADD CONSTRAINT `videos_e_fotos_ritmos_ritmo_id_foreign` FOREIGN KEY (`ritmo_id`) REFERENCES `ritmos` (`id`),
  ADD CONSTRAINT `videos_e_fotos_ritmos_videos_e_fotos_id_foreign` FOREIGN KEY (`videos_e_fotos_id`) REFERENCES `videos_e_fotos` (`id`);

--
-- Limitadores para a tabela `videos_e_fotos_videos`
--
ALTER TABLE `videos_e_fotos_videos`
  ADD CONSTRAINT `videos_e_fotos_videos_videos_e_fotos_id_foreign` FOREIGN KEY (`videos_e_fotos_id`) REFERENCES `videos_e_fotos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
