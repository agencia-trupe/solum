<?php

return [

    'name'        => 'Solum',
    'title'       => 'Solum Escola de Dança',
    'description' => 'A Solum é uma escola especializada em danças de salão, que tem como missão capacitar o público jovem em pouco tempo de curso.',
    'keywords'    => '',
    'share_image' => 'fb.jpg',
    'analytics'   => null

];
