<?php

namespace App\Helpers;

class Tools
{

    public static function navActive($routes, $current)
    {
        if (! is_array($routes)) $routes = [$routes];

        foreach($routes as $route) {
            if (str_is($route, $current)) return true;
        }

        return false;
    }

    public static function videoThumb($tipo, $id)
    {
        if (!file_exists(public_path('assets/img/videos/'))) {
            mkdir(public_path('assets/img/videos/'), 0777, true);
        }

        try {
            if ($tipo == 'vimeo') {

                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
                $thumbURL = isset($hash[0]['thumbnail_large']) ? $hash[0]['thumbnail_large'] : false;

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/videos/'.$filename);

                return $filename;

            } elseif ($tipo == 'youtube') {

                $thumbURL = "http://img.youtube.com/vi/$id/mqdefault.jpg";

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/videos/'.$filename);

                return $filename;

            }
        } catch (\Exception $e) {
            throw new \Exception("Erro ao obter imagem de capa do vídeo de código <strong>${id}</strong> (${tipo}), verifique o código do vídeo.", 1);
        }

        return '';
    }

    public static function telefones($string)
    {
        return array_map(function($telefone) {
            return trim($telefone);
        }, explode(',', $string));
    }

    public static function parseDataCalendario($data)
    {
        $meses = [
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro'
        ];

        $dias = [
            0 => 'domingo',
            1 => 'segunda',
            2 => 'terça',
            3 => 'quarta',
            4 => 'quinta',
            5 => 'sexta',
            6 => 'sábado'
        ];

        $semanaIndex = \Carbon\Carbon::createFromFormat('d/m/Y', $data)->dayOfWeek;

        list($dia, $mes, $ano) = explode('/', $data);

        return [
            'dia'    => $dia,
            'mes'    => $meses[$mes],
            'ano'    => $ano,
            'semana' => $dias[$semanaIndex]
        ];
    }

    public static function dataGaleria($data)
    {
        $meses = [
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro'
        ];

        list($mes, $ano) = explode('/', $data);

        return $meses[$mes] . ' ' . $ano;
    }

    public static function dataEnquete($data)
    {
        $meses = [
            '01' => 'JAN',
            '02' => 'FEV',
            '03' => 'MAR',
            '04' => 'ABR',
            '05' => 'MAI',
            '06' => 'JUN',
            '07' => 'JUL',
            '08' => 'AGO',
            '09' => 'SET',
            '10' => 'OUT',
            '11' => 'NOV',
            '12' => 'DEZ'
        ];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' ' . $meses[$mes] . ' ' . $ano;
    }

    public static function porcentagemDeVotos($id_opcao)
    {
        $opcao = \App\Models\EnqueteOpcoes::find($id_opcao);
        $questao = \App\Models\EnquetePergunta::find($opcao->enquete_pergunta_id);

        $votos = sizeof($opcao->votos);
        $total = sizeof($questao->numeroVotos);
        $retorno = ($total > 0) ? 100 * round(($votos/$total), 2) : 0;
        return $retorno.'%';
    }

    public static function totalDeVotos($id_enquete){
        $questao = \App\Models\EnquetePergunta::find($id_enquete);

        $resultado = sizeof($questao->numeroVotos);

        if($resultado == 1)
            return "1 voto";
        else
            return $resultado." votos";
    }

    public static function votosNaOpcao($id_opcao){
        $opcao = \App\Models\EnqueteOpcoes::find($id_opcao);

        $resultado = sizeof($opcao->votos);

        if($resultado == 1)
            return "1 voto";
        else
            return $resultado." votos";
    }
}
