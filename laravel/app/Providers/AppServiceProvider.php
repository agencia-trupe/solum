<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.common.*', function($view) {
            $view->with([
                'contato'        => \App\Models\Contato::first(),
                'servicosFooter' => \App\Models\Servico::ordenados()->lists('slug', 'titulo'),
                'ritmosFooter'   => \App\Models\Ritmo::ordenados()->lists('slug', 'titulo')
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
