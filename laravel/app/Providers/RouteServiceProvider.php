<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('enquetes', 'App\Models\EnquetePergunta');
		$router->model('avisos', 'App\Models\Aviso');
        $router->model('grade-de-aulas', 'App\Models\Grade');
		$router->model('niveis', 'App\Models\Nivel');
		$router->model('videos-e-fotos', 'App\Models\Galeria');
		$router->model('professores', 'App\Models\Professor');
		$router->model('ritmos', 'App\Models\Ritmo');
		$router->model('promocoes', 'App\Models\Promocao');
		$router->model('precos', 'App\Models\Preco');
		$router->model('chamada', 'App\Models\CursosIntensivosChamada');
		$router->model('cursos-intensivos', 'App\Models\CursoIntensivo');
		$router->model('cursos-regulares', 'App\Models\CursosRegulares');
		$router->model('onde-comprar', 'App\Models\EstabelecimentoCompra');
		$router->model('onde-dancar', 'App\Models\EstabelecimentoDanca');
        $router->model('historico', 'App\Models\Historico');
        $router->model('estrutura', 'App\Models\Estrutura');
		$router->model('vantagens', 'App\Models\Vantagens');
		$router->model('respostas-automaticas', 'App\Models\RespostaAutomatica');
		$router->model('turmas-e-aulas', 'App\Models\AvisoTurmas');
		$router->model('eventos', 'App\Models\Evento');
		$router->model('hospedagem', 'App\Models\Hospedagem');
		$router->model('como-chegar', 'App\Models\ComoChegar');
		$router->model('mais-servicos', 'App\Models\Servico');
		$router->model('banners', 'App\Models\Banner');
        $router->model('chamadas', 'App\Models\Chamadas');

        $router->bind('videos', function($id, $route, $model = null) {
            if (str_is('*home*', $route->getPrefix())) {
                $model = \App\Models\VideoHome::find($id);
            } elseif ($route->hasParameter('ritmos')) {
                $model = \App\Models\VideoRitmo::find($id);
            } elseif ($route->hasParameter('professores')) {
                $model = \App\Models\VideoProfessor::find($id);
            } elseif ($route->hasParameter('videos_e_fotos')) {
                $model = \App\Models\VideoGaleria::find($id);
            }

            return $model ?: abort('404');
        });

        $router->bind('imagens-laterais', function($id, $route, $model = null) {
            if ($route->hasParameter('mais_servicos')) {
                $model = \App\Models\ServicoImagemLateral::find($id);
            } elseif (str_is('*historico*', $route->getPrefix())) {
                $model = \App\Models\HistoricoImagemLateral::find($id);
            } elseif (str_is('*estrutura*', $route->getPrefix())) {
                $model = \App\Models\EstruturaImagemLateral::find($id);
            } elseif (str_is('*vantagens*', $route->getPrefix())) {
                $model = \App\Models\VantagensImagemLateral::find($id);
            } elseif (str_is('*cursos-regulares*', $route->getPrefix())) {
                $model = \App\Models\CursosRegularesImagemLateral::find($id);
            }

            return $model ?: abort('404');
        });

        $router->bind('imagens', function($id, $route, $model = null) {
            if ($route->hasParameter('mais_servicos')) {
                $model = \App\Models\ServicoImagem::find($id);
            } elseif ($route->hasParameter('professores')) {
                $model = \App\Models\ProfessorImagem::find($id);
            } elseif ($route->hasParameter('videos_e_fotos')) {
                $model = \App\Models\GaleriaImagem::find($id);
            } elseif (str_is('*historico*', $route->getPrefix())) {
                $model = \App\Models\HistoricoImagem::find($id);
            } elseif (str_is('*estrutura*', $route->getPrefix())) {
                $model = \App\Models\EstruturaImagem::find($id);
            } elseif (str_is('*vantagens*', $route->getPrefix())) {
                $model = \App\Models\VantagensImagem::find($id);
            }

            return $model ?: abort('404');
        });

        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('professor_slug', function($value) {
            return \App\Models\Professor::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('ritmo_slug', function($value) {
            return \App\Models\Ritmo::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('mais_servicos_slug', function($value) {
            return \App\Models\Servico::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('galeria_slug', function($value) {
            return \App\Models\Galeria::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
