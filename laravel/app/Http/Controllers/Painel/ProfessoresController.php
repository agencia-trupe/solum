<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProfessoresRequest;
use App\Http\Controllers\Controller;

use App\Models\Professor;
use App\Models\Ritmo;

class ProfessoresController extends Controller
{
    public function index()
    {
        $registros = Professor::ordenados()->get();

        return view('painel.professores.index', compact('registros'));
    }

    public function create()
    {
        $ritmos = Ritmo::ordenados()->get();

        return view('painel.professores.create', compact('ritmos'));
    }

    public function store(ProfessoresRequest $request)
    {
        try {

            $input  = $request->except('ritmos');
            $ritmos = ($request->get('ritmos') ?: []);

            if (isset($input['imagem'])) $input['imagem'] = Professor::upload_imagem();

            $registro = Professor::create($input);
            $registro->ritmos()->sync($ritmos);

            return redirect()->route('painel.a-solum.professores.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {
            dd($e);
            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Professor $registro)
    {
        $ritmos = Ritmo::ordenados()->get();

        return view('painel.professores.edit', compact('registro', 'ritmos'));
    }

    public function update(ProfessoresRequest $request, Professor $registro)
    {
        try {

            $input  = $request->except('ritmos');
            $ritmos = ($request->get('ritmos') ?: []);

            if (isset($input['imagem'])) $input['imagem'] = Professor::upload_imagem();

            $registro->update($input);
            $registro->ritmos()->sync($ritmos);

            return redirect()->route('painel.a-solum.professores.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Professor $registro)
    {
        try {

            $registro->ritmos()->detach();
            $registro->delete();
            return redirect()->route('painel.a-solum.professores.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
