<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MaisServicosRequest;
use App\Http\Controllers\Controller;

use App\Models\Servico;

class MaisServicosController extends Controller
{
    public function index()
    {
        $registros = Servico::ordenados()->get();

        return view('painel.mais-servicos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.mais-servicos.create');
    }

    public function store(MaisServicosRequest $request)
    {
        try {

            $input = $request->all();


            Servico::create($input);
            return redirect()->route('painel.mais-servicos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Servico $registro)
    {
        return view('painel.mais-servicos.edit', compact('registro'));
    }

    public function update(MaisServicosRequest $request, Servico $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.mais-servicos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Servico $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.mais-servicos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
