<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProfessoresVideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Professor;
use App\Models\VideoProfessor;
use App\Helpers\Tools;

class ProfessoresVideosController extends Controller
{
    public function index(Professor $professor)
    {
        $registros = VideoProfessor::professor($professor->id)->ordenados()->get();

        return view('painel.professores.videos.index', compact('registros', 'professor'));
    }

    public function create(Professor $professor)
    {
        return view('painel.professores.videos.create', compact('professor'));
    }

    public function store(Professor $professor, ProfessoresVideosRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = Tools::videoThumb($request->get('video_tipo'), $request->get('video_codigo'));

            $professor->videos()->create($input);
            return redirect()->route('painel.a-solum.professores.videos.index', $professor->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withInput()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Professor $professor, VideoProfessor $registro)
    {
        return view('painel.professores.videos.edit', compact('registro', 'professor'));
    }

    public function update(Professor $professor, ProfessoresVideosRequest $request, VideoProfessor $registro)
    {
        try {

            $input = $request->all();
            if ($input['video_codigo'] !== $registro->video_codigo || $input['video_tipo'] !== $registro->video_tipo) {
                $input['capa'] = Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            $registro->update($input);
            return redirect()->route('painel.a-solum.professores.videos.index', $professor->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Professor $professor, VideoProfessor $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.a-solum.professores.videos.index', $professor->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
