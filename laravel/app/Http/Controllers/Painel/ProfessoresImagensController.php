<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProfessoresImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Professor;
use App\Models\ProfessorImagem;
use App\Helpers\CropImage;

class ProfessoresImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/professores/imagens/thumbs/'
        ],
        [
            'width'   => 1280,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/professores/imagens/'
        ]
    ];

    public function index(Professor $professor)
    {
        $imagens = ProfessorImagem::professor($professor->id)->ordenados()->get();

        return view('painel.professores.imagens.index', compact('imagens', 'professor'));
    }

    public function show(Professor $professor, ProfessorImagem $imagem)
    {
        return $imagem;
    }

    public function create(Professor $professor)
    {
        return view('painel.professores.imagens.create', compact('professor'));
    }

    public function store(Professor $professor, ProfessoresImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = $professor->imagens()->create($input);

            $view = view('painel.professores.imagens.imagem', compact('professor', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Professor $professor, ProfessorImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.a-solum.professores.imagens.index', $professor)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Professor $professor)
    {
        try {

            $professor->imagens()->delete();
            return redirect()->route('painel.a-solum.professores.imagens.index', $professor)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
