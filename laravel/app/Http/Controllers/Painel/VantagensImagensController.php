<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VantagensImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\VantagensImagem;
use App\Helpers\CropImage;

class VantagensImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/vantagens/imagens/thumbs/'
        ],
        [
            'width'   => 1280,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/vantagens/imagens/'
        ]
    ];

    public function index()
    {
        $imagens = VantagensImagem::ordenados()->get();

        return view('painel.vantagens.imagens.index', compact('imagens'));
    }

    public function show(VantagensImagem $imagem)
    {
        return $imagem;
    }

    public function store(VantagensImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = VantagensImagem::create($input);

            $view = view('painel.vantagens.imagens.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(VantagensImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.a-solum.vantagens.imagens.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear()
    {
        try {

            VantagensImagem::truncate();
            return redirect()->route('painel.a-solum.vantagens.imagens.index')
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
