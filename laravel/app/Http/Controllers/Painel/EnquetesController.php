<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\EnquetesRequest;

use App\Http\Controllers\Controller;

use App\Models\EnquetePergunta;
use App\Models\EnqueteOpcoes;

class EnquetesController extends Controller
{
	public function index()
	{
		if (request()->has('historico')) {
			$registros = EnquetePergunta::historico()->get();
			$historico = true;
		} else {
			$registros = EnquetePergunta::semvotos()->get();
			$historico = false;
		}

		return view('painel.enquetes.index', compact('registros', 'historico'));
	}

	public function create()
	{
		return view('painel.enquetes.form');
	}

	public function store(EnquetesRequest $request)
	{
		try {

			$enquete = EnquetePergunta::create($request->except('enquete_opcao'));
			$opcoes  = $request->get('enquete_opcao');

			foreach ($opcoes as $key => $value) {
				EnqueteOpcoes::create([
					'enquete_pergunta_id' => $enquete->id,
					'texto'               => $value,
					'ordem'               => $key
				]);
			}

			return redirect()->route('painel.mural.enquetes.index')->with('success', 'Registro adicionado com sucesso.');

		} catch (\Exception $e) {

			return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

		}
	}

	public function show(EnquetePergunta $registro)
	{
		return view('painel.enquetes.show', compact('registro'));
	}

	public function edit(EnquetePergunta $registro)
	{
		if (sizeof($registro->numeroVotos) > 0) {
			return redirect()->route('painel.mural.enquetes.index')->withErrors(['Depois de receber votos a enquete não pode mais ser editada.']);
		}
		return view('painel.enquetes.edit', compact('registro'));
	}

	public function update(EnquetesRequest $request, EnquetePergunta $registro)
	{
		try {

		    $registro->update($request->except('enquete_opcao'));

		    $registro->opcoes()->delete();

		    $opcoes = $request->get('enquete_opcao');

		    foreach ($opcoes as $key => $value) {
		    	EnqueteOpcoes::create([
		    		'enquete_pergunta_id' => $registro->id,
		    		'texto'               => $value,
		    		'ordem'               => $key
		    	]);
		    }

		    return redirect()->route('painel.mural.enquetes.index')->with('success', 'Registro alterado com sucesso.');

		} catch (\Exception $e) {

		    return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

		}
	}

	public function destroy(EnquetePergunta $registro)
	{
		try {

            $registro->delete();
            return redirect()->route('painel.mural.enquetes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
	}

	public function ativar(EnquetePergunta $registro)
	{
		$enquetes = EnquetePergunta::all();
		foreach ($enquetes as $enquete) {
		    $enquete->ativa = '0';
		    $enquete->save();
		}

		$registro->ativa = '1';
		$registro->save();

		return redirect()->route('painel.mural.enquetes.index')->with('success', 'Enquete ativada com sucesso.');
	}
}
