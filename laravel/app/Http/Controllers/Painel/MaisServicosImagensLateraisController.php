<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MaisServicosImagensLateraisRequest;
use App\Http\Controllers\Controller;

use App\Models\Servico;
use App\Models\ServicoImagemLateral;

class MaisServicosImagensLateraisController extends Controller
{
    public function index(Servico $servico)
    {
        $registros = ServicoImagemLateral::servico($servico->id)->ordenados()->get();

        return view('painel.mais-servicos.imagens-laterais.index', compact('registros', 'servico'));
    }

    public function create(Servico $servico)
    {
        return view('painel.mais-servicos.imagens-laterais.create', compact('servico'));
    }

    public function store(Servico $servico, MaisServicosImagensLateraisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ServicoImagemLateral::upload_imagem();

            $servico->imagensLaterais()->create($input);

            return redirect()->route('painel.mais-servicos.imagens-laterais.index', $servico->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Servico $servico, ServicoImagemLateral $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.mais-servicos.imagens-laterais.index', $servico->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
