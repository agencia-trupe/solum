<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VantagensImagensLateraisRequest;
use App\Http\Controllers\Controller;

use App\Models\VantagensImagemLateral;

class VantagensImagensLateraisController extends Controller
{
    public function index()
    {
        $registros = VantagensImagemLateral::ordenados()->get();

        return view('painel.vantagens.imagens-laterais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.vantagens.imagens-laterais.create');
    }

    public function store(VantagensImagensLateraisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = VantagensImagemLateral::upload_imagem();

            VantagensImagemLateral::create($input);

            return redirect()->route('painel.a-solum.vantagens.imagens-laterais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(VantagensImagemLateral $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.a-solum.vantagens.imagens-laterais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
