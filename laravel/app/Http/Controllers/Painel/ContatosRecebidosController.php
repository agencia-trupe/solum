<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebido;
use Carbon\Carbon;

class ContatosRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ContatoRecebido::orderBy('id', 'DESC')->paginate(30);

        return view('painel.contato.recebidos.index', compact('contatosrecebidos'));
    }

    public function show(ContatoRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.recebidos.show', compact('contato'));
    }

    public function destroy(ContatoRecebido $contato)
    {
        try {
            $contato->delete();
            return redirect()->route('painel.contato.recebidos.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);
        }
    }

    public function exportar(Request $request)
    {
        $file = 'solum-contatos_'.date('d-m-Y_His');
        
        $inicio = null;
        $final  = null;

        try {
            if ($request->get('data_inicio') && Carbon::createFromFormat('d/m/Y', $request->get('data_inicio'))) {
                $inicio = Carbon::createFromFormat('d/m/Y', $request->get('data_inicio'))->format('Y-m-d');
            }
            if ($request->get('data_final') && Carbon::createFromFormat('d/m/Y', $request->get('data_final'))) {
                $final = Carbon::createFromFormat('d/m/Y', $request->get('data_final'))->format('Y-m-d');
            }
        } catch (\Exception $e) {
        }

        $registros = ContatoRecebido::orderBy('id', 'DESC');
        if ($inicio) {
            $registros = $registros->whereDate('created_at', '>=', $inicio);
        }
        if ($final) {
            $registros = $registros->whereDate('created_at', '<=', $final);
        }

        \Excel::create($file, function ($excel) use ($registros) {
            $excel->sheet('contatos', function ($sheet) use ($registros) {
                $sheet->fromModel($registros->get([
                    'created_at as data',
                    'nome',
                    'email',
                    'telefone_fixo',
                    'telefone_celular',
                    'assunto',
                    'ritmo',
                    'curso',
                    'mensagem'
                ]));
            });
        })->download('xls');
    }
}
