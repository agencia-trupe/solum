<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EstruturaImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\EstruturaImagem;
use App\Helpers\CropImage;

class EstruturaImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/estrutura/imagens/thumbs/'
        ],
        [
            'width'   => 1280,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/estrutura/imagens/'
        ]
    ];

    public function index()
    {
        $imagens = EstruturaImagem::ordenados()->get();

        return view('painel.estrutura.imagens.index', compact('imagens'));
    }

    public function show(EstruturaImagem $imagem)
    {
        return $imagem;
    }

    public function store(EstruturaImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = EstruturaImagem::create($input);

            $view = view('painel.estrutura.imagens.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(EstruturaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.a-solum.estrutura.imagens.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear()
    {
        try {

            EstruturaImagem::truncate();
            return redirect()->route('painel.a-solum.estrutura.imagens.index')
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
