<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GradeDeAulasRequest;
use App\Http\Controllers\Controller;

use App\Models\Ritmo;
use App\Models\Nivel;
use App\Models\Grade;

use Maatwebsite\Excel\Facades\Excel;

class GradeDeAulasController extends Controller
{
    private $erros = [];
    private $results;

    public function index()
    {
        $grade = Grade::orderBy('id', 'ASC')->get()->groupBy('dia_da_semana');

        return view('painel.grade-de-aulas.index', compact('grade'));
    }

    public function store(GradeDeAulasRequest $request)
    {
        $arquivo = public_path('assets/grade-de-aulas/' . $this->uploadArquivo($request));

        $encode = mb_detect_encoding(file_get_contents($arquivo), ['UTF-8', 'ISO-8859-1'], true);

        Excel::setDelimiter(';');

        if ($encode == 'UTF-8') {
            $this->results = Excel::load($arquivo, function() {}, 'UTF-8')->get();
        } elseif ($encode == 'ISO-8859-1') {
            $this->results = Excel::load($arquivo, function() {}, 'ISO-8859-1')->get();
        } else {
            return redirect()->route('painel.aulas.grade-de-aulas.index')
                    ->withErrors(['Erro ao fazer upload de arquivo: Encoding inválido']);
        }

        if ($this->titulosValidos()) return $this->salvaGrade();

        return redirect()->route('painel.aulas.grade-de-aulas.index')
                ->withErrors(array_merge(['Erro ao fazer upload de arquivo:'], $this->erros));
    }

    public function update(Request $request)
    {
        $data     = $request->all();
        $registro = Grade::findOrFail($data['pk']);

        $registro->update([
            $data['name'] => $data['value']
        ]);

        return $registro;
    }

    public function destroy(Grade $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.grade-de-aulas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function uploadArquivo($request)
    {
        $arquivo = $request->file('arquivo');
        $path = 'assets/grade-de-aulas/';
        $name = str_slug(pathinfo($arquivo->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$arquivo->getClientOriginalExtension();

        $arquivo->move($path, $name);
        return $name;
    }

    public function titulosValidos()
    {
        $results = $this->results;

        $niveis = Nivel::lists('titulo')->toArray();
        $ritmos = Ritmo::lists('titulo')->toArray();

        $rowNum = 1;

        foreach ($results as $row) {
            $ritmo = trim($row->ritmo);
            $nivel = trim($row->nivel);

            if (! in_array($ritmo, $ritmos) && $ritmo != '') {
                $this->erros[] = "Linha ${rowNum} - O Ritmo <strong>${ritmo}</strong> não existe. Cadastre-o no painel.";
            }

            if (! in_array($nivel, $niveis) && $nivel != '') {
                $this->erros [] = "Linha ${rowNum} - O Nível <strong>${nivel}</strong> não existe. Cadastre-o no painel.";
            }

            $rowNum++;
        }

        return count($this->erros) === 0;
    }

    public function salvaGrade()
    {
        try {

            Grade::truncate();

            foreach($this->results as $row) {
                Grade::create([
                    'dia_da_semana' => $row->dia_da_semana ? trim($row->dia_da_semana) : '',
                    'ritmo'         => $row->ritmo ? trim($row->ritmo) : '',
                    'nivel'         => $row->nivel ? trim($row->nivel) : '',
                    'inicio'        => $row->inicio ? trim($row->inicio) : '',
                    'horario'       => $row->horario ? trim($row->horario) : '',
                    'professores'   => $row->professores ? trim($row->professores) : '',
                ]);
            }

            return redirect()->route('painel.aulas.grade-de-aulas.index')->with('success', 'Grade de Aulas atualizada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao salvar Grade de Aulas: '.$e->getMessage()]);

        }
    }

    public function ritmos()
    {
        $arr = [];
        $ritmos = Ritmo::lists('titulo');

        foreach ($ritmos as $ritmo) {
            $arr[$ritmo] = $ritmo;
        }

        return $arr;
    }

    public function niveis()
    {
        $arr = [];
        $niveis = Nivel::lists('titulo');

        foreach ($niveis as $nivel) {
            $arr[$nivel] = $nivel;
        }

        return $arr;
    }
}
