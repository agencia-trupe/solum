<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EstruturaImagensLateraisRequest;
use App\Http\Controllers\Controller;

use App\Models\EstruturaImagemLateral;

class EstruturaImagensLateraisController extends Controller
{
    public function index()
    {
        $registros = EstruturaImagemLateral::ordenados()->get();

        return view('painel.estrutura.imagens-laterais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.estrutura.imagens-laterais.create');
    }

    public function store(EstruturaImagensLateraisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EstruturaImagemLateral::upload_imagem();

            EstruturaImagemLateral::create($input);

            return redirect()->route('painel.a-solum.estrutura.imagens-laterais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(EstruturaImagemLateral $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.a-solum.estrutura.imagens-laterais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
