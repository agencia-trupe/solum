<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EstruturaRequest;
use App\Http\Controllers\Controller;

use App\Models\Estrutura;

class EstruturaController extends Controller
{
    public function index()
    {
        $registro = Estrutura::first();

        return view('painel.estrutura.edit', compact('registro'));
    }

    public function update(EstruturaRequest $request, Estrutura $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.a-solum.estrutura.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
