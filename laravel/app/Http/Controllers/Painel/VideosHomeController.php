<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosHomeRequest;
use App\Http\Controllers\Controller;

use App\Models\VideoHome;
use App\Helpers\Tools;

class VideosHomeController extends Controller
{
    public $limite = 4;

    public function index()
    {
        $registros = VideoHome::ordenados()->get();
        $limiteExcedido = $registros->count() >= $this->limite;

        return view('painel.videos-home.index', compact('registros', 'limiteExcedido'));
    }

    public function create()
    {
        return view('painel.videos-home.create');
    }

    public function store(VideosHomeRequest $request)
    {
        try {
            $limiteExcedido = VideoHome::count() >= $this->limite;
            if ($limiteExcedido) throw new \Exception("O limite de registros foi excedido.", 1);

            $input = $request->all();
            $input['capa'] = Tools::videoThumb($request->get('video_tipo'), $request->get('video_codigo'));

            VideoHome::create($input);
            return redirect()->route('painel.home.videos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withInput()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(VideoHome $registro)
    {
        return view('painel.videos-home.edit', compact('registro'));
    }

    public function update(VideosHomeRequest $request, VideoHome $registro)
    {
        try {

            $input = $request->all();
            if ($input['video_codigo'] !== $registro->video_codigo || $input['video_tipo'] !== $registro->video_tipo) {
                $input['capa'] = Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            $registro->update($input);
            return redirect()->route('painel.home.videos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(VideoHome $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.home.videos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
