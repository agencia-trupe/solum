<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ComoChegarRequest;
use App\Http\Controllers\Controller;

use App\Models\ComoChegar;

class ComoChegarController extends Controller
{
    public function index()
    {
        $registro = ComoChegar::first();

        return view('painel.como-chegar.edit', compact('registro'));
    }

    public function update(ComoChegarRequest $request, ComoChegar $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ComoChegar::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.localizacao.como-chegar.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
