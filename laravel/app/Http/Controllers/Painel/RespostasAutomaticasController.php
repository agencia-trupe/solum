<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RespostasAutomaticasRequest;
use App\Http\Controllers\Controller;

use App\Models\RespostaAutomatica;

class RespostasAutomaticasController extends Controller
{
    public function index()
    {
        $registros = RespostaAutomatica::ordenados()->get();

        return view('painel.respostas-automaticas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.respostas-automaticas.create');
    }

    public function store(RespostasAutomaticasRequest $request)
    {
        try {

            $input = $request->all();


            RespostaAutomatica::create($input);
            return redirect()->route('painel.contato.respostas-automaticas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(RespostaAutomatica $registro)
    {
        return view('painel.respostas-automaticas.edit', compact('registro'));
    }

    public function update(RespostasAutomaticasRequest $request, RespostaAutomatica $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.contato.respostas-automaticas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(RespostaAutomatica $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.contato.respostas-automaticas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
