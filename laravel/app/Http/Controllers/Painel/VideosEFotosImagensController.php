<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosEFotosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Galeria;
use App\Models\GaleriaImagem;
use App\Helpers\CropImage;

class VideosEFotosImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/videos-e-fotos/imagens/thumbs/'
        ],
        [
            'width'   => 310,
            'height'  => null,
            'path'    => 'assets/img/videos-e-fotos/imagens/thumbs-lg/'
        ],
        [
            'width'   => 1280,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/videos-e-fotos/imagens/'
        ]
    ];

    public function index(Galeria $galeria)
    {
        $imagens = GaleriaImagem::galeria($galeria->id)->ordenados()->get();

        return view('painel.videos-e-fotos.imagens.index', compact('imagens', 'galeria'));
    }

    public function show(Galeria $galeria, GaleriaImagem $imagem)
    {
        return $imagem;
    }

    public function create(Galeria $galeria)
    {
        return view('painel.videos-e-fotos.imagens.create', compact('galeria'));
    }

    public function store(Galeria $galeria, VideosEFotosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = $galeria->imagens()->create($input);

            $view = view('painel.videos-e-fotos.imagens.imagem', compact('galeria', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Galeria $galeria, GaleriaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.a-solum.videos-e-fotos.imagens.index', $galeria)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Galeria $galeria)
    {
        try {

            $galeria->imagens()->delete();
            return redirect()->route('painel.a-solum.videos-e-fotos.imagens.index', $galeria)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
