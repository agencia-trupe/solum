<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OndeComprarRequest;
use App\Http\Controllers\Controller;

use App\Models\EstabelecimentoCompra;

class OndeComprarController extends Controller
{
    public function index()
    {
        $registros = EstabelecimentoCompra::ordenados()->get();

        return view('painel.onde-comprar.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.onde-comprar.create');
    }

    public function store(OndeComprarRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EstabelecimentoCompra::upload_imagem();

            EstabelecimentoCompra::create($input);
            return redirect()->route('painel.mural.onde-comprar.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(EstabelecimentoCompra $registro)
    {
        return view('painel.onde-comprar.edit', compact('registro'));
    }

    public function update(OndeComprarRequest $request, EstabelecimentoCompra $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EstabelecimentoCompra::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.mural.onde-comprar.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(EstabelecimentoCompra $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.mural.onde-comprar.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
