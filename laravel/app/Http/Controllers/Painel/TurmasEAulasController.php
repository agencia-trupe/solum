<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TurmasEAulasRequest;
use App\Http\Controllers\Controller;

use App\Models\AvisoTurmas;

class TurmasEAulasController extends Controller
{
    public function index()
    {
        $registros = AvisoTurmas::ordenados()->get();

        return view('painel.turmas-e-aulas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.turmas-e-aulas.create');
    }

    public function store(TurmasEAulasRequest $request)
    {
        try {

            $input = $request->all();


            AvisoTurmas::create($input);
            return redirect()->route('painel.calendario.turmas-e-aulas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AvisoTurmas $registro)
    {
        return view('painel.turmas-e-aulas.edit', compact('registro'));
    }

    public function update(TurmasEAulasRequest $request, AvisoTurmas $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.calendario.turmas-e-aulas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AvisoTurmas $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.calendario.turmas-e-aulas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
