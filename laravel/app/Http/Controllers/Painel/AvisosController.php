<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AvisosRequest;
use App\Http\Controllers\Controller;

use App\Models\Aviso;

class AvisosController extends Controller
{
    public function index()
    {
        $registros = Aviso::ordenados()->get();

        return view('painel.avisos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.avisos.create')->with([
            'categorias' => Aviso::categorias()
        ]);
    }

    public function store(AvisosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['notificacao']) && $input['notificacao'] != '') {
                $this->enviaNotificacao(
                    Aviso::categorias()[$input['categoria']],
                    $input['notificacao']
                );
            }

            Aviso::create($input);

            return redirect()->route('painel.mural.avisos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function edit(Aviso $registro)
    {
        $categorias = Aviso::categorias();
        return view('painel.avisos.edit', compact('registro', 'categorias'));
    }

    public function update(AvisosRequest $request, Aviso $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.mural.avisos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function destroy(Aviso $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.mural.avisos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    private function enviaNotificacao($titulo, $mensagem)
    {
        $headers = [
            'Authorization: key=AAAAPvrRvt4:APA91bH7HRjixBchx1DcxyoW3UI7xTFELPT9t0g1kY4S0n7jkYgFVfwTc_mzhcrQHMbRGHa-vbVabHZM5xw9JOA-UljyHzbN9Lu6ij0DpVDmnWeo1Fz5E-oao9ZXWHT8XQPqZYE9lWsL',
            'Content-Type: application/json'
        ];

        $data = [
            'notification' => [
                'title' => $titulo,
                'body'  => $mensagem
            ],
            'to'           => '/topics/avisos',
            'priority'     => 'high'
        ];

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result);

        if (!isset($json->message_id)) {
            throw new \Exception('Erro ao enviar notificação. Tente novamente.', 1);
        }
    }

}
