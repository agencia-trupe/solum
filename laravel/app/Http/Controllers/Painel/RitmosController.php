<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RitmosRequest;
use App\Http\Controllers\Controller;

use App\Models\Ritmo;

class RitmosController extends Controller
{
    public function index()
    {
        $registros = Ritmo::ordenados()->get();

        return view('painel.ritmos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.ritmos.create');
    }

    public function store(RitmosRequest $request)
    {
        try {

            $input = $request->all();


            Ritmo::create($input);
            return redirect()->route('painel.aulas.ritmos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Ritmo $registro)
    {
        return view('painel.ritmos.edit', compact('registro'));
    }

    public function update(RitmosRequest $request, Ritmo $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.aulas.ritmos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Ritmo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.ritmos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
