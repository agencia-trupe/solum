<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CursosIntensivosRequest;
use App\Http\Controllers\Controller;

use App\Models\CursoIntensivo;

class CursosIntensivosController extends Controller
{
    public function index()
    {
        $registros = CursoIntensivo::ordenados()->get();

        return view('painel.cursos-intensivos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cursos-intensivos.create');
    }

    public function store(CursosIntensivosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CursoIntensivo::upload_imagem();

            CursoIntensivo::create($input);
            return redirect()->route('painel.aulas.cursos-intensivos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(CursoIntensivo $registro)
    {
        return view('painel.cursos-intensivos.edit', compact('registro'));
    }

    public function update(CursosIntensivosRequest $request, CursoIntensivo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CursoIntensivo::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.aulas.cursos-intensivos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(CursoIntensivo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.cursos-intensivos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
