<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HistoricoImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\HistoricoImagem;
use App\Helpers\CropImage;

class HistoricoImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/historico/imagens/thumbs/'
        ],
        [
            'width'   => 1280,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/historico/imagens/'
        ]
    ];

    public function index()
    {
        $imagens = HistoricoImagem::ordenados()->get();

        return view('painel.historico.imagens.index', compact('imagens'));
    }

    public function show(HistoricoImagem $imagem)
    {
        return $imagem;
    }

    public function store(HistoricoImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = HistoricoImagem::create($input);

            $view = view('painel.historico.imagens.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(HistoricoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.a-solum.historico.imagens.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear()
    {
        try {

            HistoricoImagem::truncate();
            return redirect()->route('painel.a-solum.historico.imagens.index')
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
