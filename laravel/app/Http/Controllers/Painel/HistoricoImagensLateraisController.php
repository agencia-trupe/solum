<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HistoricoImagensLateraisRequest;
use App\Http\Controllers\Controller;

use App\Models\HistoricoImagemLateral;

class HistoricoImagensLateraisController extends Controller
{
    public function index()
    {
        $registros = HistoricoImagemLateral::ordenados()->get();

        return view('painel.historico.imagens-laterais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.historico.imagens-laterais.create');
    }

    public function store(HistoricoImagensLateraisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = HistoricoImagemLateral::upload_imagem();

            HistoricoImagemLateral::create($input);

            return redirect()->route('painel.a-solum.historico.imagens-laterais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(HistoricoImagemLateral $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.a-solum.historico.imagens-laterais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
