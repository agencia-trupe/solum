<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HospedagemRequest;
use App\Http\Controllers\Controller;

use App\Models\Hospedagem;

class HospedagemController extends Controller
{
    public function index()
    {
        $registros = Hospedagem::ordenados()->get();

        return view('painel.hospedagem.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.hospedagem.create');
    }

    public function store(HospedagemRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Hospedagem::upload_imagem();

            Hospedagem::create($input);
            return redirect()->route('painel.localizacao.hospedagem.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Hospedagem $registro)
    {
        return view('painel.hospedagem.edit', compact('registro'));
    }

    public function update(HospedagemRequest $request, Hospedagem $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Hospedagem::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.localizacao.hospedagem.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Hospedagem $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.localizacao.hospedagem.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
