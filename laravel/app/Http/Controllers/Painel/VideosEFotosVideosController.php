<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosEFotosVideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Galeria;
use App\Models\VideoGaleria;
use App\Helpers\Tools;

class VideosEFotosVideosController extends Controller
{
    public function index(Galeria $galeria)
    {
        $registros = VideoGaleria::galeria($galeria->id)->ordenados()->get();

        return view('painel.videos-e-fotos.videos.index', compact('registros', 'galeria'));
    }

    public function create(Galeria $galeria)
    {
        return view('painel.videos-e-fotos.videos.create', compact('galeria'));
    }

    public function store(Galeria $galeria, VideosEFotosVideosRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = Tools::videoThumb($request->get('video_tipo'), $request->get('video_codigo'));

            $galeria->videos()->create($input);
            return redirect()->route('painel.a-solum.videos-e-fotos.videos.index', $galeria->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withInput()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Galeria $galeria, VideoGaleria $registro)
    {
        return view('painel.videos-e-fotos.videos.edit', compact('registro', 'galeria'));
    }

    public function update(Galeria $galeria, VideosEFotosVideosRequest $request, VideoGaleria $registro)
    {
        try {

            $input = $request->all();
            if ($input['video_codigo'] !== $registro->video_codigo || $input['video_tipo'] !== $registro->video_tipo) {
                $input['capa'] = Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            $registro->update($input);
            return redirect()->route('painel.a-solum.videos-e-fotos.videos.index', $galeria->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Galeria $galeria, VideoGaleria $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.a-solum.videos-e-fotos.videos.index', $galeria->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
