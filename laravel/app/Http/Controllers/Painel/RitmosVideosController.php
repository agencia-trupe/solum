<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RitmosVideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Ritmo;
use App\Models\VideoRitmo;
use App\Helpers\Tools;

class RitmosVideosController extends Controller
{
    public $limite = 4;

    public function index(Ritmo $ritmo)
    {
        $registros = VideoRitmo::ritmo($ritmo->id)->ordenados()->get();
        $limiteExcedido = $registros->count() >= $this->limite;

        return view('painel.ritmos.videos.index', compact('registros', 'ritmo', 'limiteExcedido'));
    }

    public function create(Ritmo $ritmo)
    {
        return view('painel.ritmos.videos.create', compact('ritmo'));
    }

    public function store(Ritmo $ritmo, RitmosVideosRequest $request)
    {
        try {
            $limiteExcedido = VideoRitmo::ritmo($ritmo->id)->count() >= $this->limite;
            if ($limiteExcedido) throw new \Exception("O limite de registros foi excedido.", 1);

            $input = $request->all();
            $input['capa'] = Tools::videoThumb($request->get('video_tipo'), $request->get('video_codigo'));

            $ritmo->videos()->create($input);
            return redirect()->route('painel.aulas.ritmos.videos.index', $ritmo->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withInput()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Ritmo $ritmo, VideoRitmo $registro)
    {
        return view('painel.ritmos.videos.edit', compact('registro', 'ritmo'));
    }

    public function update(Ritmo $ritmo, RitmosVideosRequest $request, VideoRitmo $registro)
    {
        try {

            $input = $request->all();
            if ($input['video_codigo'] !== $registro->video_codigo || $input['video_tipo'] !== $registro->video_tipo) {
                $input['capa'] = Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            $registro->update($input);
            return redirect()->route('painel.aulas.ritmos.videos.index', $ritmo->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Ritmo $ritmo, VideoRitmo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.ritmos.videos.index', $ritmo->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
