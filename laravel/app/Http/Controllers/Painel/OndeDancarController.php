<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OndeDancarRequest;
use App\Http\Controllers\Controller;

use App\Models\EstabelecimentoDanca;

class OndeDancarController extends Controller
{
    public function index()
    {
        $registros = EstabelecimentoDanca::ordenados()->get();

        return view('painel.onde-dancar.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.onde-dancar.create');
    }

    public function store(OndeDancarRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EstabelecimentoDanca::upload_imagem();

            EstabelecimentoDanca::create($input);
            return redirect()->route('painel.mural.onde-dancar.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(EstabelecimentoDanca $registro)
    {
        return view('painel.onde-dancar.edit', compact('registro'));
    }

    public function update(OndeDancarRequest $request, EstabelecimentoDanca $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EstabelecimentoDanca::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.mural.onde-dancar.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(EstabelecimentoDanca $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.mural.onde-dancar.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
