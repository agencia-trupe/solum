<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosEFotosRequest;
use App\Http\Controllers\Controller;

use App\Models\Galeria;
use App\Models\Ritmo;

class VideosEFotosController extends Controller
{
    public function index()
    {
        $registros = Galeria::ordenados()->get();

        return view('painel.videos-e-fotos.index', compact('registros'));
    }

    public function create()
    {
        $ritmos = Ritmo::ordenados()->get();

        return view('painel.videos-e-fotos.create', compact('ritmos'));
    }

    public function store(VideosEFotosRequest $request)
    {
        try {

            $input  = $request->except('ritmos');
            $ritmos = ($request->get('ritmos') ?: []);

            if (isset($input['capa'])) $input['capa'] = Galeria::upload_capa();

            $registro = Galeria::create($input);
            $registro->ritmos()->sync($ritmos);

            return redirect()->route('painel.a-solum.videos-e-fotos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Galeria $registro)
    {
        $ritmos = Ritmo::ordenados()->get();

        return view('painel.videos-e-fotos.edit', compact('registro', 'ritmos'));
    }

    public function update(VideosEFotosRequest $request, Galeria $registro)
    {
        try {

            $input  = $request->except('ritmos');
            $ritmos = ($request->get('ritmos') ?: []);

            if (isset($input['capa'])) $input['capa'] = Galeria::upload_capa();

            $registro->update($input);
            $registro->ritmos()->sync($ritmos);

            return redirect()->route('painel.a-solum.videos-e-fotos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Galeria $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.a-solum.videos-e-fotos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
