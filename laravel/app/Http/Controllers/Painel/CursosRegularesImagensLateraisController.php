<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CursosRegularesImagensLateraisRequest;
use App\Http\Controllers\Controller;

use App\Models\CursosRegularesImagemLateral;

class CursosRegularesImagensLateraisController extends Controller
{
    public function index()
    {
        $registros = CursosRegularesImagemLateral::ordenados()->get();

        return view('painel.cursos-regulares.imagens-laterais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cursos-regulares.imagens-laterais.create');
    }

    public function store(CursosRegularesImagensLateraisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CursosRegularesImagemLateral::upload_imagem();

            CursosRegularesImagemLateral::create($input);

            return redirect()->route('painel.aulas.cursos-regulares.imagens-laterais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(CursosRegularesImagemLateral $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.cursos-regulares.imagens-laterais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
