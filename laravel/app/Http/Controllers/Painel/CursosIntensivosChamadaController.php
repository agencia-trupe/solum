<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CursosIntensivosChamadaRequest;
use App\Http\Controllers\Controller;

use App\Models\CursosIntensivosChamada;

class CursosIntensivosChamadaController extends Controller
{
    public function index()
    {
        $registro = CursosIntensivosChamada::first();

        return view('painel.cursos-intensivos-chamada.edit', compact('registro'));
    }

    public function update(CursosIntensivosChamadaRequest $request, CursosIntensivosChamada $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.aulas.cursos-intensivos.chamada.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
