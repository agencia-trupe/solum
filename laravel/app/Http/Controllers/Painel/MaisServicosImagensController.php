<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MaisServicosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Servico;
use App\Models\ServicoImagem;
use App\Helpers\CropImage;

class MaisServicosImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/mais-servicos/imagens/thumbs/'
        ],
        [
            'width'   => 1280,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/mais-servicos/imagens/'
        ]
    ];

    public function index(Servico $servico)
    {
        $imagens = ServicoImagem::servico($servico->id)->ordenados()->get();

        return view('painel.mais-servicos.imagens.index', compact('imagens', 'servico'));
    }

    public function show(Servico $servico, ServicoImagem $imagem)
    {
        return $imagem;
    }

    public function create(Servico $servico)
    {
        return view('painel.mais-servicos.imagens.create', compact('servico'));
    }

    public function store(Servico $servico, MaisServicosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = $servico->imagens()->create($input);

            $view = view('painel.mais-servicos.imagens.imagem', compact('servico', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Servico $servico, ServicoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.mais-servicos.imagens.index', $servico)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Servico $servico)
    {
        try {

            $servico->imagens()->delete();
            return redirect()->route('painel.mais-servicos.imagens.index', $servico)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
