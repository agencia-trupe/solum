<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VantagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Vantagens;

class VantagensController extends Controller
{
    public function index()
    {
        $registro = Vantagens::first();

        return view('painel.vantagens.edit', compact('registro'));
    }

    public function update(VantagensRequest $request, Vantagens $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.a-solum.vantagens.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
