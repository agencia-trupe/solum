<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PromocoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Promocao;

class PromocoesController extends Controller
{
    public function index()
    {
        $registros = Promocao::ordenados()->get();

        return view('painel.promocoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.promocoes.create');
    }

    public function store(PromocoesRequest $request)
    {
        try {

            $input = $request->all();


            Promocao::create($input);
            return redirect()->route('painel.aulas.promocoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Promocao $registro)
    {
        return view('painel.promocoes.edit', compact('registro'));
    }

    public function update(PromocoesRequest $request, Promocao $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.aulas.promocoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Promocao $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.promocoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
