<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CursosRegularesRequest;
use App\Http\Controllers\Controller;

use App\Models\CursosRegulares;

class CursosRegularesController extends Controller
{
    public function index()
    {
        $registro = CursosRegulares::first();

        return view('painel.cursos-regulares.edit', compact('registro'));
    }

    public function update(CursosRegularesRequest $request, CursosRegulares $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.aulas.cursos-regulares.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
