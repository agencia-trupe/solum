<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NiveisRequest;
use App\Http\Controllers\Controller;

use App\Models\Nivel;

class NiveisController extends Controller
{
    public function index()
    {
        $registros = Nivel::ordenados()->get();

        return view('painel.niveis.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.niveis.create');
    }

    public function store(NiveisRequest $request)
    {
        try {

            $input = $request->all();


            Nivel::create($input);
            return redirect()->route('painel.aulas.grade-de-aulas.niveis.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Nivel $registro)
    {
        return view('painel.niveis.edit', compact('registro'));
    }

    public function update(NiveisRequest $request, Nivel $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.aulas.grade-de-aulas.niveis.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Nivel $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.grade-de-aulas.niveis.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
