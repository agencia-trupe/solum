<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PrecosRequest;
use App\Http\Controllers\Controller;

use App\Models\Preco;

class PrecosController extends Controller
{
    public function index()
    {
        $registros = Preco::ordenados()->get();

        return view('painel.precos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.precos.create');
    }

    public function store(PrecosRequest $request)
    {
        try {

            $input = $request->all();


            Preco::create($input);
            return redirect()->route('painel.aulas.precos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Preco $registro)
    {
        return view('painel.precos.edit', compact('registro'));
    }

    public function update(PrecosRequest $request, Preco $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.aulas.precos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Preco $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aulas.precos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
