<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Professor;

class ProfessoresController extends Controller
{
    public function index()
    {
        return view('frontend.a-solum.professores.index')->with([
            'professores' => Professor::ordenados()->get()
        ]);
    }

    public function show(Professor $professor)
    {
        return view('frontend.a-solum.professores.show')->with([
            'professor'         => $professor,
            'outrosProfessores' => Professor::where('id', '!=', $professor->id)
                    ->orderByRaw("RAND()")->take(6)->get()
        ]);
    }
}
