<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Ritmo;
use App\Models\Chamadas;
use App\Models\Evento;
use App\Models\Aviso;
use App\Models\VideoHome;
use App\Models\Hospedagem;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home')->with([
            'banners'    => Banner::ordenados()->get(),
            'ritmos'     => Ritmo::ordenados()->lists('slug', 'titulo'),
            'chamadas'   => Chamadas::first(),
            'eventos'    => Evento::ordenados()->take(3)->get(),
            'avisos'     => Aviso::ordenados()->take(3)->get(),
            'videos'     => VideoHome::ordenados()->take(4)->get(),
            'hospedagem' => Hospedagem::ordenados()->take(3)->get()
        ]);
    }
}
