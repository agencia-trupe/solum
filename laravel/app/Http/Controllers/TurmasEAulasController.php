<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AvisoTurmas;

class TurmasEAulasController extends Controller
{
    public function index()
    {
        return view('frontend.calendario.turmas-e-aulas')->with([
            'avisos' => AvisoTurmas::ordenados()->get(),
        ]);
    }
}
