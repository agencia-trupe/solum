<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Vantagens;
use App\Models\VantagensImagem;
use App\Models\VantagensImagemLateral;

class VantagensController extends Controller
{
    public function index()
    {
        return view('frontend.a-solum.vantagens')->with([
            'texto'           => Vantagens::first(),
            'imagens'         => VantagensImagem::ordenados()->get(),
            'imagensLaterais' => VantagensImagemLateral::ordenados()->get()
        ]);
    }
}
