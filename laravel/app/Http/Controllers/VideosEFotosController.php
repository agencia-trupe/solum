<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Galeria;
use App\Models\Ritmo;

class VideosEFotosController extends Controller
{
    public function index()
    {
        return view('frontend.a-solum.videos-e-fotos.index')->with([
            'galerias' => Galeria::ordenados()->get(),
            'ritmos'   => Ritmo::ordenados()->lists('titulo', 'slug')
        ]);
    }

    public function busca(Request $request)
    {
        $termo = $request->get('palavra-chave');

        if (!$termo) return redirect()->route('a-solum.videos-e-fotos.index');

        return view('frontend.a-solum.videos-e-fotos.index')->with([
            'galerias' => Galeria::where('titulo', 'LIKE', '%'.$termo.'%')->ordenados()->get(),
            'ritmos'   => Ritmo::ordenados()->lists('titulo', 'slug'),
            'termo'    => $termo
        ]);
    }

    public function ritmo(Ritmo $ritmo)
    {
        if (!$ritmo->exists) return redirect()->route('a-solum.videos-e-fotos.index');

        return view('frontend.a-solum.videos-e-fotos.index')->with([
            'galerias' => Galeria::whereHas('ritmos', function ($q) use ($ritmo) {
                $q->where('ritmo_id', $ritmo->id);
            })->ordenados()->get(),
            'ritmos'   => Ritmo::ordenados()->lists('titulo', 'slug'),
            'ritmo'    => $ritmo->slug
        ]);
    }

    public function show(Galeria $galeria)
    {
        return view('frontend.a-solum.videos-e-fotos.show')->with([
            'galeria' => $galeria
        ]);
    }
}
