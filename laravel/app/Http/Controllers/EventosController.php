<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Evento;

class EventosController extends Controller
{
    public function index()
    {
        return view('frontend.calendario.eventos')->with([
            'eventos' => Evento::ordenados()->get(),
        ]);
    }
}
