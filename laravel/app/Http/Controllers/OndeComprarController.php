<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\EstabelecimentoCompra;

class OndeComprarController extends Controller
{
    public function index()
    {
        return view('frontend.mural.onde-comprar')->with([
            'estabelecimentos' => EstabelecimentoCompra::ordenados()->get(),
        ]);
    }
}
