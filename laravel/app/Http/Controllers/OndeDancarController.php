<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\EstabelecimentoDanca;

class OndeDancarController extends Controller
{
    public function index()
    {
        return view('frontend.mural.onde-dancar')->with([
            'estabelecimentos' => EstabelecimentoDanca::ordenados()->get(),
        ]);
    }
}
