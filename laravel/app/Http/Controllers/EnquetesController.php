<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\EnquetePergunta;
use App\Models\EnqueteOpcoes;
use App\Models\EnqueteVotos;

class EnquetesController extends Controller
{
    public function index()
    {
        $enquete   = EnquetePergunta::ativa();
        $historico = EnquetePergunta::historico()->get();

        return view('frontend.mural.enquetes.index', compact('enquete', 'historico'));
    }

    public function resultados(EnquetePergunta $enquete)
    {
        $historico = EnquetePergunta::historico()->get();

        return view('frontend.mural.enquetes.resultados', compact('enquete', 'historico'));
    }

    public function formVotar($opcao = null)
    {
        return view('frontend.mural.enquetes._form', compact('opcao'));
    }

    public function votar(Request $request)
    {
        $email = $request->get('emailVoto');
        $voto  = $request->get('opcaoVoto');

        if (!$email) {
            session()->flash('erro', 'O e-mail é obrigatório para computarmos o voto!');
            return redirect()->back();
        }

        $opcao = EnqueteOpcoes::where('id', '=', $voto)->first();

        if(is_null($opcao)) {
            session()->flash('erro', 'Erro ao inserir voto.');
            return redirect()->back();
        }

        if(sizeof(EnqueteVotos::where('email', '=', $email)->where('enquete_perguntas_id', '=', $opcao->enquete_pergunta_id)->get())) {
            session()->flash('erro', 'Você já votou nesta enquete!');
            session()->put('enqueteVotada', $opcao->enquete_pergunta_id);
            return redirect()->back();
        }

        $voto = new EnqueteVotos;
        $voto->enquete_opcoes_id = $opcao->id;
        $voto->enquete_perguntas_id = $opcao->enquete_pergunta_id;
        $voto->email = $email;
        $voto->save();
        session()->flash('sucesso', 'Seu voto foi computado com sucesso!');
        session()->put('enqueteVotada', $voto->enquete_perguntas_id);
        return redirect()->back();
    }
}
