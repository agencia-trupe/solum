<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Grade;
use App\Models\Ritmo;
use App\Models\RespostaAutomatica;
use App\Models\Nivel;

class GradeDeAulasController extends Controller
{
    public function index()
    {
        $grade  = Grade::orderBy('id', 'ASC')->get()->groupBy('dia_da_semana');
        $ritmos = Ritmo::ordenados()->lists('titulo', 'slug');

        $niveis = Nivel::ordenados()->get();

        return view('frontend.aulas.grade-de-aulas.index', compact('grade', 'ritmos', 'niveis'));
    }

    public function ritmo(Ritmo $ritmo)
    {
        $grade  = Grade::where('ritmo', $ritmo->titulo)->orderBy('id', 'ASC')->get()->groupBy('dia_da_semana');
        $ritmos = Ritmo::ordenados()->lists('titulo', 'slug');
        $ritmo  = $ritmo->slug;

        $niveis = Nivel::ordenados()->get();

        return view('frontend.aulas.grade-de-aulas.index', compact('grade', 'ritmos', 'ritmo', 'niveis'));
    }

    public function show(Grade $grade)
    {
        $nivel = Nivel::where('titulo', $grade->nivel)->first();
        if (!$nivel) abort('404');

        return view('frontend.aulas.grade-de-aulas.show')->with([
            'grade'    => $grade,
            'ritmos'   => Ritmo::ordenados()->lists('titulo', 'titulo'),
            'ritmo'    => $grade->ritmo,
            'assuntos' => RespostaAutomatica::ordenados()->lists('assunto_formulario', 'id'),
            'nivel'    => $nivel
        ]);
    }
}
