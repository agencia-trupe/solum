<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\CursoIntensivo;
use App\Models\CursosIntensivosChamada;
use App\Models\RespostaAutomatica;

class CursosIntensivosController extends Controller
{
    public function index()
    {
        return view('frontend.aulas.cursos-intensivos')->with([
            'chamada' => CursosIntensivosChamada::first(),
            'cursos'  => CursoIntensivo::ordenados()->get()
        ]);
    }

    public function curso($id)
    {
        $curso = CursoIntensivo::findOrFail($id);
        return view('frontend.aulas.cursos-intensivos-form')->with([
            'curso'    => $curso,
            'assuntos' => RespostaAutomatica::ordenados()->lists('assunto_formulario', 'id')
        ]);
    }
}
