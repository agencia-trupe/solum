<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servico;

class MaisServicosController extends Controller
{
    public function index(Servico $servico)
    {
        $servicosLista = Servico::ordenados()->lists('slug', 'titulo');
        $servico       = $servico->exists ? $servico : Servico::ordenados()->first();

        if (!$servico) abort('404');

        view()->share('servicoSelecionado', $servico->slug);

        return view('frontend.mais-servicos', compact('servicosLista', 'servico'));
    }
}
