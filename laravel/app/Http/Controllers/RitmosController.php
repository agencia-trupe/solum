<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Ritmo;
use App\Models\RespostaAutomatica;

class RitmosController extends Controller
{
    public function index(Ritmo $ritmo)
    {
        $ritmo = $ritmo->exists ? $ritmo : Ritmo::ordenados()->first();
        if (!$ritmo) abort('404');

        return view('frontend.aulas.ritmos')->with([
            'ritmos'     => Ritmo::ordenados()->lists('slug', 'titulo'),
            'assuntos'   => RespostaAutomatica::ordenados()->lists('assunto_formulario', 'id'),
            'ritmosForm' => Ritmo::ordenados()->lists('titulo', 'titulo'),
            'ritmo'      => $ritmo
        ]);
    }
}
