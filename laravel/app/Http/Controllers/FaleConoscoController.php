<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\RespostaAutomatica;
use App\Models\Ritmo;

class FaleConoscoController extends Controller
{
    public function index()
    {
        $contato  = Contato::first();
        $assuntos = RespostaAutomatica::ordenados()->lists('assunto_formulario', 'id');
        $ritmos   = Ritmo::ordenados()->lists('titulo', 'titulo');

        return view('frontend.fale-conosco', compact('contato', 'assuntos', 'ritmos'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $contato = Contato::first();
        $assunto = RespostaAutomatica::find($request->get('assunto'));

        $input = $request->all();
        $input['assunto'] = $assunto->assunto_formulario;

        $contatoRecebido->create($input);

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'Solum Escola de Dança')
                        ->subject('Dança')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        if ($assunto) {
            \Mail::send('emails.resposta-automatica', array_merge($request->all(), ['assunto' => $assunto]), function($message) use ($request, $contato, $assunto)
            {
                $message->to($request->get('email'), $request->get('nome'))
                        ->subject($assunto->assunto_do_e_mail)
                        ->replyTo($contato->email, 'Solum Escola de Dança');
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
