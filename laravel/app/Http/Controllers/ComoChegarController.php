<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ComoChegar;

class ComoChegarController extends Controller
{
    public function index()
    {
        return view('frontend.localizacao.como-chegar')->with([
            'comoChegar' => ComoChegar::first(),
        ]);
    }
}
