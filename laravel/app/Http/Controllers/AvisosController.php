<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Aviso;

class AvisosController extends Controller
{
    public function index()
    {
        return view('frontend.mural.avisos')->with([
            'avisos' => Aviso::ordenados()->get(),
        ]);
    }
}
