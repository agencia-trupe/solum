<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Historico;
use App\Models\HistoricoImagem;
use App\Models\HistoricoImagemLateral;

class HistoricoController extends Controller
{
    public function index()
    {
        return view('frontend.a-solum.historico')->with([
            'texto'           => Historico::first(),
            'imagens'         => HistoricoImagem::ordenados()->get(),
            'imagensLaterais' => HistoricoImagemLateral::ordenados()->get()
        ]);
    }
}
