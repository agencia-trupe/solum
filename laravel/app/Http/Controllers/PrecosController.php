<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Preco;
use App\Models\Promocao;

class PrecosController extends Controller
{
    public function index()
    {
        return view('frontend.aulas.precos')->with([
            'precos'    => Preco::ordenados()->get(),
            'promocoes' => Promocao::ordenados()->get(),
        ]);
    }
}
