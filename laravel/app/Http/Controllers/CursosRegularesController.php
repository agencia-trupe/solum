<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\CursosRegulares;
use App\Models\CursosRegularesImagemLateral;

class CursosRegularesController extends Controller
{
    public function index()
    {
        return view('frontend.aulas.cursos-regulares')->with([
            'textos'          => CursosRegulares::first(),
            'imagensLaterais' => CursosRegularesImagemLateral::ordenados()->get()
        ]);
    }
}
