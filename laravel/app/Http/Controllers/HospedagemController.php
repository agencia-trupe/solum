<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Hospedagem;

class HospedagemController extends Controller
{
    public function index()
    {
        return view('frontend.localizacao.hospedagem')->with([
            'hospedagens' => Hospedagem::ordenados()->get(),
        ]);
    }
}
