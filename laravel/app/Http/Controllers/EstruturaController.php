<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Estrutura;
use App\Models\EstruturaImagem;
use App\Models\EstruturaImagemLateral;

class EstruturaController extends Controller
{
    public function index()
    {
        return view('frontend.a-solum.estrutura')->with([
            'texto'           => Estrutura::first(),
            'imagens'         => EstruturaImagem::ordenados()->get(),
            'imagensLaterais' => EstruturaImagemLateral::ordenados()->get()
        ]);
    }
}
