<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GradeDeAulasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'arquivo' => 'required|mimetypes:application/vnd.ms-excel,text/plain,text/csv,text/tsv'
        ];
    }

    public function messages()
    {
        return [
            'arquivo.mimetypes' => 'O arquivo deve ser do tipo CSV.'
        ];
    }
}
