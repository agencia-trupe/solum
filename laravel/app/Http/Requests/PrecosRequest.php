<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PrecosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'subtitulo' => 'required',
            'descricao' => 'required',
            'observacoes' => 'required',
            'chamada' => 'required',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
