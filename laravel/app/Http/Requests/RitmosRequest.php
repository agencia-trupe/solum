<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RitmosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required|unique:ritmos',
            'slug' => '',
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['titulo'] = 'required|unique:ritmos,titulo,'.$this->route('ritmos')->id;
        }

        return $rules;
    }
}
