<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NiveisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required|unique:niveis',
            'estrutura_do_curso' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['titulo'] = 'required|unique:niveis,titulo,'.$this->route('niveis')->id;
        }

        return $rules;
    }
}
