<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfessoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'slug' => '',
            'imagem' => 'required|image',
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
