<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada_1' => 'required',
            'chamada_1_link' => 'required',
            'chamada_2' => 'required',
            'chamada_2_link' => 'required',
            'chamada_3' => 'required',
            'chamada_3_link' => 'required',
            'chamada_4' => 'required',
            'chamada_4_link' => 'required',
        ];
    }
}
