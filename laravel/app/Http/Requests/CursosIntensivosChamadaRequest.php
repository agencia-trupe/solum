<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CursosIntensivosChamadaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada' => 'required',
        ];
    }
}
