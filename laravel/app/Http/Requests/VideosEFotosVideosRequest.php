<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideosEFotosVideosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'video_tipo' => 'required',
            'video_codigo' => 'required',
            'capa' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
