<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EnquetesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data'            => 'required',
            'pergunta'        => 'required',
            'enquete_opcao.*' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'enquete_opcao.*.required' => 'O campo respostas é obrigatório.'
        ];
    }
}
