<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ComoChegarRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'endereco' => 'required',
            'observacoes' => '',
            'codigo_googlemaps' => 'required',
        ];
    }
}
