<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RespostasAutomaticasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'assunto_formulario' => 'required',
            'assunto_do_e_mail' => 'required',
            'resposta_automatica' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
