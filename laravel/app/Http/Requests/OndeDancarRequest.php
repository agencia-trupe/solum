<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OndeDancarRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'imagem' => 'required|image',
            'endereco' => 'required',
            'telefone' => '',
            'link' => '',
            'observacoes' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
