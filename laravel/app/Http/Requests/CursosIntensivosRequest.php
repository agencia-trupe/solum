<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CursosIntensivosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'imagem' => 'required|image',
            'descricao' => 'required',
            'investimento' => 'required',
            'chamada' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
