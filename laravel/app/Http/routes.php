<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('intensivos', function () {
        return redirect()->route('aulas.cursos-intensivos');
    });
    Route::get('grade', function () {
        return redirect()->route('aulas.grade-de-aulas.index');
    });

    Route::group(['prefix' => 'a-solum'], function () {
        Route::get('/', 'HistoricoController@index')
                ->name('a-solum.index');
        Route::get('historico', 'HistoricoController@index')
                ->name('a-solum.historico');
        Route::get('estrutura', 'EstruturaController@index')
                ->name('a-solum.estrutura');
        Route::get('professores', 'ProfessoresController@index')
                ->name('a-solum.professores.index');
        Route::get('professores/{professor_slug}', 'ProfessoresController@show')
                ->name('a-solum.professores.show');
        Route::get('vantagens', 'VantagensController@index')
                ->name('a-solum.vantagens');
        Route::get('videos-e-fotos', 'VideosEFotosController@index')
                ->name('a-solum.videos-e-fotos.index');
        Route::get('videos-e-fotos/ritmo/{ritmo_slug?}', 'VideosEFotosController@ritmo')
                ->name('a-solum.videos-e-fotos.ritmo');
        Route::get('videos-e-fotos/busca', 'VideosEFotosController@busca')
                ->name('a-solum.videos-e-fotos.busca');
        Route::get('videos-e-fotos/{galeria_slug}', 'VideosEFotosController@show')
                ->name('a-solum.videos-e-fotos.show');
    });

    Route::group(['prefix' => 'aulas'], function () {
        Route::get('/', 'RitmosController@index')
                ->name('aulas.index');
        Route::get('ritmos/{ritmo_slug?}', 'RitmosController@index')
                ->name('aulas.ritmos');
        Route::get('grade-de-aulas', 'GradeDeAulasController@index')
                ->name('aulas.grade-de-aulas.index');
        Route::get('grade-de-aulas/ritmo/{ritmo_slug?}', 'GradeDeAulasController@ritmo')
                ->name('aulas.grade-de-aulas.ritmo');
        Route::get('grade-de-aulas/{grade_de_aulas}', 'GradeDeAulasController@show')
                ->name('aulas.grade-de-aulas.show');
        Route::get('cursos-regulares', 'CursosRegularesController@index')
                ->name('aulas.cursos-regulares');
        Route::get('cursos-intensivos', 'CursosIntensivosController@index')
                ->name('aulas.cursos-intensivos');
        Route::get('cursos-intensivos/{intensivo_id}', 'CursosIntensivosController@curso')
                ->name('aulas.cursos-intensivos.curso');
        Route::get('precos', 'PrecosController@index')
                ->name('aulas.precos');
    });

    Route::group(['prefix' => 'calendario'], function () {
        Route::get('/', 'EventosController@index')
                ->name('calendario.index');
        Route::get('eventos', 'EventosController@index')
                ->name('calendario.eventos');
        Route::get('turmas-e-aulas', 'TurmasEAulasController@index')
                ->name('calendario.turmas-e-aulas');
    });

    Route::get('mais-servicos/{mais_servicos_slug?}', 'MaisServicosController@index')
            ->name('mais-servicos');

    Route::group(['prefix' => 'mural'], function () {
        Route::get('/', 'AvisosController@index')
                ->name('mural.index');
        Route::get('avisos', 'AvisosController@index')
                ->name('mural.avisos');
        Route::get('onde-dancar', 'OndeDancarController@index')
                ->name('mural.onde-dancar');
        Route::get('onde-comprar', 'OndeComprarController@index')
                ->name('mural.onde-comprar');
        Route::get('enquete', 'EnquetesController@index')
                ->name('mural.enquetes');
        Route::get('enquete/votar/{opcao?}', 'EnquetesController@formVotar')
                ->name('mural.enquetes.formVotar');
        Route::post('enquete/votar', 'EnquetesController@votar')
                ->name('mural.enquetes.votar');
        Route::get('enquete/{enquetes}', 'EnquetesController@resultados')
                ->name('mural.enquetes.resultados');
    });

    Route::group(['prefix' => 'localizacao'], function () {
        Route::get('/', 'ComoChegarController@index')
                ->name('localizacao.index');
        Route::get('como-chegar', 'ComoChegarController@index')
                ->name('localizacao.como-chegar');
        Route::get('hospedagem', 'HospedagemController@index')
                ->name('localizacao.hospedagem');
    });

    Route::get('fale-conosco', 'FaleConoscoController@index')
            ->name('fale-conosco.index');
    Route::post('fale-conosco', 'FaleConoscoController@post')
            ->name('fale-conosco.post');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        // HOME
        Route::resource('home/banners', 'BannersController');
        Route::resource('home/chamadas', 'ChamadasController', ['only' => ['index', 'update']]);
        Route::resource('home/videos', 'VideosHomeController');

        // A SOLUM
        Route::resource('a-solum/historico', 'HistoricoController', ['only' => ['index', 'update']]);
        Route::get('a-solum/historico/imagens/clear', [
            'as'   => 'painel.a-solum.historico.imagens.clear',
            'uses' => 'HistoricoImagensController@clear'
        ]);
        Route::resource('a-solum/historico/imagens-laterais', 'HistoricoImagensLateraisController');
        Route::resource('a-solum/historico/imagens', 'HistoricoImagensController');
        Route::resource('a-solum/estrutura', 'EstruturaController', ['only' => ['index', 'update']]);
        Route::get('a-solum/estrutura/imagens/clear', [
            'as'   => 'painel.a-solum.estrutura.imagens.clear',
            'uses' => 'EstruturaImagensController@clear'
        ]);
        Route::resource('a-solum/estrutura/imagens-laterais', 'EstruturaImagensLateraisController');
        Route::resource('a-solum/estrutura/imagens', 'EstruturaImagensController');
        Route::resource('a-solum/professores', 'ProfessoresController');
        Route::resource('a-solum/professores.videos', 'ProfessoresVideosController');
        Route::get('a-solum/professores/{professores}/imagens/clear', [
            'as'   => 'painel.a-solum.professores.imagens.clear',
            'uses' => 'ProfessoresImagensController@clear'
        ]);
        Route::resource('a-solum/professores.imagens', 'ProfessoresImagensController');
        Route::resource('a-solum/vantagens', 'VantagensController', ['only' => ['index', 'update']]);
        Route::get('a-solum/vantagens/imagens/clear', [
            'as'   => 'painel.a-solum.vantagens.imagens.clear',
            'uses' => 'VantagensImagensController@clear'
        ]);
        Route::resource('a-solum/vantagens/imagens-laterais', 'VantagensImagensLateraisController');
        Route::resource('a-solum/vantagens/imagens', 'VantagensImagensController');
        Route::resource('a-solum/videos-e-fotos', 'VideosEFotosController');
        Route::resource('a-solum/videos-e-fotos.videos', 'VideosEFotosVideosController');
        Route::get('a-solum/videos-e-fotos/{videos_e_fotos}/imagens/clear', [
            'as'   => 'painel.a-solum.videos-e-fotos.imagens.clear',
            'uses' => 'VideosEFotosImagensController@clear'
        ]);
        Route::resource('a-solum/videos-e-fotos.imagens', 'VideosEFotosImagensController');

        // AULAS
        Route::resource('aulas/ritmos', 'RitmosController');
        Route::resource('aulas/ritmos.videos', 'RitmosVideosController');
        Route::resource('aulas/cursos-regulares', 'CursosRegularesController', ['only' => ['index', 'update']]);
        Route::resource('aulas/cursos-regulares/imagens-laterais', 'CursosRegularesImagensLateraisController');
        Route::resource('aulas/cursos-intensivos/chamada', 'CursosIntensivosChamadaController', ['only' => ['index', 'update']]);
        Route::resource('aulas/cursos-intensivos', 'CursosIntensivosController');
        Route::resource('aulas/precos', 'PrecosController');
        Route::resource('aulas/promocoes', 'PromocoesController');

        // GRADE DE AULAS
        Route::resource('aulas/grade-de-aulas/niveis', 'NiveisController');
        Route::get('aulas/grade-de-aulas', 'GradeDeAulasController@index')->name('painel.aulas.grade-de-aulas.index');
        Route::post('aulas/grade-de-aulas', 'GradeDeAulasController@store')->name('painel.aulas.grade-de-aulas.store');
        Route::get('aulas/grade-de-aulas/get-ritmos', 'GradeDeAulasController@ritmos')->name('painel.aulas.grade-de-aulas.getRitmos');
        Route::get('aulas/grade-de-aulas/get-niveis', 'GradeDeAulasController@niveis')->name('painel.aulas.grade-de-aulas.getNiveis');
        Route::post('aulas/grade-de-aulas/update', 'GradeDeAulasController@update')->name('painel.aulas.grade-de-aulas.update');
        Route::delete('aulas/grade-de-aulas/{grade_de_aulas}', 'GradeDeAulasController@destroy')->name('painel.aulas.grade-de-aulas.destroy');

        // CALENDÁRIO
        Route::resource('calendario/turmas-e-aulas', 'TurmasEAulasController');
        Route::resource('calendario/eventos', 'EventosController');

        // MAIS SERVIÇOS
        Route::resource('mais-servicos', 'MaisServicosController');
        Route::get('mais-servicos/{mais_servicos}/imagens/clear', [
            'as'   => 'painel.mais-servicos.imagens.clear',
            'uses' => 'MaisServicosImagensController@clear'
        ]);
        Route::resource('mais-servicos.imagens-laterais', 'MaisServicosImagensLateraisController');
        Route::resource('mais-servicos.imagens', 'MaisServicosImagensController');

        // MURAL
        Route::resource('mural/avisos', 'AvisosController');
        Route::resource('mural/onde-comprar', 'OndeComprarController');
        Route::resource('mural/onde-dancar', 'OndeDancarController');
        Route::resource('mural/enquetes', 'EnquetesController');
        Route::get('mural/enquetes/ativar/{enquetes}', 'EnquetesController@ativar')->name('painel.mural.enquetes.ativar');

        // LOCALIZAÇÃO
        Route::resource('localizacao/hospedagem', 'HospedagemController');
        Route::resource('localizacao/como-chegar', 'ComoChegarController', ['only' => ['index', 'update']]);

        // CONTATO
        Route::resource('contato/respostas-automaticas', 'RespostasAutomaticasController');
        Route::get('contatos/recebidos/exportar', 'ContatosRecebidosController@exportar')->name('painel.contato.recebidos.exportar');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');

        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
