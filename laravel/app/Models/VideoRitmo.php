<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class VideoRitmo extends Model
{
    protected $table = 'ritmos_videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeRitmo($query, $id)
    {
        return $query->where('ritmo_id', $id);
    }
}
