<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Carbon\Carbon;

class Aviso extends Model
{
    protected $table = 'avisos';

    protected $guarded = ['id'];

    protected $appends = ['data_formatada', 'categoria_nome'];

    public static function categorias() {
        return [
            'atencao'     => 'Atenção',
            'lembrete'    => 'Lembrete',
            'novidade'    => 'Novidade',
            'aviso'       => 'Aviso',
            'urgente'     => 'Urgente',
            'diversao'    => 'Diversão',
            'comemoracao' => 'Comemoração',
            'dicas'       => 'Dicas',
        ];
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i', $date)->format('d/m/Y H:i');
    }

    public function getDataFormatadaAttribute()
    {
        $dt = Carbon::createFromFormat('d/m/Y H:i', $this->data);

        return $dt->format('d/m/Y') . '<br> às ' . $dt->format('H:i') . ' h';
    }

    public function getCategoriaNomeAttribute()
    {
        return Aviso::categorias()[$this->categoria];
    }
}
