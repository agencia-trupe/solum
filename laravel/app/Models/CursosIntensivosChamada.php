<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CursosIntensivosChamada extends Model
{
    protected $table = 'cursos_intensivos_chamada';

    protected $guarded = ['id'];

}
