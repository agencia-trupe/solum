<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class VideoGaleria extends Model
{
    protected $table = 'videos_e_fotos_videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeGaleria($query, $id)
    {
        return $query->where('videos_e_fotos_id', $id);
    }
}
