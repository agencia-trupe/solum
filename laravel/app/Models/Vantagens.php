<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Vantagens extends Model
{
    protected $table = 'vantagens';

    protected $guarded = ['id'];

}
