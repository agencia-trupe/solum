<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ServicoImagemLateral extends Model
{
    protected $table = 'mais_servicos_imagens_laterais';

    protected $guarded = ['id'];

    public function scopeServico($query, $id)
    {
        return $query->where('servico_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/mais-servicos/imagens-laterais/'
        ]);
    }
}
