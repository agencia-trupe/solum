<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaleriaImagem extends Model
{
    protected $table = 'videos_e_fotos_imagens';

    protected $guarded = ['id'];

    public function scopeGaleria($query, $id)
    {
        return $query->where('videos_e_fotos_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
