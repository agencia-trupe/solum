<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Professor extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'professores';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 240,
            'height' => 240,
            'path'   => 'assets/img/professores/'
        ]);
    }

    public function ritmos()
    {
        return $this->belongsToMany('App\Models\Ritmo', 'professores_ritmos', 'professor_id', 'ritmo_id')->ordenados();
    }

    public function videos()
    {
        return $this->hasMany('App\Models\VideoProfessor', 'professor_id')->ordenados();
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProfessorImagem', 'professor_id')->ordenados();
    }
}
