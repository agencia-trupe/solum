<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class EnquetePergunta extends Model
{
	protected $table = 'enquete_perguntas';

	protected $guarded = ['id'];

    public function scopeAtiva($query)
    {
        return $query->where('ativa', '=', '1')->first();
    }

    public function scopeSemvotos($query)
    {
    	$ids = \DB::table('enquete_votos')->groupBy('enquete_perguntas_id')->lists('enquete_perguntas_id');

    	if(!$ids)
    		$ids = array('');

    	return $query->whereNotIn('id', $ids)->orWhere('ativa', '=', '1');
    }

    public function scopeHistorico($query)
    {
    	$ids = \DB::table('enquete_votos')->groupBy('enquete_perguntas_id')->lists('enquete_perguntas_id');

    	if(!$ids)
    		$ids = array('');

    	return $query->where('ativa', '!=', '1')->whereIn('id', $ids);
    }

    public function opcoes()
    {
    	return $this->hasMany('\App\Models\EnqueteOpcoes', 'enquete_pergunta_id')->orderBy('ordem', 'asc');
    }

    public function numeroVotos()
    {
    	return $this->hasMany('\App\Models\EnqueteVotos', 'enquete_perguntas_id');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}
