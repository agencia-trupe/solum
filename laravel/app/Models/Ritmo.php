<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Ritmo extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'ritmos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function videos()
    {
        return $this->hasMany('App\Models\VideoRitmo', 'ritmo_id')->ordenados();
    }

    public function professores()
    {
        return $this->belongsToMany('App\Models\Professor', 'professores_ritmos', 'ritmo_id', 'professor_id')->ordenados();
    }
}
