<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicoImagem extends Model
{
    protected $table = 'mais_servicos_imagens';

    protected $guarded = ['id'];

    public function scopeServico($query, $id)
    {
        return $query->where('servico_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
