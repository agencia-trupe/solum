<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class VideoProfessor extends Model
{
    protected $table = 'professores_videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProfessor($query, $id)
    {
        return $query->where('professor_id', $id);
    }
}
