<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CursosRegulares extends Model
{
    protected $table = 'cursos_regulares';

    protected $guarded = ['id'];

}
