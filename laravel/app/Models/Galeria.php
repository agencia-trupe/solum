<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;
use Carbon\Carbon;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Galeria extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'videos_e_fotos';

    protected $guarded = ['id'];

    public function ritmos()
    {
        return $this->belongsToMany('App\Models\Ritmo', 'videos_e_fotos_ritmos', 'videos_e_fotos_id', 'ritmo_id');
    }

    public function videos()
    {
        return $this->hasMany('App\Models\VideoGaleria', 'videos_e_fotos_id')->ordenados();
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\GaleriaImagem', 'videos_e_fotos_id')->ordenados();
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('m/Y', $date)->format('Y-m');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m', $date)->format('m/Y');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 290,
            'height' => 165,
            'path'   => 'assets/img/videos-e-fotos/'
        ]);
    }

}
