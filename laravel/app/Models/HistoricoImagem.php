<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricoImagem extends Model
{
    protected $table = 'historico_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
