<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnqueteVotos extends Model
{
	protected $table = 'enquete_votos';

    protected $guarded = ['id'];
}
