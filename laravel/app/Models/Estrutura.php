<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Estrutura extends Model
{
    protected $table = 'estrutura';

    protected $guarded = ['id'];

}
