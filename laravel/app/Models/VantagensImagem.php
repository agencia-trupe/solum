<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VantagensImagem extends Model
{
    protected $table = 'vantagens_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
