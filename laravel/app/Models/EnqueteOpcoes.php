<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnqueteOpcoes extends Model
{
	protected $table = 'enquete_opcoes';

    protected $guarded = ['id'];

    public function votos()
    {
    	return $this->hasMany('\App\Models\EnqueteVotos', 'enquete_opcoes_id');
    }

    public function pergunta()
    {
    	return $this->belongsTo('\App\Models\EnquetePergunta', 'enquete_pergunta_id');
    }
}
