<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class VantagensImagemLateral extends Model
{
    protected $table = 'vantagens_imagens_laterais';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/vantagens/imagens-laterais/'
        ]);
    }
}
