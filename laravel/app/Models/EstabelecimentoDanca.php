<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class EstabelecimentoDanca extends Model
{
    protected $table = 'onde_dancar';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 200,
            'height' => 200,
            'path'   => 'assets/img/onde-dancar/'
        ]);
    }

}
