<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Historico extends Model
{
    protected $table = 'historico';

    protected $guarded = ['id'];

}
