<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessorImagem extends Model
{
    protected $table = 'professores_imagens';

    protected $guarded = ['id'];

    public function scopeProfessor($query, $id)
    {
        return $query->where('professor_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
