<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Chamadas extends Model
{
    protected $table = 'chamadas';

    protected $guarded = ['id'];

}
