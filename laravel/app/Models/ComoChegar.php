<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ComoChegar extends Model
{
    protected $table = 'como_chegar';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/como-chegar/'
        ]);
    }

}
