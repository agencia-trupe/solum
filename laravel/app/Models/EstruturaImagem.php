<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstruturaImagem extends Model
{
    protected $table = 'estrutura_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
