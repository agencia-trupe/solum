<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Servico extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'mais_servicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagensLaterais()
    {
        return $this->hasMany('App\Models\ServicoImagemLateral', 'servico_id')->ordenados();
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ServicoImagem', 'servico_id')->ordenados();
    }
}
