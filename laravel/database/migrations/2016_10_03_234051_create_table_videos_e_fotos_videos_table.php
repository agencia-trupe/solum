<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideosEFotosVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos_e_fotos_videos', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('videos_e_fotos_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('video_tipo');
            $table->string('video_codigo');
            $table->string('capa');
            $table->timestamps();
            $table->foreign('videos_e_fotos_id')->references('id')->on('videos_e_fotos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos_e_fotos_videos');
    }
}
