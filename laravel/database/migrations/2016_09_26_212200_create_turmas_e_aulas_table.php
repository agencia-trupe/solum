<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurmasEAulasTable extends Migration
{
    public function up()
    {
        Schema::create('turmas_e_aulas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->text('aviso');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('turmas_e_aulas');
    }
}
