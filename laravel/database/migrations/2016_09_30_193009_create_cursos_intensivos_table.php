<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosIntensivosTable extends Migration
{
    public function up()
    {
        Schema::create('cursos_intensivos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('imagem');
            $table->text('descricao');
            $table->text('investimento');
            $table->string('chamada');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cursos_intensivos');
    }
}
