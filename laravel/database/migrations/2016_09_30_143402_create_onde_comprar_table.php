<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOndeComprarTable extends Migration
{
    public function up()
    {
        Schema::create('onde_comprar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('imagem');
            $table->text('endereco');
            $table->string('telefone');
            $table->string('link');
            $table->text('observacoes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('onde_comprar');
    }
}
