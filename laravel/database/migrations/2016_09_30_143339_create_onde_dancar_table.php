<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOndeDancarTable extends Migration
{
    public function up()
    {
        Schema::create('onde_dancar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('imagem');
            $table->text('endereco');
            $table->string('telefone');
            $table->string('link');
            $table->text('observacoes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('onde_dancar');
    }
}
