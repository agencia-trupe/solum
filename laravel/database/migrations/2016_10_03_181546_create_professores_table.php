<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessoresTable extends Migration
{
    public function up()
    {
        Schema::create('professores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('slug');
            $table->string('imagem');
            $table->text('descricao');
            $table->timestamps();
        });

        Schema::create('professores_ritmos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('professor_id')->unsigned();
            $table->integer('ritmo_id')->unsigned();
            $table->timestamps();
            $table->foreign('professor_id')->references('id')->on('professores');
            $table->foreign('ritmo_id')->references('id')->on('ritmos');
        });
    }

    public function down()
    {
        Schema::drop('professores_ritmos');
        Schema::drop('professores');
    }
}
