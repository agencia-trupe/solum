<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrecosTable extends Migration
{
    public function up()
    {
        Schema::create('precos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('subtitulo');
            $table->text('descricao');
            $table->text('observacoes');
            $table->string('chamada');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('precos');
    }
}
