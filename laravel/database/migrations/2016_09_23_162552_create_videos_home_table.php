<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosHomeTable extends Migration
{
    public function up()
    {
        Schema::create('videos_home', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('video_tipo');
            $table->string('video_codigo');
            $table->string('capa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('videos_home');
    }
}
