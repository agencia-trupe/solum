<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComoChegarTable extends Migration
{
    public function up()
    {
        Schema::create('como_chegar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->text('endereco');
            $table->text('observacoes');
            $table->text('codigo_googlemaps');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('como_chegar');
    }
}
