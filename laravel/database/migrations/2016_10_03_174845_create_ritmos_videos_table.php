<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRitmosVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ritmos_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ritmo_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('video_tipo');
            $table->string('video_codigo');
            $table->string('capa');
            $table->timestamps();
            $table->foreign('ritmo_id')->references('id')->on('ritmos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ritmos_videos');
    }
}
