<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRitmosTable extends Migration
{
    public function up()
    {
        Schema::create('ritmos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->text('descricao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ritmos');
    }
}
