<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosEFotosTable extends Migration
{
    public function up()
    {
        Schema::create('videos_e_fotos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('data');
            $table->string('titulo');
            $table->string('slug');
            $table->string('capa');
            $table->timestamps();
        });

        Schema::create('videos_e_fotos_ritmos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('videos_e_fotos_id')->unsigned();
            $table->integer('ritmo_id')->unsigned();
            $table->timestamps();
            $table->foreign('videos_e_fotos_id')->references('id')->on('videos_e_fotos');
            $table->foreign('ritmo_id')->references('id')->on('ritmos');
        });
    }

    public function down()
    {
        Schema::drop('videos_e_fotos_ritmos');
        Schema::drop('videos_e_fotos');
    }
}
