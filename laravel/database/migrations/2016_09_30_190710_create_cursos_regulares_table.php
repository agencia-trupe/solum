<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosRegularesTable extends Migration
{
    public function up()
    {
        Schema::create('cursos_regulares', function (Blueprint $table) {
            $table->increments('id');
            $table->text('titulo');
            $table->text('texto');
            $table->timestamps();
        });

        Schema::create('cursos_regulares_imagens_laterais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cursos_regulares');
        Schema::drop('cursos_regulares_imagens_laterais');
    }
}
