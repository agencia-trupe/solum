<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeDeAulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grade_de_aulas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dia_da_semana')->default('');
            $table->string('ritmo')->default('');
            $table->string('nivel')->default('');
            $table->string('inicio')->default('');
            $table->string('horario')->default('');
            $table->string('professores')->default('');
            $table->string('observacoes')->default('Disponível para Homens e Mulheres');
            $table->string('observacoes_extra')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grade_de_aulas');
    }
}
