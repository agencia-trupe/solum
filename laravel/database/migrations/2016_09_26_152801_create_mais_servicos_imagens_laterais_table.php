<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaisServicosImagensLateraisTable extends Migration
{
    public function up()
    {
        Schema::create('mais_servicos_imagens_laterais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('servico_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('servico_id')->references('id')->on('mais_servicos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('mais_servicos_imagens_laterais');
    }
}
