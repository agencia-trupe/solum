<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespostasAutomaticasTable extends Migration
{
    public function up()
    {
        Schema::create('respostas_automaticas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('assunto_formulario');
            $table->string('assunto_do_e_mail');
            $table->text('resposta_automatica');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('respostas_automaticas');
    }
}
