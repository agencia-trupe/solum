<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNiveisTable extends Migration
{
    public function up()
    {
        Schema::create('niveis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('estrutura_do_curso');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('niveis');
    }
}
