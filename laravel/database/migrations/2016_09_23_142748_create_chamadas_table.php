<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadasTable extends Migration
{
    public function up()
    {
        Schema::create('chamadas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('chamada_1');
            $table->string('chamada_1_link');
            $table->text('chamada_2');
            $table->string('chamada_2_link');
            $table->text('chamada_3');
            $table->string('chamada_3_link');
            $table->text('chamada_4');
            $table->string('chamada_4_link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chamadas');
    }
}
