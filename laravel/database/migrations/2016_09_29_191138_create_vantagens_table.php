<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVantagensTable extends Migration
{
    public function up()
    {
        Schema::create('vantagens', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });

        Schema::create('vantagens_imagens_laterais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('vantagens_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('vantagens');
        Schema::drop('vantagens_imagens_laterais');
        Schema::drop('vantagens_imagens');
    }
}
