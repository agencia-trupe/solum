<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosIntensivosChamadaTable extends Migration
{
    public function up()
    {
        Schema::create('cursos_intensivos_chamada', function (Blueprint $table) {
            $table->increments('id');
            $table->text('chamada');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cursos_intensivos_chamada');
    }
}
