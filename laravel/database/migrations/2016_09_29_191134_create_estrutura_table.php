<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstruturaTable extends Migration
{
    public function up()
    {
        Schema::create('estrutura', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });

        Schema::create('estrutura_imagens_laterais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('estrutura_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('estrutura');
        Schema::drop('estrutura_imagens_laterais');
        Schema::drop('estrutura_imagens');
    }
}
