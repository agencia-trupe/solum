<?php

use Illuminate\Database\Seeder;

class ChamadasSeeder extends Seeder
{
    public function run()
    {
        DB::table('chamadas')->insert([
            'chamada_1' => '',
            'chamada_1_link' => '',
            'chamada_2' => '',
            'chamada_2_link' => '',
            'chamada_3' => '',
            'chamada_3_link' => '',
            'chamada_4' => '',
            'chamada_4_link' => '',
        ]);
    }
}
