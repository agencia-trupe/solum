<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(CursosIntensivosChamadaSeeder::class);
		$this->call(CursosRegularesSeeder::class);
        $this->call(HistoricoSeeder::class);
        $this->call(EstruturaSeeder::class);
		$this->call(VantagensSeeder::class);
		$this->call(ComoChegarSeeder::class);
		$this->call(ChamadasSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
