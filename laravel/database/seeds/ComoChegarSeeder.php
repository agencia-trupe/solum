<?php

use Illuminate\Database\Seeder;

class ComoChegarSeeder extends Seeder
{
    public function run()
    {
        DB::table('como_chegar')->insert([
            'imagem' => '',
            'endereco' => '',
            'observacoes' => '',
            'codigo_googlemaps' => '',
        ]);
    }
}
