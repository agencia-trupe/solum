(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });

        $('#nav-mobile a[data-sub=true]').click(function(event) {
            event.preventDefault();

            if ($(this).hasClass('open')) {
                return $(this).removeClass('open').next().slideUp();
            }

            $('#nav-mobile .open').removeClass('open').next().slideUp();
            $(this).addClass('open').next().slideDown();
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            speed: 1500,
            pagerTemplate: '<a href=#>{{slideNum}}</a>'
        });
    };

    App.imagensLightbox = function() {
        $('.lightbox-imagem').fancybox({
            helpers: { overlay: { locked: true } },
            padding: 0
        });
    };

    App.videosLightbox = function() {
        $('.lightbox-video').fancybox({
            helpers: { overlay: { locked: true } },
            padding: 0,
            type: 'iframe',
            width: 800,
            height: 450,
            aspectRatio: true
        });
    };

    App.filtroRitmo = function() {
        $('.filtro-ritmo').on('change', function () {
            var slug  = $(this).val(),
                base  = $('base').attr('href'),
                route = $(this).data('route');

            if (slug) {
                window.location = base + '/' + route + '/ritmo/' + slug;
            } else {
                window.location = base + '/' + route;
            }
        });
    };

    App.gradeLightbox = function() {
        var base = $('base').attr('href');

        $('.tabela-aula-row').click(function() {
            var url = base + '/aulas/grade-de-aulas/' + $(this).attr('id');

            $.fancybox.open({
                href: url,
                helpers: { overlay: { locked: true } },
                type: 'ajax',
                maxWidth: 980,
                maxHeight: '95%',
                padding: 0
            });
        });
    };

    App.intensivoLightbox = function() {
        var base = $('base').attr('href');

        $('.cursos-intensivos .chamada').click(function(event) {
            event.preventDefault();

            var url = base + '/aulas/cursos-intensivos/' + $(this).attr('id');

            $.fancybox.open({
                href: url,
                helpers: { overlay: { locked: true } },
                type: 'ajax',
                maxWidth: 980,
                maxHeight: '95%',
                padding: 0
            });
        });
    };

    App.imagensMasonry = function() {
        var $grid = $('.imagens-masonry');

        $grid.waitForImages(function() {
            $grid.masonry({
                itemSelector: '.thumb-masonry',
                columnWidth: '.grid-sizer',
                gutter: '.gutter-sizer'
            });
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/fale-conosco',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone_fixo: $('#telefone_fixo').val(),
                telefone_celular: $('#telefone_celular').val(),
                assunto: $('#assunto').val(),
                ritmo: $('#ritmo').val() || '',
                mensagem: $('#mensagem').val(),
                curso: $('#curso').val() || '',
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
                }
            },
            dataType: 'json'
        });
    };

    App.enqueteVoto = function() {
        $('#form-enquete').submit(function(event) {
            event.preventDefault();

            var parameter = $(this).find('input[type=radio]:checked').length
                ? '/' + $(this).find('input[type=radio]:checked').val()
                : '';

            $.fancybox.open({
                href: $(this).data('url') + parameter,
                helpers: { overlay: { locked: true } },
                type: 'ajax',
                maxWidth: 500,
                maxHeight: '95%',
                padding: 0
            });
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.imagensLightbox();
        this.videosLightbox();
        this.filtroRitmo();
        this.gradeLightbox();
        this.intensivoLightbox();
        this.imagensMasonry();
        this.enqueteVoto();
        $('body').on('submit', '#form-contato', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
