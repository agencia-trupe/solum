@extends('frontend.common.template')

@section('content')

    <div class="main eventos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.calendario._nav')
            </div>

            <div class="institucional-content">
                <h2>CALENDÁRIO &middot; EVENTOS</h2>
                <div class="texto">
                    @foreach($eventos as $evento)
                    <?php $data = Tools::parseDataCalendario($evento->data); ?>
                    <div class="evento">
                        <div class="data">
                            <div>
                                <span class="dia">{{ $data['dia'] }}</span>
                                <span class="mes">{{ $data['mes'] }}</span>
                                <span class="ano">{{ $data['ano'] }}</span>
                                <span class="semana">{{ $data['semana'] }}</span>
                            </div>
                        </div>

                        <div class="descricao">
                            <p class="titulo">{{ $evento->titulo }}</p>
                            {!! $evento->descricao !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
