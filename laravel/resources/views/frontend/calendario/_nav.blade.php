<a href="{{ route('calendario.eventos') }}" @if(str_is('calendario.eventos', Route::currentRouteName()) || str_is('calendario.index', Route::currentRouteName())) class="active" @endif>Eventos</a>
<a href="{{ route('calendario.turmas-e-aulas') }}" @if(str_is('calendario.turmas-e-aulas', Route::currentRouteName())) class="active" @endif>Turmas e Aulas</a>
