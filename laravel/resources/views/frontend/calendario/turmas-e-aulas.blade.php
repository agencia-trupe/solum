@extends('frontend.common.template')

@section('content')

    <div class="main turmas-e-aulas institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.calendario._nav')
            </div>

            <div class="institucional-content">
                <h2>CALENDÁRIO &middot; TURMAS E AULAS</h2>
                <div class="texto">
                    @foreach($avisos as $aviso)
                    <?php $data = Tools::parseDataCalendario($aviso->data); ?>
                    <div class="aviso">
                        <div class="data">
                            <div class="dia">{{ $data['dia'] }}</div>
                            <div class="mes-ano">
                                <span class="mes">{{ mb_substr($data['mes'], 0, 3) }}</span>
                                <span class="ano">{{ $data['ano'] }}</span>
                            </div>
                            <div class="semana">{{ $data['semana'] }}</div>
                        </div>

                        <div class="descricao">
                            {!! $aviso->aviso !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
