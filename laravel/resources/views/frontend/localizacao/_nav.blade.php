<a href="{{ route('localizacao.como-chegar') }}" @if(str_is('localizacao.como-chegar', Route::currentRouteName()) || str_is('localizacao.index', Route::currentRouteName())) class="active" @endif>Como Chegar</a>
<a href="{{ route('localizacao.hospedagem') }}" @if(str_is('localizacao.hospedagem', Route::currentRouteName())) class="active" @endif>Hospedagem</a>
