@extends('frontend.common.template')

@section('content')

    <div class="main como-chegar institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.localizacao._nav')
            </div>

            <div class="institucional-content">
                <h2>LOCALIZAÇÃO &middot; COMO CHEGAR</h2>
                <div class="texto">
                    <h3>{!! $comoChegar->endereco !!}</h3>
                    {!! $comoChegar->observacoes !!}
                </div>
                <div class="imagens-laterais">
                    <img src="{{ asset('assets/img/como-chegar/'.$comoChegar->imagem) }}" alt="">
                </div>
            </div>
        </div>

        <div class="mapa">
            {!! $comoChegar->codigo_googlemaps !!}
        </div>
    </div>

@endsection
