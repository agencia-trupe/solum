@extends('frontend.common.template')

@section('content')

    <div class="main hospedagem institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.localizacao._nav')
            </div>

            <div class="institucional-content">
                <h2>LOCALIZAÇÃO &middot; HOSPEDAGEM</h2>
                <div class="texto">
                    <h3>Confira dicas de lugares para se hospedar próximos à Solum. Venha fazer seu curso nas férias em São Paulo!</h3>

                    @foreach($hospedagens as $hospedagem)
                    <div class="hospedagem">
                        <img src="{{ asset('assets/img/hospedagem/'.$hospedagem->imagem) }}" alt="">
                        <div class="descricao">
                            <h4>{{ $hospedagem->nome }}</h4>
                            <p>
                                {!! $hospedagem->endereco !!}<br>
                                {{ $hospedagem->telefone }}
                                <a href="{{ !preg_match('/:\/\//', $hospedagem->link) ? '//' : '' }}{{ $hospedagem->link }}">{{ $hospedagem->link }}</a>
                            </p>
                            <p>{!! $hospedagem->observacoes !!}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
