@extends('frontend.common.template')

@section('content')

    <div class="main grade-de-aulas institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.aulas._nav')
            </div>

            <div class="institucional-content">
                <h2>AULAS &middot; GRADE DE AULAS</h2>
                <div class="texto">
                    <p>
                        Para mais informações ou inscrições clique no curso desejado.<br>
                        Se não houver vagas p/ mulheres, cadastre-se na Lista de Espera (CLIQUE NO CURSO DESEJADO)
                    </p>
                    <div class="filtro">
                        <label>
                            Exibir só as aulas do ritmo:
                        </label>
                        {!! Form::select('ritmo', $ritmos, (isset($ritmo) ? $ritmo : null), ['class' => 'filtro-ritmo', 'placeholder' => 'FILTRAR POR RITMO', 'data-route' => 'aulas/grade-de-aulas']) !!}
                    </div>
                </div>
            </div>

            <div class="grade-tabela">
                @foreach($grade as $diaDaSemana => $rows)
                <div class="dia-semana">{{ $diaDaSemana }}</div>
                <div class="mobile-wrapper">
                <table>
                    <thead>
                        <tr>
                            <th>Ritmo</th>
                            <th>Nível</th>
                            <th>Início</th>
                            <th>Horário</th>
                            <th>Professores</th>
                            <th>Observações</th>
                            <th><span class="icone-info"></span></th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach ($rows as $row)
                        <tr class="tabela-aula-row" id="{{ $row->id }}">
                            <td>{{ $row->ritmo }}</td>
                            <td>{{ $row->nivel }}</td>
                            <td>{{ $row->inicio }}</td>
                            <td>{{ $row->horario }}</td>
                            <td>{{ $row->professores }}</td>
                            <td>
                                @if($row->observacoes_extra)
                                {{ $row->observacoes_extra }}
                                @else
                                @if($row->observacoes == 'Disponível para Homens')
                                Vagas disponíveis <strong>somente</strong> para: <strong>homens</strong> <span class="icone-homens"></span> &raquo; inscreva-se na lista de espera
                                @elseif($row->observacoes == 'Disponível para Mulheres')
                                Vagas disponíveis <strong>somente</strong> para: <strong>mulheres</strong> <span class="icone-mulheres"></span> &raquo; inscreva-se na lista de espera
                                @else
                                Vagas disponíveis para: homens e mulheres <span class="icone-homens"></span><span class="icone-mulheres"></span>
                                @endif
                                @endif
                            </td>
                            <td>
                                <span class="icone-seta"></span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                @endforeach

                <div class="legenda-niveis">
                    @foreach($niveis as $nivel)
                    <p><strong>{{ $nivel->titulo }}:</strong> {{ $nivel->estrutura_do_curso }}</p>
                    @endforeach
                </div>

                <a href="{{ route('aulas.cursos-intensivos') }}" class="link-intensivos">
                    Confira também nossa programação de Cursos Intensivos
                </a>
            </div>
        </div>
    </div>

@endsection
