<div class="lightbox-wrapper">
    <p class="titulo">
        <span>CURSO REGULAR</span>
        <strong>{{ $grade->ritmo }}</strong>
        <span>{{ $grade->dia_da_semana }} - {{ $grade->horario }} - Nível: <em>{{ $grade->nivel }}</em></span>
    </p>

    <p class="estrutura">
        <strong>ESTRUTURA DO CURSO:</strong> {{ $nivel->estrutura_do_curso }}
    </p>

    <div class="contato-formulario">
        <h3>ESTOU INTERESSADO &middot; QUERO SABER MAIS:</h3>
        <form action="" id="form-contato">
            <div class="left">
                <input type="text" name="nome" id="nome" placeholder="nome completo" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone_fixo" id="telefone_fixo" placeholder="telefone fixo">
                <input type="text" name="telefone_celular" id="telefone_celular" placeholder="telefone celular">
            </div>
            <div class="right">
                {!! Form::select('assunto', $assuntos, null, ['class' => 'form-control', 'id' => 'assunto', 'placeholder' => 'selecione o assunto', 'required' => true]) !!}
                {!! Form::select('ritmo', $ritmos, $ritmo, ['class' => 'form-control', 'id' => 'ritmo', 'placeholder' => 'ritmo [selecione um ritmo se for o caso]']) !!}
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="hidden" name="curso" id="curso" value="{{ $grade->ritmo }} - {{ $grade->dia_da_semana }} - {{ $grade->horario }} - Nível: {{ $grade->nivel }}">
                <div id="form-contato-response"></div>
                <input type="submit" value="enviar">
            </div>
        </form>
    </div>
</div>
