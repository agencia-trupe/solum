@extends('frontend.common.template')

@section('content')

    <div class="main ritmos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.aulas._nav')
            </div>

            <div class="institucional-content">
                <h2>AULAS &middot; RITMOS</h2>

                <div class="ritmos-list">
                    @foreach($ritmos as $titulo => $slug)
                    <a href="{{ route('aulas.ritmos', $slug) }}" @if($ritmo->slug === $slug) class="active" @endif>{{ $titulo }}</a>
                    @endforeach
                </div>

                <div class="texto">
                    <h2>{{ $ritmo->titulo }}</h2>
                    {!! $ritmo->descricao !!}

                    @if(count($ritmo->professores))
                    <div class="ritmo-professores">
                        <p>Conheça nossos professores de {{ $ritmo->titulo }}</p>
                        <div>
                            @foreach($ritmo->professores as $professor)
                            <a href="{{ route('a-solum.professores.show', $professor->slug) }}">
                                <img src="{{ asset('assets/img/professores/'.$professor->imagem) }}" alt="">
                                <span class="mais">saber mais &raquo;</span>
                                <span class="nome">{{ $professor->nome }}</span>
                            </a>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>

                @if(count($ritmo->videos))
                <div class="ritmo-videos">
                    <p>Veja alguns vídeos de {{ $ritmo->titulo }} da Solum</p>
                    <div class="video-principal">
                        <div class="video">
                            <div class="embed-video">
                                <iframe src="{{ $ritmo->videos->first()->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$ritmo->videos->first()->video_codigo : 'https://player.vimeo.com/video/'.$ritmo->videos->first()->video_codigo }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                            </div>
                        </div>
                    </div>

                    @unless(count($ritmo->videos) < 2)
                    <div class="videos-thumbs">
                        @foreach($ritmo->videos as $video)
                        @unless($video === $ritmo->videos->first())
                        <a href="{{ $video->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$video->video_codigo.'?autoplay=1' : 'https://player.vimeo.com/video/'.$video->video_codigo }}" class="lightbox-video">
                            <img src="{{ asset('assets/img/videos/'.$video->capa) }}" alt="">
                        </a>
                        @endunless
                        @endforeach
                    </div>
                    @endunless
                </div>
                @endif
            </div>
        </div>

        <div class="ritmos-contato">
            <div class="center">
                <div class="contato-formulario">
                    <h3>ESTOU INTERESSADO &middot; QUERO SABER MAIS:</h3>
                    <form action="" id="form-contato">
                        <div class="left">
                            <input type="text" name="nome" id="nome" placeholder="nome completo" required>
                            <input type="email" name="email" id="email" placeholder="e-mail" required>
                            <input type="text" name="telefone_fixo" id="telefone_fixo" placeholder="telefone fixo">
                            <input type="text" name="telefone_celular" id="telefone_celular" placeholder="telefone celular">
                        </div>
                        <div class="right">
                            {!! Form::select('assunto', $assuntos, null, ['class' => 'form-control', 'id' => 'assunto', 'placeholder' => 'selecione o assunto', 'required' => true]) !!}
                            {!! Form::select('ritmo', $ritmosForm, $ritmo->titulo, ['class' => 'form-control', 'id' => 'ritmo', 'placeholder' => 'ritmo [selecione um ritmo se for o caso]']) !!}
                            <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                            <div id="form-contato-response"></div>
                            <input type="submit" value="enviar">
                        </div>
                    </form>
                </div>

                <a href="{{ route('aulas.grade-de-aulas.index') }}" class="chamada-grade">
                    <div>
                        Confira nossa
                        <span>Grade de Aulas</span>
                        e venha aprender a dançar com a gente!
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection
