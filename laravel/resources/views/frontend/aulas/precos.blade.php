@extends('frontend.common.template')

@section('content')

    <div class="main precos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.aulas._nav')
            </div>

            <div class="institucional-content">
                <h2>AULAS &middot; PREÇOS</h2>
                <div class="texto">
                    <h3>Confira nossos preços e promoções</h3>

                    @foreach($precos as $preco)
                    <div class="preco">
                        <div class="texto">
                            <h4>{{ $preco->titulo }}</h4>

                            <div class="left">
                                <p class="subtitulo">{!! $preco->subtitulo !!}</p>
                                {!! $preco->descricao !!}
                            </div>
                            <div class="right">
                                {!! $preco->observacoes !!}
                            </div>
                        </div>

                        <a href="{{ $preco->link }}" class="chamada">
                            {{ $preco->chamada }}
                        </a>
                    </div>
                    @endforeach
                </div>

                <h2 class="promocoes-titulo">NOSSAS PROMOÇÕES</h2>
                <div class="texto">
                    <h3>Fique atento às vantagens que a Solum preparou pra você!</h3>

                    @foreach($promocoes as $promocao)
                    <div class="promocao">
                        <div class="titulo">
                            <p>{!! $promocao->titulo !!}</p>
                        </div>
                        <div class="descricao">
                            {!! $promocao->descricao !!}
                        </div>
                        <div class="round-top"></div>
                        <div class="round-bottom"></div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
