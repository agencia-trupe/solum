<div class="lightbox-wrapper">
    <p class="titulo">
        <span>CURSO INTENSIVO</span>
        <strong>{{ $curso->titulo }}</strong>
    </p>

    <div class="contato-formulario" style="width:100%;border-top:1px solid #6BB9AB;padding-top:10px">
        <h3>ESTOU INTERESSADO &middot; QUERO SABER MAIS:</h3>
        <form action="" id="form-contato">
            <div class="left">
                <input type="text" name="nome" id="nome" placeholder="nome completo" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone_fixo" id="telefone_fixo" placeholder="telefone fixo">
                <input type="text" name="telefone_celular" id="telefone_celular" placeholder="telefone celular">
            </div>
            <div class="right">
                {!! Form::select('assunto', $assuntos, null, ['class' => 'form-control', 'id' => 'assunto', 'placeholder' => 'selecione o assunto', 'required' => true]) !!}
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="hidden" name="curso" id="curso" value="Curso Intensivo: {{ $curso->titulo }}">
                <div id="form-contato-response"></div>
                <input type="submit" value="enviar">
            </div>
        </form>
    </div>
</div>
