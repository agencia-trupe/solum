<a href="{{ route('aulas.ritmos') }}" @if(str_is('aulas.ritmos', Route::currentRouteName()) || str_is('aulas.index', Route::currentRouteName())) class="active" @endif>Ritmos</a>
<a href="{{ route('aulas.grade-de-aulas.index') }}" @if(str_is('aulas.grade-de-aulas.*', Route::currentRouteName())) class="active" @endif>Grade de Aulas</a>
<a href="{{ route('aulas.cursos-regulares') }}" @if(str_is('aulas.cursos-regulares', Route::currentRouteName())) class="active" @endif>Cursos Regulares</a>
<a href="{{ route('aulas.cursos-intensivos') }}" @if(str_is('aulas.cursos-intensivos', Route::currentRouteName())) class="active" @endif>Cursos Intensivos</a>
<a href="{{ route('aulas.precos') }}" @if(str_is('aulas.precos', Route::currentRouteName())) class="active" @endif>Preços</a>
