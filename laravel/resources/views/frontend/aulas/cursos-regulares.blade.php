@extends('frontend.common.template')

@section('content')

    <div class="main cursos-regulares institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.aulas._nav')
            </div>

            <div class="institucional-content">
                <h2>AULAS &middot; CURSOS REGULARES</h2>
                <div class="texto">
                    <h3>{{ $textos->titulo }}</h3>
                    {!! $textos->texto !!}
                    <a href="{{ route('aulas.grade-de-aulas.index') }}" class="chamada-grade">
                        <div>
                            Confira nossa
                            <span>Grade de Aulas</span>
                            e venha aprender a dançar com a gente!
                        </div>
                    </a>
                </div>
                <div class="imagens-laterais">
                    @foreach($imagensLaterais as $imagem)
                    <img src="{{ asset('assets/img/cursos-regulares/imagens-laterais/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
