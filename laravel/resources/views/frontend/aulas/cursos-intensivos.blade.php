@extends('frontend.common.template')

@section('content')

    <div class="main cursos-intensivos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.aulas._nav')
            </div>

            <div class="institucional-content">
                <h2>AULAS &middot; CURSOS INTENSIVOS</h2>
                <div class="texto">
                    <h3>{{ $chamada->chamada }}</h3>

                    @foreach($cursos as $curso)
                    <div class="curso">
                        <div class="left">
                            <img src="{{ asset('assets/img/cursos-intensivos/'.$curso->imagem) }}" alt="">
                            <div>
                                <h4>{{ $curso->titulo }}</h4>
                                {!! $curso->descricao !!}
                            </div>
                        </div>

                        <div class="right">
                            <h4>
                                INVESTIMENTO
                                <span class="mobile">{{ $curso->titulo }}</span>
                            </h4>
                            {!! $curso->investimento !!}
                        </div>

                        <a href="#" class="chamada" id="{{ $curso->id }}">
                            {{ $curso->chamada }}
                        </a>
                    </div>
                    @endforeach

                    <a href="{{ route('aulas.grade-de-aulas.index') }}" class="chamada-grade">
                        <div>
                            Confira nossa
                            <span>Grade de Aulas</span>
                            e venha aprender a dançar com a gente!
                        </div>
                    </a>
                    <a href="{{ route('aulas.cursos-regulares') }}" class="chamada-regulares">
                        <p>Conheça também nossos CURSOS REGULARES e venha dançar com a gente!</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <style>
        .left a, .right a {
            text-decoration: underline;
        }
        .left a:hover, .right a:hover {
            text-decoration: none;
        }
        .left a { color: #3b887b; }
        .right a { color: #ffffff; }
    </style>

@endsection
