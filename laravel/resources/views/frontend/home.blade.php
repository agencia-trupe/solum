@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners center">
            @foreach($banners as $banner)
                @if($banner->link)
                <a href="{{ $banner->link }}" class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                    <div class="wrapper">
                        @if($banner->titulo) <h3>{{ $banner->titulo }}</h3> @endif
                        @if($banner->frase) <p>{{ $banner->frase }}</p> @endif
                    </div>
                </a>
                @else
                <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                    <div class="wrapper">
                        @if($banner->titulo) <h3>{{ $banner->titulo }}</h3> @endif
                        @if($banner->frase) <p>{{ $banner->frase }}</p> @endif
                    </div>
                </div>
                @endif
            @endforeach
            <div class="cycle-pager"></div>
        </div>

        <div class="ritmos center">
            <p>SOLUM &middot; ESCOLA DE DANÇA DE SALÃO &middot; SÃO PAULO</p>
            <div>
                @foreach($ritmos as $titulo => $slug)
                <a href="{{ route('aulas.ritmos', $slug) }}">{{ $titulo }}</a>
                @endforeach
            </div>
        </div>

        <div class="chamadas center">
            <div class="wrapper">
                @foreach(range(1,4) as $i)
                <a href="{{ $chamadas->{'chamada_'.$i.'_link'} }}">
                    <p>{!! $chamadas->{'chamada_'.$i} !!}</p>
                </a>
                @endforeach
            </div>
        </div>

        <div class="eventos-mural">
            <div class="center">
                <div class="home-eventos">
                    <h4>PRÓXIMOS EVENTOS DO NOSSO CALENDÁRIO</h4>

                    <div>
                    @foreach($eventos as $evento)
                    <?php $data = Tools::parseDataCalendario($evento->data); ?>
                    <a href="{{ route('calendario.eventos') }}" class="evento">
                        <div class="data">
                            <div>
                                <span class="dia">{{ $data['dia'] }}</span>
                                <span class="mes">{{ $data['mes'] }}</span>
                                <span class="ano">{{ $data['ano'] }}</span>
                                <span class="semana">{{ $data['semana'] }}</span>
                            </div>
                        </div>

                        <p>{{ $evento->titulo }}</p>
                    </a>
                    @endforeach
                    </div>

                    <a href="{{ route('calendario.eventos') }}" class="todos">
                        CALENDÁRIO COMPLETO &raquo;
                    </a>
                </div>

                <div class="home-mural">
                    <h4>ÚLTIMAS DO MURAL ONLINE</h4>

                    @foreach($avisos as $aviso)
                    <p>{!! $aviso->aviso !!}</p>
                    @endforeach

                    <a href="{{ route('mural.avisos') }}" class="todos">
                        MURAL ONLINE &raquo;
                    </a>
                </div>
            </div>
        </div>

        <div class="videos">
            <div class="center">
                @if(count($videos))
                <div class="video-principal">
                    <div class="video">
                        <div class="embed-video">
                            <iframe src="{{ $videos->first()->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$videos->first()->video_codigo : 'https://player.vimeo.com/video/'.$videos->first()->video_codigo }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                        </div>
                        <span>{{ $videos->first()->titulo }}</span>
                    </div>
                </div>

                @unless(count($videos) < 2)
                <div class="videos-thumbs">
                    <div>
                    @foreach($videos as $video)
                    @unless($video === $videos->first())
                    <a href="{{ $video->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$video->video_codigo.'?autoplay=1' : 'https://player.vimeo.com/video/'.$video->video_codigo }}" class="lightbox-video">
                        <img src="{{ asset('assets/img/videos/'.$video->capa) }}" alt="">
                        <span>{{ $video->titulo }}</span>
                    </a>
                    @endunless
                    @endforeach
                    </div>
                    <a href="{{ route('a-solum.videos-e-fotos.index') }}" class="todos">Ver todos os vídeos &raquo;</a>
                </div>
                @endunless
                @endif
            </div>
        </div>

        <div class="hospedagem">
            <div class="center">
                <h3>DICAS DE HOSPEDAGEM</h3>
                <p>Confira dicas de lugares para se hospedar próximos à Solum. Venha fazer seu curso nas férias em São Paulo!</p>

                @foreach($hospedagem as $estabelecimento)
                <a href="{{ route('localizacao.hospedagem') }}">
                    <img src="{{ asset('assets/img/hospedagem/'.$estabelecimento->imagem) }}" alt="">
                    <p>
                        {{ $estabelecimento->nome }}
                        <span>{!! $estabelecimento->endereco !!}</span>
                    </p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
