@extends('frontend.common.template')

@section('content')

    <div class="main historico institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.a-solum._nav')
            </div>

            <div class="institucional-content">
                <h2>A SOLUM &middot; HISTÓRICO</h2>
                <div class="texto">
                    {!! $texto->texto !!}
                </div>
                <div class="imagens-laterais">
                    @foreach($imagensLaterais as $imagem)
                    <img src="{{ asset('assets/img/historico/imagens-laterais/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>

                <div class="imagens">
                    @foreach($imagens as $imagem)
                    <a href="{{ asset('assets/img/historico/imagens/'.$imagem->imagem) }}" class="lightbox-imagem" rel="galeria">
                        <img src="{{ asset('assets/img/historico/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
