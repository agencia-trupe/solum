<a href="{{ route('a-solum.historico') }}" @if(Route::currentRouteName() === 'a-solum.historico' || Route::currentRouteName() === 'a-solum.index') class="active" @endif>Histórico</a>
<a href="{{ route('a-solum.estrutura') }}" @if(Route::currentRouteName() === 'a-solum.estrutura') class="active" @endif>Estrutura</a>
<a href="{{ route('a-solum.professores.index') }}" @if(str_is('a-solum.professores.*', Route::currentRouteName())) class="active" @endif>Professores</a>
<a href="{{ route('a-solum.vantagens') }}" @if(Route::currentRouteName() === 'a-solum.vantagens') class="active" @endif>Vantagens</a>
<a href="{{ route('a-solum.videos-e-fotos.index') }}" @if(str_is('a-solum.videos-e-fotos.*', Route::currentRouteName())) class="active" @endif>Vídeos e Fotos</a>
