@extends('frontend.common.template')

@section('content')

    <div class="main estrutura institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.a-solum._nav')
            </div>

            <div class="institucional-content">
                <h2>A SOLUM &middot; ESTRUTURA</h2>
                <div class="texto">
                    {!! $texto->texto !!}
                </div>
                <div class="imagens-laterais">
                    @foreach($imagensLaterais as $imagem)
                    <img src="{{ asset('assets/img/estrutura/imagens-laterais/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>

                <div class="imagens">
                    @foreach($imagens as $imagem)
                    <a href="{{ asset('assets/img/estrutura/imagens/'.$imagem->imagem) }}" class="lightbox-imagem" rel="galeria">
                        <img src="{{ asset('assets/img/estrutura/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
