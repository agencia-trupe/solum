@extends('frontend.common.template')

@section('content')

    <div class="main professores institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.a-solum._nav')
            </div>

            <div class="institucional-content">
                <h2>A SOLUM &middot; PROFESSORES</h2>
                <div class="professores-index">
                    @foreach($professores as $professor)
                    <a href="{{ route('a-solum.professores.show', $professor->slug) }}">
                        <img src="{{ asset('assets/img/professores/'.$professor->imagem) }}" alt="">

                        <p class="nome">{{ $professor->nome }}</p>

                        <div class="ritmos">
                            @foreach($professor->ritmos as $ritmo)
                            <span>{{ $ritmo->titulo }}</span>
                            @endforeach
                        </div>

                        <span class="mais">saber mais &raquo;</span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
