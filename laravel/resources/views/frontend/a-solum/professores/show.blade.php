@extends('frontend.common.template')

@section('content')

    <div class="main professores institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.a-solum._nav')
            </div>

            <div class="institucional-content">
                <h2>A SOLUM &middot; PROFESSORES</h2>
                <div class="professores-show">
                    <div class="professor-informacoes">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/professores/'.$professor->imagem) }}" alt="">
                        </div>
                        <div class="ritmos">
                            <p>Ritmos:</p>
                            @foreach($professor->ritmos as $ritmo)
                            <a href="{{ route('aulas.ritmos', $ritmo->slug) }}">{{ $ritmo->titulo }}</a>
                            @endforeach
                        </div>
                    </div>
                    <div class="professor-texto">
                        <h4>{{ $professor->nome }}</h4>
                        {!! $professor->descricao !!}
                    </div>
                    <div class="professor-videos">
                        @foreach($professor->videos as $video)
                        <a href="{{ $video->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$video->video_codigo.'?autoplay=1' : 'https://player.vimeo.com/video/'.$video->video_codigo }}" class="lightbox-video">
                            <img src="{{ asset('assets/img/videos/'.$video->capa) }}" alt="">
                        </a>
                        @endforeach
                    </div>
                </div>
                <div class="imagens">
                    @foreach($professor->imagens as $imagem)
                    <a href="{{ asset('assets/img/professores/imagens/'.$imagem->imagem) }}" class="lightbox-imagem" rel="galeria">
                        <img src="{{ asset('assets/img/professores/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="outros-professores">
            <div class="center">
                <h3>VEJA MAIS PROFESSORES</h3>
                <div class="outros-professores-thumbs">
                    @foreach($outrosProfessores as $professor)
                    <a href="{{ route('a-solum.professores.show', $professor->slug) }}">
                        <img src="{{ asset('assets/img/professores/'.$professor->imagem) }}" alt="">

                        <p class="nome">{{ $professor->nome }}</p>

                        <div class="ritmos">
                            @foreach($professor->ritmos as $ritmo)
                            <span>{{ $ritmo->titulo }}</span>
                            @endforeach
                        </div>

                        <span class="mais">saber mais &raquo;</span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
