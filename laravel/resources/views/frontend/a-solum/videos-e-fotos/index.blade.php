@extends('frontend.common.template')

@section('content')

    <div class="main videos-e-fotos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.a-solum._nav')
            </div>

            <div class="institucional-content">
                <h2>A SOLUM &middot; VÍDEOS E FOTOS</h2>
                <div class="videos-e-fotos-filtros">
                    <div class="ritmo">
                        <label>
                            Exibir só as galerias relacionadas ao ritmo:
                        </label>
                        {!! Form::select('ritmo', $ritmos, (isset($ritmo) ? $ritmo : null), ['class' => 'filtro-ritmo', 'placeholder' => 'FILTRAR POR RITMO', 'data-route' => 'a-solum/videos-e-fotos']) !!}
                    </div>
                    <div class="busca">
                        <form action="{{ route('a-solum.videos-e-fotos.busca') }}" method="GET">
                            <input type="text" name="palavra-chave" placeholder="busca por palavra-chave" value="{{ $termo or '' }}" required>
                            <input type="submit" value="enviar">
                        </form>
                    </div>
                </div>
                <div class="videos-e-fotos-index">
                    @foreach($galerias as $galeria)
                    <a href="{{ route('a-solum.videos-e-fotos.show', $galeria->slug) }}">
                        <img src="{{ asset('assets/img/videos-e-fotos/'.$galeria->capa) }}" alt="">
                        <p>
                            {{ $galeria->titulo }}
                            <span>{{ Tools::dataGaleria($galeria->data) }}</span>
                        </p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
