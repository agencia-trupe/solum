@extends('frontend.common.template')

@section('content')

    <div class="main videos-e-fotos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.a-solum._nav')
            </div>

            <div class="institucional-content">
                <h2>A SOLUM &middot; VÍDEOS E FOTOS</h2>
                <div class="videos-e-fotos-show">
                    <h3>
                        {{ $galeria->titulo }}
                        <span>{{ Tools::dataGaleria($galeria->data) }}</span>
                    </h3>

                    @if(count($galeria->videos))
                    <div class="videos">
                        <div class="video-principal">
                            <div class="video">
                                <div class="embed-video">
                                    <iframe src="{{ $galeria->videos->first()->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$galeria->videos->first()->video_codigo : 'https://player.vimeo.com/video/'.$galeria->videos->first()->video_codigo }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                                </div>
                                <span>{{ $galeria->videos->first()->titulo }}</span>
                            </div>
                        </div>

                        @unless(count($galeria->videos) < 2)
                        <div class="videos-thumbs">
                            @foreach($galeria->videos as $video)
                            @unless($video === $galeria->videos->first())
                            <a href="{{ $video->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$video->video_codigo.'?autoplay=1' : 'https://player.vimeo.com/video/'.$video->video_codigo }}" class="lightbox-video">
                                <div class="imagem">
                                    <img src="{{ asset('assets/img/videos/'.$video->capa) }}" alt="">
                                </div>
                                <span>{{ $video->titulo }}</span>
                            </a>
                            @endunless
                            @endforeach
                        </div>
                        @endunless
                    </div>
                    @endif

                    @if(count($galeria->imagens))
                    <div class="imagens-masonry">
                        <div class="gutter-sizer"></div>
                        <div class="grid-sizer"></div>
                        @foreach($galeria->imagens as $imagem)
                        <a href="{{ asset('assets/img/videos-e-fotos/imagens/'.$imagem->imagem) }}" class="lightbox-imagem thumb-masonry" rel="galeria">
                            <img src="{{ asset('assets/img/videos-e-fotos/imagens/thumbs-lg/'.$imagem->imagem) }}" alt="" title="">
                        </a>
                        @endforeach
                    </div>
                    @endif

                </div>
                <a href="{{ route('a-solum.videos-e-fotos.index') }}" class="voltar">VOLTAR</a>
            </div>
        </div>
    </div>

@endsection
