<ul>
    <li>
        <a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>Home</a>
    </li>
    <li>
        <a href="{{ route('a-solum.index') }}" @if(str_is('a-solum.*', Route::currentRouteName())) class="active" @endif data-sub="true">A Solum</a>
        <ul>
            <li>
                <a href="{{ route('a-solum.historico') }}" @if(Route::currentRouteName() === 'a-solum.historico') class="active" @endif>Histórico</a>
            </li>
            <li>
                <a href="{{ route('a-solum.estrutura') }}" @if(Route::currentRouteName() === 'a-solum.estrutura') class="active" @endif>Estrutura</a>
            </li>
            <li>
                <a href="{{ route('a-solum.professores.index') }}" @if(str_is('a-solum.professores.*', Route::currentRouteName())) class="active" @endif>Professores</a>
            </li>
            <li>
                <a href="{{ route('a-solum.vantagens') }}" @if(Route::currentRouteName() === 'a-solum.vantagens') class="active" @endif>Vantagens</a>
            </li>
            <li>
                <a href="{{ route('a-solum.videos-e-fotos.index') }}" @if(str_is('a-solum.videos-e-fotos.*', Route::currentRouteName())) class="active" @endif>Vídeos e Fotos</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('aulas.index') }}" @if(str_is('aulas.*', Route::currentRouteName())) class="active" @endif data-sub="true">Aulas</a>
        <ul>
            <li>
                <a href="{{ route('aulas.ritmos') }}" @if(str_is('aulas.ritmos', Route::currentRouteName())) class="active" @endif>Ritmos</a>
            </li>
            <li>
                <a href="{{ route('aulas.grade-de-aulas.index') }}" @if(str_is('aulas.grade-de-aulas.*', Route::currentRouteName())) class="active" @endif>Grade de Aulas</a>
            </li>
            <li>
                <a href="{{ route('aulas.cursos-regulares') }}" @if(str_is('aulas.cursos-regulares', Route::currentRouteName())) class="active" @endif>Cursos Regulares</a>
            </li>
            <li>
                <a href="{{ route('aulas.cursos-intensivos') }}" @if(str_is('aulas.cursos-intensivos', Route::currentRouteName())) class="active" @endif>Cursos Intensivos</a>
            </li>
            <li>
                <a href="{{ route('aulas.precos') }}" @if(str_is('aulas.precos', Route::currentRouteName())) class="active" @endif>Preços</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('calendario.index') }}" @if(str_is('calendario.*', Route::currentRouteName())) class="active" @endif data-sub="true">Calendário</a>
        <ul>
            <li>
                <a href="{{ route('calendario.eventos') }}" @if(str_is('calendario.eventos', Route::currentRouteName())) class="active" @endif>Eventos</a>
            </li>
            <li>
                <a href="{{ route('calendario.turmas-e-aulas') }}" @if(str_is('calendario.turmas-e-aulas', Route::currentRouteName())) class="active" @endif>Turmas e Aulas</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('mais-servicos') }}" @if(str_is('mais-servicos', Route::currentRouteName())) class="active" @endif data-sub="true">Mais Serviços</a>
        <ul>
            @foreach($servicosFooter as $titulo => $slug)
            <li>
                <a href="{{ route('mais-servicos', $slug) }}" @if(isset($servicoSelecionado) && $servicoSelecionado == $slug) class="active" @endif>{{ $titulo }}</a>
            </li>
            @endforeach
        </ul>
    </li>
    <li>
        <a href="{{ route('mural.index') }}" @if(str_is('mural.*', Route::currentRouteName())) class="active" @endif data-sub="true">Mural Online</a>
        <ul>
            <li>
                <a href="{{ route('mural.avisos') }}" @if(str_is('mural.avisos', Route::currentRouteName())) class="active" @endif>Avisos</a>
            </li>
            <li>
                <a href="{{ route('mural.onde-dancar') }}" @if(str_is('mural.onde-dancar', Route::currentRouteName())) class="active" @endif>Onde Dançar</a>
            </li>
            <li>
                <a href="{{ route('mural.onde-comprar') }}" @if(str_is('mural.onde-comprar', Route::currentRouteName())) class="active" @endif>Onde Comprar</a>
            </li>
            <li>
                <a href="{{ route('mural.enquetes') }}" @if(str_is('mural.enquetes*', Route::currentRouteName())) class="active" @endif>Enquete</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('localizacao.index') }}" @if(str_is('localizacao.*', Route::currentRouteName())) class="active" @endif data-sub="true">Localização</a>
        <ul>
            <li>
                <a href="{{ route('localizacao.como-chegar') }}" @if(str_is('localizacao.como-chegar', Route::currentRouteName())) class="active" @endif>Como Chegar</a>
            </li>
            <li>
                <a href="{{ route('localizacao.hospedagem') }}" @if(str_is('localizacao.hospedagem', Route::currentRouteName())) class="active" @endif>Hospedagem</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('fale-conosco.index') }}" @if(Route::currentRouteName() === 'fale-conosco.index') class="active" @endif>Fale Conosco</a>
    </li>
</ul>
