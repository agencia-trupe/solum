    <footer>
        <div class="center">
            <div class="footer-links">
                <div class="col">
                    <a href="{{ route('home') }}" class="link-header">&raquo; HOME</a>

                    <a href="{{ route('a-solum.index') }}" class="link-header">&raquo; A SOLUM</a>
                    <a href="{{ route('a-solum.historico') }}">Histórico</a>
                    <a href="{{ route('a-solum.estrutura') }}">Estrutura</a>
                    <a href="{{ route('a-solum.professores.index') }}">Professores</a>
                    <a href="{{ route('a-solum.vantagens') }}">Vantagens</a>
                    <a href="{{ route('a-solum.videos-e-fotos.index') }}">Vídeos e Fotos</a>

                    <a href="{{ route('aulas.index') }}" class="link-header">&raquo; AULAS</a>
                    <a href="{{ route('aulas.ritmos') }}">Ritmos</a>
                    <a href="{{ route('aulas.grade-de-aulas.index') }}">Grade de aulas</a>
                    <a href="{{ route('aulas.cursos-regulares') }}">Cursos Regulares</a>
                    <a href="{{ route('aulas.cursos-intensivos') }}">Cursos Intensivos</a>
                    <a href="{{ route('aulas.precos') }}">Preços</a>
                </div>

                <div class="col">
                    <a href="{{ route('calendario.index') }}" class="link-header">&raquo; CALENDÁRIO</a>
                    <a href="{{ route('calendario.eventos') }}">Eventos</a>
                    <a href="{{ route('calendario.turmas-e-aulas') }}">Turmas e aulas</a>

                    <a href="{{ route('mais-servicos') }}" class="link-header">&raquo; MAIS SERVIÇOS</a>
                    @foreach($servicosFooter as $titulo => $slug)
                    <a href="{{ route('mais-servicos', $slug) }}">{{ $titulo }}</a>
                    @endforeach

                    <a href="{{ route('mural.index') }}" class="link-header">&raquo; MURAL ONLINE</a>
                    <a href="{{ route('mural.avisos') }}">Avisos</a>
                    <a href="{{ route('mural.onde-dancar') }}">Onde dançar</a>
                    <a href="{{ route('mural.onde-comprar') }}">Onde comprar</a>
                    <a href="{{ route('mural.enquetes') }}">Enquete</a>
                </div>

                <div class="col">
                    <a href="{{ route('localizacao.index') }}" class="link-header">&raquo; LOCALIZAÇÃO</a>
                    <a href="{{ route('localizacao.como-chegar') }}">Como chegar</a>
                    <a href="{{ route('localizacao.hospedagem') }}">Hospedagem</a>

                    <a href="{{ route('fale-conosco.index') }}" class="link-header">&raquo; FALE CONOSCO</a>
                </div>

                <div class="col col-ritmos">
                    @foreach($ritmosFooter as $titulo => $slug)
                    <a href="{{ route('aulas.ritmos', $slug) }}">{{ $titulo }}</a>
                    @endforeach
                </div>
            </div>

            <div class="footer-info">
                <img src="{{ asset('assets/img/layout/marca-solum-preto.png') }}" alt="">
                <div class="telefones">
                @foreach(Tools::telefones($contato->telefones) as $telefone)
                    <p>
                        <?php
                            $telefone = explode(' ', $telefone);
                            if (count($telefone) > 2) {
                                $prefixo = array_splice($telefone, 0, 2);
                                $prefixo = implode(' ', $prefixo);
                            } else {
                                $prefixo = array_shift($telefone);
                            }
                            $numero = implode(' ', $telefone);
                        ?>
                        <span>{{ $prefixo }}</span> {{ $numero }}
                    </p>
                @endforeach
                </div>
                <p>
                    © {{ date('Y') }} Solum Escola de Dança de Salão
                    <br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
