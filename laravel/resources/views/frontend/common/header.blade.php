    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>

            <nav id="nav-desktop">
                @include('frontend.common._nav')
            </nav>

            <div class="social">
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
                @endif
                @if($contato->youtube)
                <a href="{{ $contato->youtube }}" class="youtube" target="_blank"></a>
                @endif
            </div>

            <div class="telefones">
            @foreach(Tools::telefones($contato->telefones) as $telefone)
                <p>
                    <?php
                        $telefone = explode(' ', $telefone);
                        if (count($telefone) > 2) {
                            $prefixo = array_splice($telefone, 0, 2);
                            $prefixo = implode(' ', $prefixo);
                        } else {
                            $prefixo = array_shift($telefone);
                        }
                        $numero = implode(' ', $telefone);
                    ?>
                    <span>{{ $prefixo }}</span> {{ $numero }}
                </p>
            @endforeach
            </div>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common._nav')
        </nav>
    </header>
