@extends('frontend.common.template')

@section('content')

    <div class="main fale-conosco institucional">
        <div class="center">
            <div class="institucional-aside">
                <a href="{{ route('fale-conosco.index') }}" class="active">Fale Conosco</a>
                <a href="{{ route('localizacao.como-chegar') }}">Como Chegar</a>
            </div>

            <div class="institucional-content">
                <h2>FALE CONOSCO</h2>
                <div class="texto">
                    <div class="telefones">
                    @foreach(Tools::telefones($contato->telefones) as $telefone)
                        <p>
                            <?php
                                $telefone = explode(' ', $telefone);
                                if (count($telefone) > 2) {
                                    $prefixo = array_splice($telefone, 0, 2);
                                    $prefixo = implode(' ', $prefixo);
                                } else {
                                    $prefixo = array_shift($telefone);
                                }
                                $numero = implode(' ', $telefone);
                            ?>
                            <span>{{ $prefixo }}</span> {{ $numero }}
                        </p>
                    @endforeach
                    </div>

                    <div class="atendimento">
                        <strong>HORÁRIOS DE ATENDIMENTO:</strong><br>
                        {!! $contato->horarios_de_atendimento !!}
                    </div>
                </div>

                <div class="imagens-laterais">
                    <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                </div>

                <div class="contato-formulario">
                    <h3>ESTOU INTERESSADO &middot; QUERO SABER MAIS:</h3>
                    <form action="" id="form-contato">
                        <div class="left">
                            <input type="text" name="nome" id="nome" placeholder="nome completo" required>
                            <input type="email" name="email" id="email" placeholder="e-mail" required>
                            <input type="text" name="telefone_fixo" id="telefone_fixo" placeholder="telefone fixo">
                            <input type="text" name="telefone_celular" id="telefone_celular" placeholder="telefone celular">
                        </div>
                        <div class="right">
                            {!! Form::select('assunto', $assuntos, null, ['class' => 'form-control', 'id' => 'assunto', 'placeholder' => 'selecione o assunto', 'required' => true]) !!}
                            {!! Form::select('ritmo', $ritmos, null, ['class' => 'form-control', 'id' => 'ritmo', 'placeholder' => 'ritmo [selecione um ritmo se for o caso]']) !!}
                            <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                            <div id="form-contato-response"></div>
                            <input type="submit" value="enviar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
