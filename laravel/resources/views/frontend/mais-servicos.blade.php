@extends('frontend.common.template')

@section('content')

    <div class="main mais-servicos institucional">
        <div class="center">
            <div class="institucional-aside">
                @foreach($servicosLista as $titulo => $slug)
                <a href="{{ route('mais-servicos', $slug) }}" @if($servico->slug === $slug) class="active" @endif>{{ $titulo }}</a>
                @endforeach
            </div>

            <div class="institucional-content">
                <h2>MAIS SERVIÇOS &middot; {{ $servico->titulo }}</h2>
                <div class="texto">
                    {!! $servico->texto !!}
                </div>
                <div class="imagens-laterais">
                    @foreach($servico->imagensLaterais as $imagem)
                    <img src="{{ asset('assets/img/mais-servicos/imagens-laterais/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>

                <div class="imagens">
                    @foreach($servico->imagens as $imagem)
                    <a href="{{ asset('assets/img/mais-servicos/imagens/'.$imagem->imagem) }}" class="lightbox-imagem" rel="galeria">
                        <img src="{{ asset('assets/img/mais-servicos/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
