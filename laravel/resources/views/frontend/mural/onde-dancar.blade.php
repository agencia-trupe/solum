@extends('frontend.common.template')

@section('content')

    <div class="main estabelecimentos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.mural._nav')
            </div>

            <div class="institucional-content">
                <h2>MURAL ONLINE &middot; ONDE DANÇAR</h2>
                <div class="texto">
                    <h3>A Solum indica. Vem dançar!</h3>

                    @foreach($estabelecimentos as $estabelecimento)
                    <div class="estabelecimento">
                        <img src="{{ asset('assets/img/onde-dancar/'.$estabelecimento->imagem) }}" alt="">
                        <div class="descricao">
                            <h4>{{ $estabelecimento->nome }}</h4>
                            <p>
                                {!! $estabelecimento->endereco !!}<br>
                                {{ $estabelecimento->telefone }}
                                <a href="{{ !preg_match('/:\/\//', $estabelecimento->link) ? '//' : '' }}{{ $estabelecimento->link }}">{{ $estabelecimento->link }}</a>
                            </p>
                            <p>{!! $estabelecimento->observacoes !!}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
