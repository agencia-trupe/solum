<a href="{{ route('mural.avisos') }}" @if(str_is('mural.avisos', Route::currentRouteName()) || str_is('mural.index', Route::currentRouteName())) class="active" @endif>Avisos</a>
<a href="{{ route('mural.onde-dancar') }}" @if(str_is('mural.onde-dancar', Route::currentRouteName())) class="active" @endif>Onde Dançar</a>
<a href="{{ route('mural.onde-comprar') }}" @if(str_is('mural.onde-comprar', Route::currentRouteName())) class="active" @endif>Onde Comprar</a>
<a href="{{ route('mural.enquetes') }}" @if(str_is('mural.enquetes*', Route::currentRouteName())) class="active" @endif>Enquete</a>
