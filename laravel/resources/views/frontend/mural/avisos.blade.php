@extends('frontend.common.template')

@section('content')

    <div class="main avisos institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.mural._nav')
            </div>

            <div class="institucional-content">
                <h2>MURAL ONLINE &middot; AVISOS</h2>
                <div class="texto">
                    @foreach($avisos as $aviso)
                    <div class="aviso">
                        <div class="categoria">
                            <img src="{{ asset('assets/img/layout/mural-'.$aviso->categoria.'.png') }}" alt="">
                            <span>{{ $aviso->categoria_nome }}</span>
                        </div>
                        <div class="aviso-texto">
                            <p>{!! $aviso->aviso !!}</p>
                        </div>
                        <div class="data">
                            Enviado em:<br>
                            {!! $aviso->data_formatada !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <style>
        .aviso-texto a {
            color: #71D6C0;
            text-decoration: underline;
        }
        .aviso-texto a:hover {
            text-decoration: none;
        }
    </style>

@endsection
