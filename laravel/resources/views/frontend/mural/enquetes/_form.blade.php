<div class="enquetes-lightbox">
    @if(!$opcao)
    <h3>Selecione uma resposta para a enquete.</h3>
    @else
    <form action="{{ route('mural.enquetes.votar') }}" method="POST">
        {!! csrf_field() !!}
        <h3>Insira seu e-mail abaixo para confirmar seu voto:</h3>
        <input type="hidden" name="opcaoVoto" value="{{ $opcao }}">
        <input type="email" name="emailVoto" required placeholder="E-mail">
        <input type="submit" value="confirmar voto">
    </form>
    @endif
</div>
