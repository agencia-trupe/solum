@extends('frontend.common.template')

@section('content')

    <div class="main enquetes institucional">
        <div class="center">
            <div class="institucional-aside">
                @include('frontend.mural._nav')
            </div>

            <div class="institucional-content">
                <h2>MURAL ONLINE &middot; ENQUETE</h2>

                @if(session('erro'))
                <div class="erro">{{ session('erro') }}</div>
                @endif
                @if(session('sucesso'))
                <div class="sucesso">{{ session('sucesso') }}</div>
                @endif

                @if(isset($enquete->id))
                <div class="enquete-ativa">
                    <div class="titulo">
                        <h1>ENQUETE <span>#{{ $enquete->id }}</span></h1>
                        <div class="data">{!! Tools::dataEnquete($enquete->data) !!}</div>
                    </div>
                    @if(session()->get('enqueteVotada') == $enquete->id)
                        <div class="resultados">
                            <h3>{{ $enquete->pergunta }}</h3>
                            @foreach($enquete->opcoes as $opcao)
                            <div class="porcentagem">
                                <div class="barra">
                                    <div style="width:{{ Tools::porcentagemDeVotos($opcao->id) }}"></div>
                                </div>
                                <p>
                                    <span>{{ Tools::porcentagemDeVotos($opcao->id) }}</span>
                                    | {{ $opcao->texto }}
                                </p>
                            </div>
                            @endforeach
                        </div>
                    @else
                        <form action="{{ route('mural.enquetes.votar') }}" data-url="{{ route('mural.enquetes.formVotar') }}" id="form-enquete" method="POST">
                            {!! csrf_field() !!}
                            <h2>{{ $enquete->pergunta }}</h2>
                            @foreach($enquete->opcoes as $opcao)
                                <label>
                                    <input type="radio" name="opcao" value="{{ $opcao->id }}">
                                    <span class="radio"></span>
                                    <span>{{ $opcao->texto }}</span>
                                </label>
                            @endforeach
                            <input type="submit" value="VOTAR">
                            <a href="{{ route('mural.enquetes.resultados', $enquete->id) }}">
                                ver resultados &raquo;
                            </a>
                        </form>
                    @endif
                </div>
                @endif

                @if(count($historico))
                <div class="enquete-historico">
                    @foreach($historico as $enquete)
                    <a href="{{ route('mural.enquetes.resultados', $enquete->id) }}">
                        <span>{{ Tools::dataEnquete($enquete->data) }}</span>
                        <span>{{ $enquete->pergunta }}</span>
                    </a>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>

@stop
