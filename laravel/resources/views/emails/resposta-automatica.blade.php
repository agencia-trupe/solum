<!DOCTYPE html>
<html>
<head>
    <title>{{ $assunto->assunto_do_e_mail }}</title>
    <meta charset="utf-8">
    <style>
        body {
            font-size: 14px;
            font-family: Verdana;
            line-height: 1.3;
        }
    </style>
</head>
<body>
    {!! $assunto->resposta_automatica !!}

    <hr>

    <span style='color:#000;font-size:14px;font-family:Verdana;'>Abaixo a mensagem enviada para a Solum:</span><br>
    @if($curso)
        <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Curso:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $curso }}</span><br>
    @endif
    @if($ritmo)
        <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Ritmo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $ritmo }}</span><br>
    @endif
        <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span>
</body>
</html>
