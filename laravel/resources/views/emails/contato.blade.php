<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
@if($telefone_fixo)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone Fixo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone_fixo }}</span><br>
@endif
@if($telefone_celular)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone Celular:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone_celular }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Assunto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $assunto }}</span><br>
@if($curso)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Curso:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $curso }}</span><br>
@endif
@if($ritmo)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Ritmo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $ritmo }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span>
</body>
</html>
