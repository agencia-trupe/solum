@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.aulas.cursos-intensivos.index') }}" title="Voltar para Cursos Intensivos" class="btn btn-sm btn-default">
        &larr; Voltar para Cursos Intensivos
    </a>

    <legend>
        <h2><small>Aulas / Cursos Intensivos /</small> Chamada</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.cursos-intensivos.chamada.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cursos-intensivos-chamada.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
