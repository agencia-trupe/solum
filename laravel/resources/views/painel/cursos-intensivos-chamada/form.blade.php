@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('chamada', 'Chamada') !!}
    {!! Form::textarea('chamada', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
