@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Estrutura / Imagens Laterais /</small> Adicionar Imagem Lateral</h2>
    </legend>

    {!! Form::open(['route' => ['painel.a-solum.estrutura.imagens-laterais.store'], 'files' => true]) !!}

        @include('painel.estrutura.imagens-laterais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
