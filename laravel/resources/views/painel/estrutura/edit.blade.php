@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>A Solum /</small> Estrutura
            <div class="btn-group pull-right">
                <a href="{{ route('painel.a-solum.estrutura.imagens-laterais.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens Laterais</a>
                <a href="{{ route('painel.a-solum.estrutura.imagens.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Galeria de Imagens</a>
            </div>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-solum.estrutura.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.estrutura.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
