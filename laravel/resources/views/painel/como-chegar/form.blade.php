@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/como-chegar/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'comoChegarEndereco']) !!}
</div>

<div class="form-group">
    {!! Form::label('observacoes', 'Observações') !!}
    {!! Form::textarea('observacoes', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('codigo_googlemaps', 'Código GoogleMaps') !!}
    {!! Form::text('codigo_googlemaps', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
