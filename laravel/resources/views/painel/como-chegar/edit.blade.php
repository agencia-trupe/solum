@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Localização /</small> Como Chegar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.localizacao.como-chegar.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.como-chegar.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
