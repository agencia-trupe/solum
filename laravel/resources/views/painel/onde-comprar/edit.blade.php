@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mural / Onde Comprar /</small> Editar Estabelecimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mural.onde-comprar.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.onde-comprar.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
