@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Ritmos /</small> Adicionar Ritmo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aulas.ritmos.store', 'files' => true]) !!}

        @include('painel.ritmos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
