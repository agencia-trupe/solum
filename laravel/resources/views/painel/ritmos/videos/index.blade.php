@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.aulas.ritmos.index') }}" title="Voltar para Ritmos" class="btn btn-sm btn-default">
        &larr; Voltar para Ritmos
    </a>

    <legend>
        <h2>
            <small>Aulas / Ritmos / {{ $ritmo->titulo }} /</small> Vídeos
            @unless($limiteExcedido)
            <a href="{{ route('painel.aulas.ritmos.videos.create', $ritmo->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Vídeo</a>
            @endunless
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="ritmos_videos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th>Capa</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->titulo }}</td>
                <td><img src="{{ asset('assets/img/videos/'.$registro->capa) }}" style="max-width:150px" alt=""></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.aulas.ritmos.videos.destroy', $ritmo->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.aulas.ritmos.videos.edit', [$ritmo->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
