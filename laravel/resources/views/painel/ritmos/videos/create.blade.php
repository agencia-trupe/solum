@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Ritmos / {{ $ritmo->titulo }} / Vídeos /</small> Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => ['painel.aulas.ritmos.videos.store', $ritmo->id], 'files' => true]) !!}

        @include('painel.ritmos.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
