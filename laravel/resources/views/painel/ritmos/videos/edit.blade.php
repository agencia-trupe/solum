@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Ritmos / {{ $ritmo->titulo }} / Vídeos /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.ritmos.videos.update', $ritmo->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.ritmos.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
