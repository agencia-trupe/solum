@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Ritmos /</small> Editar Ritmo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.ritmos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.ritmos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
