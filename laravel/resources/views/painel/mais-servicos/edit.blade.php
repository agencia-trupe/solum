@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mais Serviços /</small> Editar Serviço</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mais-servicos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.mais-servicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
