@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.mais-servicos.index') }}" title="Voltar para Mais Serviços" class="btn btn-sm btn-default">
        &larr; Voltar para Mais Serviços
    </a>

    <legend>
        <h2>
            <small>Mais Serviços / {{ $servico->titulo }} /</small> Imagens Laterais
            <a href="{{ route('painel.mais-servicos.imagens-laterais.create', $servico->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Imagem Lateral</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="mais_servicos_imagens_laterais">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ asset('assets/img/mais-servicos/imagens-laterais/'.$registro->imagem) }}" style="width: 100%; max-width:150px;" alt=""></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.mais-servicos.imagens-laterais.destroy', $servico->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
