@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mais Serviços / {{ $servico->titulo }} / Imagens Laterais /</small> Adicionar Imagem Lateral</h2>
    </legend>

    {!! Form::open(['route' => ['painel.mais-servicos.imagens-laterais.store', $servico->id], 'files' => true]) !!}

        @include('painel.mais-servicos.imagens-laterais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
