@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mais Serviços / Imagens Laterais /</small> Editar Imagem Lateral</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mais-servicos-imagens-laterais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.mais-servicos-imagens-laterais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
