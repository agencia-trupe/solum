@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mais Serviços /</small> Adicionar Serviço</h2>
    </legend>

    {!! Form::open(['route' => 'painel.mais-servicos.store', 'files' => true]) !!}

        @include('painel.mais-servicos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
