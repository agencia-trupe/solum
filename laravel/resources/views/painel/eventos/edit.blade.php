@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Calendário / Eventos /</small> Editar Evento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.calendario.eventos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.eventos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
