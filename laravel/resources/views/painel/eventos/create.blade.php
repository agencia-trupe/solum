@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Calendário / Eventos /</small> Adicionar Evento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.calendario.eventos.store', 'files' => true]) !!}

        @include('painel.eventos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
