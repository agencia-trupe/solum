@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mural / Avisos /</small> Editar Aviso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mural.avisos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.avisos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
