@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control datetimepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('categoria', 'Categoria') !!}
    {!! Form::select('categoria', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('aviso', 'Aviso') !!}
    {!! Form::textarea('aviso', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cursosIntensivos']) !!}
</div>

@if($submitText == 'Alterar')
@if($registro->notificacao)
<div class="form-group">
    {!! Form::label('notificacao', 'Notificação no aplicativo') !!}
    {!! Form::text('notificacao', null, ['class' => 'form-control', 'disabled' => true]) !!}
</div>
@endif
@else
<div class="form-group">
    {!! Form::label('notificacao', 'Enviar notificação no aplicativo (opcional)') !!}
    {!! Form::text('notificacao', null, ['class' => 'form-control']) !!}
</div>
@endif

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.mural.avisos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
