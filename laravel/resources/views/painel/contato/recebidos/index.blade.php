@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Contatos Recebidos</h2>
    </legend>

    @if(!count($contatosrecebidos))
    <div class="alert alert-warning" role="alert">Nenhuma mensagem recebida.</div>
    @else
    <form action="{{ route('painel.contato.recebidos.exportar') }}" method="GET">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Data de início</label>
                    <input type="text" name="data_inicio" class="datepicker datepicker-no-default form-control">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Data final</label>
                    <input type="text" name="data_final" class="datepicker datepicker-no-default form-control">
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-block btn-success" style="margin-top:26px">
                    <span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>
                    Exportar XLS
                </button>
            </div>
        </div>
    </form>
    <hr>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Assunto</th>
                <th>Ritmo</th>
                <th style="min-width:230px"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($contatosrecebidos as $contato)

            <tr class="tr-row @if(!$contato->lido) warning @endif">
                <td>{{ $contato->created_at }}</td>
                <td>{{ $contato->nome }}</td>
                <td>{{ $contato->email }}</td>
                <td>{{ $contato->assunto }}</td>
                <td>{{ $contato->ritmo ?: '-' }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.contato.recebidos.destroy', $contato->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.contato.recebidos.show', $contato->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>

    {!! $contatosrecebidos->render() !!}
    @endif

@stop
