@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Contatos Recebidos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $contato->email }}</div>
    </div>

@if($contato->telefone_fixo)
    <div class="form-group">
        <label>Telefone Fixo</label>
        <div class="well">{{ $contato->telefone_fixo }}</div>
    </div>
@endif

@if($contato->telefone_celular)
    <div class="form-group">
        <label>Telefone Celular</label>
        <div class="well">{{ $contato->telefone_celular }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Assunto</label>
        <div class="well">{{ $contato->assunto }}</div>
    </div>

@if($contato->curso)
    <div class="form-group">
        <label>Curso</label>
        <div class="well">{{ $contato->curso }}</div>
    </div>
@endif

@if($contato->ritmo)
    <div class="form-group">
        <label>Ritmo</label>
        <div class="well">{{ $contato->ritmo }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $contato->mensagem }}</div>
    </div>

    <a href="{{ route('painel.contato.recebidos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
