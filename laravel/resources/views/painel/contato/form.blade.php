@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefones', 'Telefones (separados por vírgula)') !!}
    {!! Form::text('telefones', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('horarios_de_atendimento', 'Horários de Atendimento') !!}
    {!! Form::textarea('horarios_de_atendimento', null, ['class' => 'form-control ckeditor', 'data-editor' => 'comoChegarEndereco']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('facebook', 'Facebook (opcional)') !!}
            {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('youtube', 'YouTube (opcional)') !!}
            {!! Form::text('youtube', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/contato/'.$contato->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
