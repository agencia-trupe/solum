@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Histórico / Imagens Laterais /</small> Adicionar Imagem Lateral</h2>
    </legend>

    {!! Form::open(['route' => ['painel.a-solum.historico.imagens-laterais.store'], 'files' => true]) !!}

        @include('painel.historico.imagens-laterais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
