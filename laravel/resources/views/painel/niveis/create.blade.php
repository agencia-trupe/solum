@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Grade de Aulas / Níveis /</small> Adicionar Nível</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aulas.grade-de-aulas.niveis.store', 'files' => true]) !!}

        @include('painel.niveis.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
