@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('estrutura_do_curso', 'Estrutura do Curso') !!}
    {!! Form::textarea('estrutura_do_curso', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aulas.grade-de-aulas.niveis.index') }}" class="btn btn-default btn-voltar">Voltar</a>
