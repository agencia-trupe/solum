@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Grade de Aulas / Níveis /</small> Editar Nível</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.grade-de-aulas.niveis.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.niveis.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
