@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Contato / Respostas Automáticas /</small> Editar Resposta Automática</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.contato.respostas-automaticas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.respostas-automaticas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
