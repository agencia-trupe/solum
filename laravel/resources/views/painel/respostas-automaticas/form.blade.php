@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('assunto_formulario', 'Assunto (formulário)') !!}
    {!! Form::text('assunto_formulario', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('assunto_do_e_mail', 'Assunto do E-mail') !!}
    {!! Form::text('assunto_do_e_mail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta_automatica', 'Resposta Automática') !!}
    {!! Form::textarea('resposta_automatica', null, ['class' => 'form-control ckeditor', 'data-editor' => 'respostaAutomatica']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.contato.respostas-automaticas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
