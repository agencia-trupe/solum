@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Contato / Respostas Automáticas /</small> Adicionar Resposta Automática</h2>
    </legend>

    {!! Form::open(['route' => 'painel.contato.respostas-automaticas.store', 'files' => true]) !!}

        @include('painel.respostas-automaticas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
