@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Vídeos /</small> Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.home.videos.store', 'files' => true]) !!}

        @include('painel.videos-home.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
