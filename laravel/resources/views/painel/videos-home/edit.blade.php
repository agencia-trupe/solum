@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Vídeos /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.home.videos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.videos-home.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
