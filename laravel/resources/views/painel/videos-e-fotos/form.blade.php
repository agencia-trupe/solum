@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control monthpicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/videos-e-fotos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well">
    {!! Form::label('ritmos', 'Ritmos') !!}
    <div class="row">
        @foreach($ritmos as $ritmo)
        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('ritmos[]', $ritmo->id, (isset($registro) ? ($registro->ritmos->contains($ritmo->id) ? true : false) : false)) !!}
                    {{ $ritmo->titulo }}
                </label>
            </div>
        </div>
        @endforeach
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.a-solum.videos-e-fotos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
