@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Vídeos e Fotos / {{ $galeria->titulo }} / Vídeos /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-solum.videos-e-fotos.videos.update', $galeria->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.videos-e-fotos.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
