@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Vídeos e Fotos / {{ $galeria->titulo }} / Vídeos /</small> Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => ['painel.a-solum.videos-e-fotos.videos.store', $galeria->id], 'files' => true]) !!}

        @include('painel.videos-e-fotos.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
