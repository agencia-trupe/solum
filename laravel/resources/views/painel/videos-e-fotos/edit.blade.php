@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Vídeos e Fotos /</small> Editar Galeria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-solum.videos-e-fotos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.videos-e-fotos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
