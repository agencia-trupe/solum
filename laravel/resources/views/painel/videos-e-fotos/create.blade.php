@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Vídeos e Fotos /</small> Adicionar Galeria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.a-solum.videos-e-fotos.store', 'files' => true]) !!}

        @include('painel.videos-e-fotos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
