@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mural / Enquetes /</small> Editar Enquete</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mural.enquetes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

		@include('painel.common.flash')

		<div class="form-group">
		    {!! Form::label('data', 'Data') !!}
		    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('pergunta', 'Pergunta') !!}
			{!! Form::text('pergunta', null, ['class' => 'form-control']) !!}
		</div>

		<div class="well">
			{!! Form::label('respostas', 'Respostas') !!}

			<div class="enquete-opcoes-wrapper">
			@if(sizeof($registro->opcoes))
				@foreach($registro->opcoes as $opcao)
				<div class="form-group">
					<div class="input-group">
						<input class="form-control enquete-opcao" placeholder="Opção de resposta" style="z-index:0" name="enquete_opcao[]" type="text" value="{{ $opcao->texto }}">
						<span class="input-group-btn">
							<a href="#" class="btn btn-default btn-danger enquetes-remove-opcao">
								<span class="glyphicon glyphicon-remove-circle"></span>
							</a>
						</span>
					</div>
				</div>
				@endforeach
			@endif
			</div>

			<div>
				<a href="#" class="btn btn-success btn-sm enquetes-adiciona-opcao"><span class="glyphicon glyphicon-plus-sign" style="margin-right:10px;"></span>Adicionar opção de resposta</a>
			</div>
		</div>

		{!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

		<a href="{{ route('painel.mural.enquetes.index') }}" class="btn btn-default btn-voltar">Voltar</a>

	{!! Form::close() !!}

@endsection
