@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Mural / Enquetes /</small> Resultados
        </h2>
    </legend>

	<h1>{{ $registro->pergunta }}</h1>

	<hr>

	@if(sizeof($registro->opcoes))
		@foreach($registro->opcoes as $opcao)
			<h4 style="display:inline">{{ $opcao->texto }}</h4>
			-
			{{ Tools::porcentagemDeVotos($opcao->id) }}
			({{ Tools::votosNaOpcao($opcao->id) }})
			<div class="progress progress-striped" style="margin-top:5px;height:22px">
				<div class="progress-bar progress-bar-success" style="width:{{ Tools::porcentagemDeVotos($opcao->id) }};"></div>
			</div>
		@endforeach
	@endif

	<hr>

    <div class="well">
    	Total: {{ Tools::totalDeVotos($registro->id) }}
    </div>

	<a href="{{ route('painel.mural.enquetes.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@endsection
