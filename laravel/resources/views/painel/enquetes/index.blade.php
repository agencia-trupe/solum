@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Mural /</small> Enquetes
            <a href="{{ route('painel.mural.enquetes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Enquete</a>
        </h2>
    </legend>

    <hr>
        <div class="btn-group">
            <a href="{{ route('painel.mural.enquetes.index') }}" class="btn btn-sm btn-default @if(!$historico) btn-info @endif">Enquetes Novas</a>
            <a href="{{ route('painel.mural.enquetes.index', ['historico' => 1]) }}" class="btn btn-sm btn-default @if($historico) btn-info @endif">Histórico</a>
        </div>
    <hr>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <div class="alert alert-warning text-center">
        O <strong>resultado parcial</strong> da enquente ativa fica disponível após o primeiro voto.
        <br>
        Depois de receber votos a enquete não pode mais ser editada.
        <br>
        Enquetes que já possuem votos são armazenadas no Histórico quando desativadas.
    </div>

    <table class='table table-striped table-bordered table-hover'>
        <thead>
            <tr>
                @if(!$historico)
                    <th>Ativa</th>
                @endif
                <th>Data</th>
                <th>Pergunta</th>
                <th style="min-width:265px"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row">
                @if(!$historico)
                    <td>
                        @if($registro->ativa == '1')
                            <span class="label label-success" style="padding:10px;font-size:12px">
                                <span class="glyphicon glyphicon-ok-circle" style="margin-right: 5px"></span>
                                Enquete ativa
                            </span>
                        @else
                            <a href="{{ route('painel.mural.enquetes.ativar', $registro->id) }}" class="enquete-ativar btn btn-default btn-sm">
                                Tornar ativa
                            </a>
                        @endif
                    </td>
                @endif
                <td>{{ $registro->data }}</td>
                <td>{{ $registro->pergunta }}</td>
                <td class="@if(!$historico) crud-actions @else crud-actions-lg @endif" style="width:225px !important;">
                    {!! Form::open([
                        'route'  => ['painel.mural.enquetes.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}
                        <div class="btn-group btn-group-sm">
                    @if(!$historico)
                        @if(sizeof($registro->numeroVotos) == 0)
                            <a href='{{ route('painel.mural.enquetes.edit', $registro->id) }}' class='btn btn-primary btn-sm'><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar</a>
                        @else
                            <a href='{{ route('painel.mural.enquetes.show', $registro->id) }}' class='btn btn-info btn-sm'><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Resultados parciais</a>
                        @endif
                    @else
                        <a href='{{ route('painel.mural.enquetes.show', $registro->id) }}' class='btn btn-sm btn-info'><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Resultados</a>
                    @endif
						<button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
					{{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
