@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home /</small> Chamadas</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.home.chamadas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.chamadas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
