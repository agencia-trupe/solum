@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_1', 'Chamada 1') !!}
            {!! Form::textarea('chamada_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'homeChamadas']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_1_link', 'Chamada 1 Link') !!}
            {!! Form::text('chamada_1_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_2', 'Chamada 2') !!}
            {!! Form::textarea('chamada_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'homeChamadas']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_2_link', 'Chamada 2 Link') !!}
            {!! Form::text('chamada_2_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_3', 'Chamada 3') !!}
            {!! Form::textarea('chamada_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'homeChamadas']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_3_link', 'Chamada 3 Link') !!}
            {!! Form::text('chamada_3_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_4', 'Chamada 4') !!}
            {!! Form::textarea('chamada_4', null, ['class' => 'form-control ckeditor', 'data-editor' => 'homeChamadas']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_4_link', 'Chamada 4 Link') !!}
            {!! Form::text('chamada_4_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
