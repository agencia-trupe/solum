@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Vantagens / Imagens Laterais /</small> Adicionar Imagem Lateral</h2>
    </legend>

    {!! Form::open(['route' => ['painel.a-solum.vantagens.imagens-laterais.store'], 'files' => true]) !!}

        @include('painel.vantagens.imagens-laterais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
