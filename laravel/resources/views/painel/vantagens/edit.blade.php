@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>A Solum /</small> Vantagens
            <div class="btn-group pull-right">
                <a href="{{ route('painel.a-solum.vantagens.imagens-laterais.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens Laterais</a>
                <a href="{{ route('painel.a-solum.vantagens.imagens.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Galeria de Imagens</a>
            </div>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-solum.vantagens.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.vantagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
