@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/professores/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well">
    {!! Form::label('ritmos', 'Ritmos') !!}
    <div class="row">
        @foreach($ritmos as $ritmo)
        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('ritmos[]', $ritmo->id, (isset($registro) ? ($registro->ritmos->contains($ritmo->id) ? true : false) : false)) !!}
                    {{ $ritmo->titulo }}
                </label>
            </div>
        </div>
        @endforeach
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.a-solum.professores.index') }}" class="btn btn-default btn-voltar">Voltar</a>
