@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Professores /</small> Adicionar Professor</h2>
    </legend>

    {!! Form::open(['route' => 'painel.a-solum.professores.store', 'files' => true]) !!}

        @include('painel.professores.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
