@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Professores / {{ $professor->nome }} / Vídeos /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-solum.professores.videos.update', $professor->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.professores.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
