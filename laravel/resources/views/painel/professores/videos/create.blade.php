@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Professores / {{ $professor->nome }} / Vídeos /</small> Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => ['painel.a-solum.professores.videos.store', $professor->id], 'files' => true]) !!}

        @include('painel.professores.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
