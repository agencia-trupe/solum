@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>A Solum /</small> Professores
            <a href="{{ route('painel.a-solum.professores.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Professor</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="professores">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Nome</th>
                <th>Imagem</th>
                <th>Vídeos</th>
                <th>Galeria de Imagens</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->nome }}</td>
                <td><img src="{{ asset('assets/img/professores/'.$registro->imagem) }}" style="width: 100%; max-width:100px;" alt=""></td>
                <td><a href="{{ route('painel.a-solum.professores.videos.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-film" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td><a href="{{ route('painel.a-solum.professores.imagens.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.a-solum.professores.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.a-solum.professores.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
