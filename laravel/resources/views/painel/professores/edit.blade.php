@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Solum / Professores /</small> Editar Professor</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-solum.professores.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.professores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
