@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Cursos Intensivos /</small> Editar Curso Intensivo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.cursos-intensivos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cursos-intensivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
