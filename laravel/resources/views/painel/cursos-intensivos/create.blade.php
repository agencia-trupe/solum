@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Cursos Intensivos /</small> Adicionar Curso Intensivo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aulas.cursos-intensivos.store', 'files' => true]) !!}

        @include('painel.cursos-intensivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
