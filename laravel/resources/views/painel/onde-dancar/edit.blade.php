@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mural / Onde Dançar /</small> Editar Estabelecimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mural.onde-dancar.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.onde-dancar.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
