@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mural / Onde Dançar /</small> Adicionar Estabelecimento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.mural.onde-dancar.store', 'files' => true]) !!}

        @include('painel.onde-dancar.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
