@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Aulas /</small> Cursos Regulares
            <div class="btn-group pull-right">
                <a href="{{ route('painel.aulas.cursos-regulares.imagens-laterais.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens Laterais</a>
            </div>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.cursos-regulares.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cursos-regulares.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
