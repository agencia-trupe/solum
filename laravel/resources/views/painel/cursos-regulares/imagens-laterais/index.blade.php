@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.aulas.cursos-regulares.index') }}" title="Voltar para Cursos Regulares" class="btn btn-sm btn-default">
        &larr; Voltar para Cursos Regulares
    </a>

    <legend>
        <h2>
            <small>Aulas / Cursos Regulares /</small> Imagens Laterais
            <a href="{{ route('painel.aulas.cursos-regulares.imagens-laterais.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Imagem Lateral</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="cursos_regulares_imagens_laterais">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ asset('assets/img/cursos-regulares/imagens-laterais/'.$registro->imagem) }}" style="width: 100%; max-width:150px;" alt=""></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.aulas.cursos-regulares.imagens-laterais.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
