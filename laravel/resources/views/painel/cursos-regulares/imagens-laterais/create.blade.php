@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Cursos Regulares / Imagens Laterais /</small> Adicionar Imagem Lateral</h2>
    </legend>

    {!! Form::open(['route' => ['painel.aulas.cursos-regulares.imagens-laterais.store'], 'files' => true]) !!}

        @include('painel.cursos-regulares.imagens-laterais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
