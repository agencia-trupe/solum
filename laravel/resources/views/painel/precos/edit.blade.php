@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Preços /</small> Editar Preço</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.precos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.precos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
