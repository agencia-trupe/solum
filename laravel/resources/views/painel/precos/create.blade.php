@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Preços /</small> Adicionar Preço</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aulas.precos.store', 'files' => true]) !!}

        @include('painel.precos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
