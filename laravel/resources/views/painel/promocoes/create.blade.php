@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Promoções /</small> Adicionar Promoção</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aulas.promocoes.store', 'files' => true]) !!}

        @include('painel.promocoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
