@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / Promoções /</small> Editar Promoção</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.promocoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.promocoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
