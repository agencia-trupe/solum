<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.home*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.home.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.home.banners.index') }}">Banners</a>
            </li>
        	<li @if(str_is('painel.home.chamadas*', Route::currentRouteName())) class="active" @endif>
        		<a href="{{ route('painel.home.chamadas.index') }}">Chamadas</a>
        	</li>
            <li @if(str_is('painel.home.videos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.home.videos.index') }}">Vídeos</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(str_is('painel.a-solum*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            A Solum
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.a-solum.historico*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.a-solum.historico.index') }}">Histórico</a>
            </li>
            <li @if(str_is('painel.a-solum.estrutura*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.a-solum.estrutura.index') }}">Estrutura</a>
            </li>
            <li @if(str_is('painel.a-solum.professores*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.a-solum.professores.index') }}">Professores</a>
            </li>
            <li @if(str_is('painel.a-solum.vantagens*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.a-solum.vantagens.index') }}">Vantagens</a>
            </li>
            <li @if(str_is('painel.a-solum.videos-e-fotos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.a-solum.videos-e-fotos.index') }}">Vídeos e Fotos</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(str_is('painel.aulas*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Aulas
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.aulas.ritmos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aulas.ritmos.index') }}">Ritmos</a>
            </li>
            <li @if(str_is('painel.aulas.grade-de-aulas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aulas.grade-de-aulas.index') }}">Grade de Aulas</a>
            </li>
            <li @if(str_is('painel.aulas.cursos-regulares*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aulas.cursos-regulares.index') }}">Cursos Regulares</a>
            </li>
            <li @if(str_is('painel.aulas.cursos-intensivos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aulas.cursos-intensivos.index') }}">Cursos Intensivos</a>
            </li>
            <li @if(str_is('painel.aulas.precos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aulas.precos.index') }}">Preços</a>
            </li>
            <li @if(str_is('painel.aulas.promocoes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aulas.promocoes.index') }}">Promoções</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(str_is('painel.calendario*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Calendário
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.calendario.eventos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.calendario.eventos.index') }}">Eventos</a>
            </li>
            <li @if(str_is('painel.calendario.turmas-e-aulas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.calendario.turmas-e-aulas.index') }}">Turmas e Aulas</a>
            </li>
        </ul>
    </li>

    <li @if(str_is('painel.mais-servicos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.mais-servicos.index') }}">Mais Serviços</a>
    </li>

    <li class="dropdown @if(str_is('painel.mural*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Mural
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.mural.avisos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.mural.avisos.index') }}">Avisos</a>
            </li>
            <li @if(str_is('painel.mural.onde-dancar*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.mural.onde-dancar.index') }}">Onde Dançar</a>
            </li>
            <li @if(str_is('painel.mural.onde-comprar*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.mural.onde-comprar.index') }}">Onde Comprar</a>
            </li>
            <li @if(str_is('painel.mural.enquetes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.mural.enquetes.index') }}">Enquetes</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(str_is('painel.localizacao*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Localização
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.localizacao.como-chegar*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.localizacao.como-chegar.index') }}">Como Chegar</a>
            </li>
            <li @if(str_is('painel.localizacao.hospedagem*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.localizacao.hospedagem.index') }}">Hospedagem</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.contato.index', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(str_is('painel.contato.respostas-automaticas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.contato.respostas-automaticas.index') }}">Respostas Automáticas</a>
            </li>
            <li @if(str_is('painel.contato.recebidos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>
