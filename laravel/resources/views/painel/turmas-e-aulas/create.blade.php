@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Calendário / Turmas e Aulas /</small> Adicionar Aviso</h2>
    </legend>

    {!! Form::open(['route' => 'painel.calendario.turmas-e-aulas.store', 'files' => true]) !!}

        @include('painel.turmas-e-aulas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
