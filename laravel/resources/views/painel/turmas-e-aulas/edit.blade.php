@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Calendário / Turmas e Aulas /</small> Editar Aviso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.calendario.turmas-e-aulas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.turmas-e-aulas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
