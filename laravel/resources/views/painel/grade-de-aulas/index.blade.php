@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Aulas /</small> Grade de Aulas
            <a href="{{ route('painel.aulas.grade-de-aulas.niveis.index') }}" class="btn btn-info btn-sm pull-right"><span class="glyphicon glyphicon-signal" style="margin-right:10px;"></span>Editar Níveis</a>
        </h2>
    </legend>

    <div class="well">
        {!! Form::open(['route' => 'painel.aulas.grade-de-aulas.store', 'files' => true, 'id' => 'form-grade']) !!}
            {!! Form::label('arquivo', 'Upload de arquivo CSV') !!}
            <div class="input-group">
                {!! Form::file('arquivo', ['class' => 'form-control']) !!}
                <span class="input-group-btn">
                    {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
                </span>
            </div>
        {!! Form::close() !!}
        <div class="row">
            <div class="col-md-2">
                <a href="{{ asset('assets/painel/GradeDeAulas-Modelo.csv') }}" class="alert alert-warning btn btn-warning btn btn-block" style="margin-bottom:0;margin-top:15px;text-align:left">
                    <span class="glyphicon glyphicon-download" style="margin-right:10px;"></span>
                    <small>Baixar Modelo</small>
                </a>
            </div>
            <div class="col-md-10">
                <div class="alert alert-info" style="margin-bottom:0;margin-top:15px">
                    <small>
                        <span class="glyphicon glyphicon-info-sign" style="margin-right: 10px;font-size:1.1em"></span>
                        Os títulos de <em>Ritmos</em> e <em>Níveis</em> do arquivo devem ser exatamente iguais aos cadastrados no painel.
                    </small>
                </div>
            </div>
        </div>
    </div>

    @if(!count($grade))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado. Faça upload de um arquivo.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        @foreach($grade as $diaDaSemana => $rows)
        <thead>
            <tr>
                <th colspan="8" style="text-align:center" class="success">
                    {{ $diaDaSemana }}
                </th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th style="vertical-align: middle !important;">Ritmo</th>
                <th style="vertical-align: middle !important;">Nível</th>
                <th style="vertical-align: middle !important;">Início</th>
                <th style="vertical-align: middle !important;">Horário</th>
                <th style="vertical-align: middle !important;">Professores</th>
                <th style="vertical-align: middle !important;">Observações</th>
                <th style="vertical-align: middle !important;">Observações Extra<br><small>(sobrepõe observações)</small></th>
                <th style="vertical-align: middle !important;"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($rows as $row)
            <tr class="tr-row" id="{{ $row->id }}">
                <td>
                    <a href="#" class="inline-edit-field" data-type="select" data-source="{{ route('painel.aulas.grade-de-aulas.getRitmos') }}" data-name="ritmo" data-pk="{{ $row->id }}" data-value="{{ $row->ritmo }}" data-url="{{ route('painel.aulas.grade-de-aulas.update') }}" data-title="Ritmo">
                        {{ $row->ritmo }}
                    </a>
                </td>
                <td>
                    <a href="#" class="inline-edit-field" data-type="select" data-source="{{ route('painel.aulas.grade-de-aulas.getNiveis') }}" data-name="nivel" data-pk="{{ $row->id }}" data-value="{{ $row->nivel }}" data-url="{{ route('painel.aulas.grade-de-aulas.update') }}" data-title="Nível">
                        {{ $row->nivel }}
                    </a>
                </td>
                <td>
                    <a href="#" class="inline-edit-field" data-type="text" data-name="inicio" data-pk="{{ $row->id }}" data-url="{{ route('painel.aulas.grade-de-aulas.update') }}" data-title="Início">
                        {{ $row->inicio }}
                    </a>
                </td>
                <td>
                    <a href="#" class="inline-edit-field" data-type="text" data-name="horario" data-pk="{{ $row->id }}" data-url="{{ route('painel.aulas.grade-de-aulas.update') }}" data-title="Horário">
                        {{ $row->horario }}
                    </a>
                </td>
                <td>
                    <a href="#" class="inline-edit-field" data-type="text" data-name="professores" data-pk="{{ $row->id }}" data-url="{{ route('painel.aulas.grade-de-aulas.update') }}" data-title="Professores">
                        {{ $row->professores }}
                    </a>
                </td>
                <td>
                    <a href="#" class="inline-edit-observacoes" data-type="select" data-name="observacoes" data-pk="{{ $row->id }}" data-value="{{ $row->observacoes }}" data-url="{{ route('painel.aulas.grade-de-aulas.update') }}" data-title="Observações">
                        {{ $row->observacoes }}
                    </a>
                </td>
                <td>
                    <a href="#" class="inline-edit-field" data-type="text" data-name="observacoes_extra" data-pk="{{ $row->id }}" data-url="{{ route('painel.aulas.grade-de-aulas.update') }}" data-title="Observações Extra">
                        {{ $row->observacoes_extra }}
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.aulas.grade-de-aulas.destroy', $row->id],
                        'method' => 'delete'
                    ]) !!}
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove"></span></button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
        @endforeach
    </table>
    @endif

@endsection
