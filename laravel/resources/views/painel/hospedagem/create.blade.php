@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Localização / Hospedagem /</small> Adicionar Hospedagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.localizacao.hospedagem.store', 'files' => true]) !!}

        @include('painel.hospedagem.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
