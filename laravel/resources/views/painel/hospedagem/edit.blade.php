@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Localização / Hospedagem /</small> Editar Hospedagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.localizacao.hospedagem.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.hospedagem.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
