(function(window, document, $, undefined) {
    'use strict';

    var Painel = {};

    Painel.deleteButton = function() {
        $('body').on('click', '.btn-delete', function(e) {
            e.preventDefault();
            var form = $(this).closest('form'),
                _this = this,
                message = ($(this).hasClass('btn-delete-multiple') ? 'Deseja excluir todos os registros?' :'Deseja excluir o registro?');

            bootbox.confirm({
                size: 'small',
                backdrop: true,
                message: message,
                buttons: {
                    'cancel': {
                        label: 'Cancelar',
                        className: 'btn-default btn-sm'
                    },
                    'confirm': {
                        label: '<span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir',
                        className: 'btn-primary btn-danger btn-sm'
                    }
                },
                callback: function(result) {
                    if (result) {
                        if ($(_this).hasClass('btn-delete-link')) return window.location.href = $(_this)[0].href;
                        form.submit();
                    }
                }
            });
        });
    };

    Painel.orderTable = function() {
        if (!$('.table-sortable').length) return;

        $('.table-sortable tbody').sortable({
            update: function () {
                var url   = $('base').attr('href') + '/painel/order',
                    data  = [],
                    table = $('.table-sortable').attr('data-table');

                $('.table-sortable tbody').children('tr').each(function(index, el) {
                    data.push(el.id)
                });

                $.post(url, { data: data, table: table });
            },
            handle: $('.btn-move')
        }).disableSelection();
    };

    Painel.datePicker = function() {
        if (!$('.datepicker').length) return;

        $('.datepicker').datepicker({
            closeText: 'Fechar',
            prevText: '&#x3c;Anterior',
            nextText: 'Pr&oacute;ximo&#x3e;',
            currentText: 'Hoje',
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
            dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        });

        if ($('.datepicker').val() == '' && !$('.datepicker').hasClass('datepicker-no-default')) $('.datepicker').datepicker("setDate", new Date());
    };

    Painel.datetimePicker = function() {
        if (!$('.datetimepicker').length) return;

        $('.datetimepicker').datetimepicker({
            timeText: 'Horário',
            hourText: 'Hora',
            minuteText: 'Minuto',
            closeText: 'Fechar',
            prevText: '&#x3c;Anterior',
            nextText: 'Pr&oacute;ximo&#x3e;',
            currentText: 'Hoje',
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
            dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        });

        if ($('.datetimepicker').val() == '') $('.datetimepicker').datetimepicker("setDate", new Date());
    };

    Painel.monthPicker = function() {
        if (!$('.monthpicker').length) return;

        $('.monthpicker').datepicker({
            changeMonth: true,
            changeYear: true,
            onClose: function() {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            },
            beforeShow: function() {
                var selDate = $(this).val();
                if (selDate.length > 0) {
                    var year = selDate.substring(selDate.length - 4);
                    var month = selDate.substring(0, 2);
                    $(this).datepicker('option', 'defaultDate', new Date(year, month, 0))
                    $(this).datepicker('setDate', new Date(year, month, 0));
                }
            },
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dateFormat: 'mm/yy',
        });

        $('html > head').append('<style>.ui-datepicker-calendar { display: none; }.ui-datepicker select.ui-datepicker-month,.ui-datepicker select.ui-datepicker-year{ color: #2C3E50; font-weight: normal; }</style>');
        if ($('.monthpicker').val() == '') $('.monthpicker').datepicker("setDate", new Date());
    };

    Painel.textEditor = function() {
        if (!$('.ckeditor').length) return;

        CKEDITOR.config.language = 'pt-br';
        CKEDITOR.config.uiColor = '#dce4ec';

        var config = {
            padrao: {
                toolbar: [['Bold', 'Italic']]
            },

            padraoBr: {
                toolbar: [['Bold', 'Italic']],
                enterMode: CKEDITOR.ENTER_BR
            },

            clean: {
                toolbar: []
            },

            homeChamadas: {
                toolbar: [['Bold']],
                enterMode: CKEDITOR.ENTER_BR
            },

            institucional: {
                allowedContent: true,
                extraPlugins: 'injectimage',
                toolbar: [['Bold', 'Italic'], ['BulletedList'], ['InjectImage'], ['Maximize']]
            },

            comoChegarEndereco: {
                toolbar: [],
                enterMode: CKEDITOR.ENTER_BR
            },

            respostaAutomatica: {
                toolbar: [['Bold', 'Italic'], ['Link', 'Unlink'], ['Maximize']]
            },

            cursosIntensivos: {
                toolbar: [['Bold', 'Italic'], ['Link', 'Unlink']]
            }
        };

        $('.ckeditor').each(function (i, obj) {
            CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
        });
    };

    Painel.imagesUpload = function() {
        var $wrapper = $('#images-upload');
        if (!$wrapper.length) return;

        var errors;

        $wrapper.fileupload({
            dataType: 'json',
            start: function(e) {
                if ($('.no-images').length) $('.no-images').fadeOut();

                if ($('.errors').length) {
                    errors = [];
                    $('.errors').fadeOut().html('');
                }
            },
            done: function (e, data) {
                $('#imagens').prepend($(data.result.body).hide().addClass('new'))
                             .sortable('refresh');
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                $('.progress-bar').css(
                    'width',
                    progress + '%'
                );
            },
            stop: function() {
                $('.progress-bar').css('width', 0);

                $('#imagens').find('.imagem').sort(function(a, b) {
                    var imgA = $(a).data('imagem').toUpperCase();
                    var imgB = $(b).data('imagem').toUpperCase();
                    var ordA = $(a).data('ordem');
                    var ordB = $(b).data('ordem');

                    if (ordA < ordB) return -1;
                    if (ordA > ordB) return 1;
                    if (imgA < imgB) return -1;
                    if (imgA > imgB) return 1;
                    return 0;
                }).appendTo($('#imagens'));

                $('#imagens .imagem.new').each(function(i) {
                    $(this).delay((i++) * 400).fadeIn(300);
                });

                $('.imagem').removeClass('new');

                if (errors.length) {
                    errors.forEach(function(message) {
                        $('.errors').append(message + '<br>');
                    });
                    $('.errors').fadeIn();
                }
            },
            fail: function(e, data) {
                var status       = data.jqXHR.status,
                    errorMessage = (status == '422' ? 'O arquivo deve ser uma imagem.' : 'Erro interno do servidor.'),
                    response     = 'Ocorreu um erro ao enviar o arquivo ' + data.files[0].name + ': ' + errorMessage;

                errors.push(response);
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    };

    Painel.orderImages = function() {
        var $wrapper = $('#imagens');
        if (!$wrapper.length) return;

        $wrapper.sortable({
            update: function () {
                var url   = $('base').attr('href') + '/painel/order',
                    data  = [],
                    table = $wrapper.attr('data-table');

                $wrapper.children('.imagem').each(function(index, el) {
                    el.dataset.ordem = index + 1;
                    $(el).data('ordem', index + 1);
                    data.push(el.id);
                });

                $.post(url, { data: data, table: table });
            },
            handle: '> img'
        }).disableSelection();
    };

    Painel.filtroSelect = function() {
        $('#filtro-select').on('change', function () {
            var id    = $(this).val(),
                base  = $('base').attr('href'),
                route = $(this).data('route');

            if (id) {
                window.location = base + '/' + route + '?filtro=' + id;
            } else {
                window.location = base + '/' + route;
            }
        });
    };

    Painel.dataTables = function() {
        var table = $('.data-table').DataTable({
            "aaSorting": [],
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "lengthChange": false,
                    "targets": 'no-filter'
                },
            ],
            "lengthChange": false,
            "pagingType": 'full_numbers',
            "language": {
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhuma entrada na tabela",
                "search": "_INPUT_",
                "searchPlaceholder": "Procurar...",
                "zeroRecords": "Nenhum dado encontrado",
                "infoFiltered":   "(filtrado de _MAX_ no total)",
                "paginate": {
                    "first":    'Primeiro',
                    "previous": '‹',
                    "next":     '›',
                    "last":     'Último'
                }
            },
            "dom": "<'row'<'col-sm-6'f><'col-sm-6'p>>" +
                   "<'row'<'col-sm-12'tr>>"
        });
        $('.dataTables_filter').css('text-align', 'left').find('input').removeClass('input-sm').css('margin-left', 0);
    };

    Painel.confirmacaoGrade = function() {
        $('#form-grade .btn').click(function(e) {
            e.preventDefault();
            var form = $('#form-grade');

            bootbox.confirm({
                size: 'medium',
                backdrop: true,
                message: 'Deseja enviar um novo arquivo?<br>Todos os registros da Grade de Aulas serão substituídos.',
                buttons: {
                    'cancel': {
                        label: 'Cancelar',
                        className: 'btn-default btn-sm'
                    },
                    'confirm': {
                        label: '<span class="glyphicon glyphicon-upload" style="margin-right:10px;"></span>Enviar',
                        className: 'btn-primary btn-success btn-sm'
                    }
                },
                callback: function(result) {
                    if (result) form.submit();
                }
            });
        });
    };

    Painel.inlineEdit = function() {
        $('.inline-edit-field').editable({
            emptytext: 'Vazio',
            error: function() {
                return 'Ocorreu um erro. Tente novamente.';
            }
        });

        $('.inline-edit-observacoes').editable({
            emptytext: 'Vazio',
            source: {
                '': 'Vazio',
                'Disponível para Homens e Mulheres': 'Disponível para Homens e Mulheres',
                'Disponível para Homens': 'Disponível para Homens',
                'Disponível para Mulheres': 'Disponível para Mulheres'
            },
            error: function() {
                return 'Ocorreu um erro. Tente novamente.';
            }
        });
    };

    Painel.enquetes = function() {
        var inputEnqueteOpcao = '<div class="form-group"><div class="input-group"><input class="form-control enquete-opcao" placeholder="Opção de resposta" style="z-index:0" name="enquete_opcao[]" type="text"><span class="input-group-btn"><a href="#" class="btn btn-default btn-danger enquetes-remove-opcao"><span class="glyphicon glyphicon-remove-circle"></span></a></span></div></div>';

        $('.enquetes-adiciona-opcao').click(function(event) {
            event.preventDefault();
            $('.enquete-opcoes-wrapper').append(inputEnqueteOpcao)
                .children(':last').hide().fadeIn().find('input').focus();
        });

        $('body').on('click', '.enquetes-remove-opcao', function(event) {
            event.preventDefault();
            $(this).parent().parent().parent().fadeOut(function() {
                $(this).remove();
            });
        });

        $('.enquete-ativar').click(function(event) {
            event.preventDefault();

            var _this = $(this);

            bootbox.confirm({
                size: 'medium',
                backdrop: true,
                message: '<h4 style="margin-top:0">Deseja ativar a enquete?</h4><div class="alert alert-warning text-center" style="margin-bottom:0" role="alert">Ao ativar esta enquete, a enquete ativa anteriormente será desativa.<br>Caso ela já possua votos, será encerrada e arquivada no Histórico.</div>',
                buttons: {
                    'cancel': {
                        label: 'Cancelar',
                        className: 'btn-default btn-sm'
                    },
                    'confirm': {
                        label: 'Ativar',
                        className: 'btn-primary btn-primary btn-sm'
                    }
                },
                callback: function(result) {
                    if (result) {
                        return window.location.href = _this[0].href;
                    }
                }
            });
        });
    };

    Painel.init = function() {
        this.deleteButton();
        this.orderTable();
        this.datePicker();
        this.datetimePicker();
        this.monthPicker();
        this.textEditor();
        this.imagesUpload();
        this.orderImages();
        this.filtroSelect();
        this.dataTables();
        this.confirmacaoGrade();
        this.inlineEdit();
        this.enquetes();
    };

    $(document).ready(function() {
        Painel.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
